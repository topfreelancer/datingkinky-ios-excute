//
//  ApiManager.swift
//  Everave Update
//
//  Created by Ubuntu on 16/01/2020
//  Copyright © 2020 Ubuntu. All rights reserved.
//
    
import Alamofire
import Foundation
import SwiftyJSON
// ************************************************************************//
                            // Datingkinky project //
// ************************************************************************//

let SERVER_URL = "https://api.datingkinky.com/" // live server
let SERVER_URL1 = "http://meets.email/api/" // live server
//let SERVER_URL = "https://staging.datingkinky.com/" // staging server

let AUTHENTICATE = SERVER_URL + "authenticate"
let REGISTER = SERVER_URL + "users"
let CONFIRM_EMAIL = SERVER_URL + "confirm-email"
let RESETPASSWORD4TOKEN = SERVER_URL + "reset-password"
let RESETPASSWORD = SERVER_URL + "reset-password"
let GETSITEUSERS = SERVER_URL + "users"
/* https://staging.datingkinky.com/users/5f23ff38b5d619261e876618/profile*/
let UPDATE_USER_PROFILE = SERVER_URL + "users/" + "\(thisuser?.user?._id ?? "")" + "/profile"
let GETUSER_PROFILE = SERVER_URL + "users/"
let SAVEAVATAR = SERVER_URL + "photos"
let GETOPTIONS = SERVER_URL + "options"
let FILTERUSER = SERVER_URL + "search/users?limit=21&skip="
let FILTERBYUSERNAME = SERVER_URL + "search/users?username="
let REPORT = SERVER_URL + "reports"
let UPLOADTOKEN = SERVER_URL1 + "uploadToken"
let CHATREQUEST = SERVER_URL1 + "chatRequest"
let SETRECORDINGEND_OTHERERSTATUS = SERVER_URL1 + "setRecordingEnd_CheckOtherStatus"
let SETBLOCK_PARAMETERS = SERVER_URL1 + "setBlockParameters"
let ADD_BLOCKUSERS = SERVER_URL1 + "addBlockUsers"
let GET_BLOCKUSERS = SERVER_URL1 + "getBlockUserss"
let REMOVE_BLOCKUSERS = SERVER_URL1 + "removeBlockUsers"
let GETBLOCK_PARAMETERS = SERVER_URL1 + "getBlockParameters"
let GETMEMBERSHIPINFO = SERVER_URL1 + "getMembershipInfo"
let GETOTHERUSERPARAMETER = SERVER_URL1 + "getOtherUsersParameters"

struct PARAMS {
    
    static let AVATAR                     = "avatar"
    static let BLOCKUSERS                 = "blocks"
    static let BLOCKEDUSERS               = "blockedusers"
    static let SNOOZEDUSERS               = "snoozedusers"
    static let CHAT_ACCEPT                = "chatAccept"
    static let CHAT_REJECT                = "chatReject"
    static let CHAT_REQUEST               = "chatRequest"
    static let CLIENT_ACC_NUM             = "clientAccnum"
    static let CLIENT_SUB_ACC             = "clientSubacc"
    static let COORDINATES                = "coordinates"
    static let CREATED_AT                 = "createdAt"
    static let CURRENTAGE                 = "currentage"
    static let DATE_OF_BIRTH              = "dateOfBirth"
    static let EMAIL                      = "email"
    static let EVENT_TYPE                 = "eventType"
    static let FIRST_20MINS               = "first_20mins"
    static let FIRST_LOGIN                = "first_login"
    static let FOSTA                      = "fosta"
    static let FREE_TRIAL                 = "free_trial"
    static let GENDER                     = "gender"
    static let HAS_AVATAR                 = "hasAvatar"
    static let HAS_FILLED_ABOUTME         = "hasFilledAboutMe"
    static let HAS_FILLED_MATCHING_DATA   = "hasFilledMatchingData"
    static let HAS_LOGGEDIN_ONCE          = "hasLoggedInOnce"
    static let HAS_PLUS_MEMBERSHIP        = "hasPlusMembership"
    static let HAS_PRIMARY_AUDIO_GREETING = "hasPrimaryAudioGreeting"
    static let HAS_PRIMARY_VIDEO_GREETING = "hasPrimaryVideoGreeting"
    static let ISBLOCK_AUDIOCHAT          = "isblock_audio_chat"
    static let SNOOZE_ACCOUNTS          = "snooze_accounts"
    static let ISBLOCK_CHATREQUEST        = "isblock_chat_request"
    static let ISBLOCK_RANDOMCHAT         = "isblock_random_chat"
    static let ISBLOCK_VIDEOCHAT          = "isblock_video_chat"
    static let ISBLOCK_VIDEOVERIFY        = "isblock_video_verify"
    static let JOINED_DATE                = "joined_date"
    static let KINKROLE                   = "kinkRole"
    static let LAST_DAY                   = "last_day"
    static let LOCATION                   = "location"
    static let LOGIN_COUNT                = "loginCount"
    static let LOGOUT                     = "logout"
    static let LOWERCASE_NAME             = "lowerCaseName"
    static let NAME                       = "name"
    static let NEXT_RENEWAL_DATE          = "nextRenewalDate"
    static let PASSWORD                   = "password"
    static let PAYMENT_TYPE               = "paymentType"
    static let PHONE                      = "phone"
    static let PLUS_PAYMENT_RECORD        = "plusPaymentRecord"
    static let PRICE_DESCRIPTION          = "priceDescription"
    static let PRONOUN                    = "pronoun"
    static let REBILLS                    = "rebills"
    static let RECEIVER                   = "1"
    static let RECURRING_PERIOD           = "recurringPeriod"
    static let REFERRING_URL              = "referringUrl"
    static let ROLE                       = "role"
    static let SENDER                     = "0"
    static let SNOOZE_ACCOUNT             = "snooze_account"
    static let STATUS                     = "status"
    static let STATUS_CODE                = "statusCode"
    static let SUBSCRIPTION_ID            = "subscriptionId"
    static let SUBSCRIPTION_TYPE_ID       = "subscriptionTypeId"
    static let TIMESTAMP                  = "timestamp"
    static let TOKEN                      = "token"
    static let TRANSACTION_ID             = "transactionId"
    static let UPDATED_AT                 = "updatedAt"
    static let USER                       = "user"
    static let USERNAME                   = "username"
    static let USER_ID                    = "user_id"
    static let USER_LAT                   = "user_lat"
    static let USER_LNG                   = "user_lng"
    static let USER_LOCATION              = "user_location"
    static let VERIFY_ACCEPT              = "verifyAccept"
    static let VERIFY_REJECT              = "verifyReject"
    static let VERIFY_REQUEST             = "verifyRequest"
    static let VERIFY_TOKEN               = "verify_token"
    static let X_AUTH_TOKEN               = "x-auth-token"
    static let _1ADY_BEFORE               = "1day_before"
    static let _2DAYS_BEFORE              = "2days_before"
    static let _ID                        = "_id"
    static let __V                        = "__v"
    
    // FOR MEMBERSHIP PART
    static let ID                        = "id"
    static let IS_PLUSMEMBER      = "is_plusmember"
    static let CURRENT_MEMBERSHIP = "current_membership"
    static let RENEW_DATE         = "renew_date"
    static let LOOKING_FOR        = "looking_for"
    static let ORIENTATION        = "orientation"
    static let RELATIONSHIP_STYLE = "relationship_style"
    static let HEREFOR            = "herefor"
    static let KINKROLES          = "kinkroles"
    static let TIRAL_START          = "tiralstart"
    static let TRIAL_END          = "trialend"
    static let READ_RECEIPT          = "read_receipt"
    // VIDEO CHATTING FLAG
    static let IS_VIDEO_CHATTING    = "is_video_chatting"
    static let IS_VOICE_CHATTING    = "is_voice_chatting"
}

enum RequestType: String {
    case chat_accept       = "chatAcccept"
    case chat_reject       = "chatReject"
    case chat_send         = "chatSend"
    case chat_verifyaccept = "verifyAccept"
    case chat_verifyreject = "verifyReject"
    case chat_verifysend   = "verifySend"
    case randomChat        = "randomChat"
    
    case video_chat_send   = "videoChatSend"
    case video_chat_reject = "videoChatReject"
    case video_chat_accept = "videoChatAccept"
    
    case voice_chat_send   = "voiceChatSend"
    case voice_chat_reject = "voiceChatReject"
    case voice_chat_accept = "voiceChatAccept"
    case general_chat      = "general_chat"
}

class ApiManager {

    class func authenticate(username : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        UserDefault.setString(key: PARAMS.X_AUTH_TOKEN, value: nil)
        let params = [PARAMS.USERNAME : username, PARAMS.PASSWORD : password] as [String : Any]
        Alamofire.request(AUTHENTICATE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                // initialize all local varialbe
                roomID = nil
                memberName4ChatRequest = nil
                memberProfile4ChatRequest = nil
                partnerAge4ChatRequestAccept = nil
                partnerAvatar4ChatRequest = nil
                partnerGender4ChatRequest = nil
                partnerKinkRole4ChatRequest = nil
                partnerPronoun4ChatRequest = nil
                partnerLocation4ChatRequest = nil
                memberName4AccepttRequest = nil
                blockusers.clearUserInfo()
                dismiss_flag = false
                let dict = JSON(data)
                //print(dict)
                if let headers = response.response?.allHeaderFields {
                    let x_auth_token = JSON(headers["x-auth-token"] as Any).stringValue
                    UserDefault.setString(key: PARAMS.X_AUTH_TOKEN, value: x_auth_token)
                    print("x-auth-token===>",JSON(headers["x-auth-token"] as Any).stringValue)
                    print("userid===>", thisuser?.user?._id ?? "")
                }
                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                if status == 201 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func getLocations(positionName : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + "\(positionName).json?" + "access_token=" + Constants.MAPBOX_ACCESS_TOKEN
        Alamofire.request(requestURL, method:.get)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    //print(dict)
                    completion(true,dict)
            }
        }
    }
    
    class func register( registerModel:RegisterModel!,completion: @escaping ( _ success: Bool, _ response: Any? ) -> ()) {
        let params: [String: Any] = [
            "username": registerModel.username!,
            "password": registerModel.password!,
            "email": registerModel.email!,
            "dateOfBirth": registerModel.dateofBirth!,
            "location": registerModel.location!,
            "coordinates": [
                "lat": registerModel.coordinates?.lat!.toFloat() ?? 0,
                "lng": registerModel.coordinates?.lng!.toFloat() ?? 0
            ],
            "accountType": "5b8c3662633635082db7ae29",
            "role": "5eeba968feaa9256ed6eea3c",
            "fosta": "true"
        ]
        
        Alamofire.request(REGISTER,method:.post, parameters:params)
              .responseJSON { response in
                    switch response.result {
                      case .failure:
                          completion(false, nil)
                      case .success(let data):
                          let dict = JSON(data)
                          print("this is response result==>",dict)
                          let status = dict[PARAMS.STATUS_CODE].intValue
                          if status == 201 {
                              completion(true, dict)
                              
                          } else {
                              completion(false, dict)
                          }
                      }
            }
         }
    
    class func confirmEmail(email : String, token : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL : email, PARAMS.TOKEN : token] as [String : Any]
        Alamofire.request(CONFIRM_EMAIL, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func resetPassword4Token(email : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL : email] as [String : Any]
        Alamofire.request(RESETPASSWORD4TOKEN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)

                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func resetPassword(email : String,token : String,password : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL : email, PARAMS.TOKEN: token, PARAMS.PASSWORD: password] as [String : Any]
        Alamofire.request(RESETPASSWORD, method:.patch, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func getsiteUsers(status: String?, dialingCode: String?, phoneNumberVerified: Bool?, plusMembers: Bool?, role: String?, username: String?, emailConfirmed: Bool?, accountType: String?, onlyDeleted: Bool?,updatedAt: String?, skip: Int?, limit: Int?, sort: String?,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        var requestURL = GETSITEUSERS + "?"
        var firstadded: Bool = true
        var hasParameters: Bool = false
        if let status = status{
            requestURL = requestURL + "status=" + status
            firstadded = false
            hasParameters = true
        }
        if let dialingCode = dialingCode{
            if firstadded{
                requestURL = requestURL + "dialingCode=" + dialingCode
                firstadded = false
            }else{
                requestURL = requestURL + "&dialingCode=" + dialingCode
            }
            hasParameters = true
        }
        if let phoneNumberVerified = phoneNumberVerified{
            if firstadded{
                requestURL = requestURL + "phoneNumberVerified=" + "\(phoneNumberVerified)"
                firstadded = false
            }else{
                requestURL = requestURL + "&phoneNumberVerified=" + "\(phoneNumberVerified)"
            }
            hasParameters = true
        }
        if let plusMembers = plusMembers{
            if firstadded{
                requestURL = requestURL + "plusMembers=" + "\(plusMembers)"
                firstadded = false
            }else{
                requestURL = requestURL + "&plusMembers=" + "\(plusMembers)"
            }
            hasParameters = true
        }
        if let role = role{
            if firstadded{
                requestURL = requestURL + "role=" + role
                firstadded = false
            }else{
                requestURL = requestURL + "&role=" + role
            }
            hasParameters = true
        }
        if let username = username{
            if firstadded{
                requestURL = requestURL + "username=" + username
                firstadded = false
            }else{
                requestURL = requestURL + "&username=" + username
            }
            hasParameters = true
        }
        if let emailConfirmed = emailConfirmed{
            if firstadded{
                requestURL = requestURL + "emailConfirmed=" + "\(emailConfirmed)"
                firstadded = false
            }else{
                requestURL = requestURL + "&emailConfirmed=" + "\(emailConfirmed)"
            }
            hasParameters = true
        }
        if let accountType = accountType{
            if firstadded{
                requestURL = requestURL + "accountType=" + accountType
                firstadded = false
            }else{
                requestURL = requestURL + "&accountType=" + accountType
            }
            hasParameters = true
        }
        if let onlyDeleted = onlyDeleted{
            if firstadded{
                requestURL = requestURL + "onlyDeleted=" + "\(onlyDeleted)"
                firstadded = false
            }else{
                requestURL = requestURL + "&onlyDeleted=" + "\(onlyDeleted)"
            }
            hasParameters = true
        }
        if let updatedAt = updatedAt{
            if firstadded{
                requestURL = requestURL + "accountType=" + updatedAt
                firstadded = false
            }else{
                requestURL = requestURL + "&accountType=" + updatedAt
            }
            hasParameters = true
        }
        if let skip = skip{
            if firstadded{
                requestURL = requestURL + "skip=" + "\(skip)"
                firstadded = false
            }else{
                requestURL = requestURL + "&skip=" + "\(skip)"
            }
            hasParameters = true
        }
        if let limit = limit{
            if firstadded{
                requestURL = requestURL + "limit=" + "\(limit)"
                firstadded = false
            }else{
                requestURL = requestURL + "&limit=" + "\(limit)"
            }
            hasParameters = true
        }
        if let sort = sort{
            if firstadded{
                requestURL = requestURL + "sort=" + "\(sort)"
                firstadded = false
            }else{
                requestURL = requestURL + "&sort=" + "\(sort)"
            }
            hasParameters = true
        }
        if !hasParameters{
            requestURL = GETSITEUSERS
        }
        let x_auth = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let headers = ["Authorization" : x_auth ?? ""] as [String : Any]
        Alamofire.request(requestURL, method: .get, headers: headers as? HTTPHeaders)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)

                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func updateUserProfile( updateuserModel:UpdateUserModel!,completion: @escaping ( _ success: Bool, _ response: Any? ) -> ()) {
    let params: [String: Any] = [
        "pronoun": updateuserModel.pronoun!,
        "location": updateuserModel.location!,
        "orientation": updateuserModel.orientation!,
      "coordinates": [
        "lat": updateuserModel.coordinates?.lat!.toFloat() ?? 0,
        "lng": updateuserModel.coordinates?.lng!.toFloat() ?? 0
      ],
      "gender": updateuserModel.gender!,
      "hereFor": updateuserModel.hereFor!,
      "relationshipStatus": updateuserModel.relationshipStatus!,
      "relationshipStyle": updateuserModel.relationshipStyle!,
      "relationshipStyleDetails": "testdetail",
      "kinkRole": updateuserModel.kinkRole!,
      "aboutMe": updateuserModel.aboutMe!,
    ]
        print(params)
    let x_auth = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
    let headers = ["Authorization" : x_auth ?? ""] as [String : Any]
        Alamofire.request(UPDATE_USER_PROFILE,method:.patch, parameters:params,headers: headers as? HTTPHeaders)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):

                let dict = JSON(data)
                print("this is response result==>",dict)
                let status = dict[PARAMS.STATUS_CODE].intValue
                if status == 200 {
                    completion(true, dict)
                }else{
                    completion(false, dict)
                }
            }
        }
    }
    
    class func getUserProfile(completion: @escaping ( _ success: Bool, _ response: Any? ) -> ()) {
        let x_auth = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let headers = ["Authorization" : x_auth ?? ""] as [String : Any]
        let getuserProfile = GETUSER_PROFILE + thisuser!.id! + "/profile"
        Alamofire.request( getuserProfile,method:.get,headers: headers as? HTTPHeaders)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                print("this is response result==>",dict)
                let status = dict[PARAMS.STATUS_CODE].intValue
                if status == 200 {
                    completion(true, dict)
                }else{
                    completion(false, dict)
                }
            }
        }
    }
    
    class func getPhotoURL(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = SERVER_URL + "files/upload-url/Site%20Photo?filetype=image/png"
        let x_auth = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let headers = ["Authorization" : x_auth ?? ""] as [String : Any]
        Alamofire.request(requestURL, method: .get, headers: headers as? HTTPHeaders)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    class func getPhotoURL4Report(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = SERVER_URL + "files/upload-url/Report?filetype=image/png"
        let x_auth = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let headers = ["Authorization" : x_auth ?? ""] as [String : Any]
        Alamofire.request(requestURL, method: .get, headers: headers as? HTTPHeaders)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                if status == 200 { // User authenticate
                    completion(true,dict)
                }else {
                    completion(false, dict)
                }
            }
        }
    }
    
    // upload image with binary type with put method:
    class func s3Upload(presignedURL : String,fileName:String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        /*let compressionQuality: CGFloat = 0.8
        guard let imageData = UIImageJPEGRepresentation(image, compressionQuality) else {
            print("Unable to get JPEG representation for image \(image)")
            return
        }*/
        // presignedUrl is a String
        let imgData = URL(fileURLWithPath: fileName)
        let url = presignedURL.removingPercentEncoding
        let header: HTTPHeaders = ["content-type": "image/png"]
        Alamofire.upload(imgData, to: presignedURL, method: .put, headers: header)
        .responseData {
            response in
            guard let httpResponse = response.response else {
                 print("Something went wrong uploading")
                 completion(false, nil)
                 return
            }
            print(response.result.debugDescription)
            if let publicUrl = presignedURL.components(separatedBy: "?").first {
                 print(publicUrl)
                completion(true, publicUrl)
            }
        }
    }
    
    class func saveAvatar(file :String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params: [String: Any] =  [
          "file": file,
          "avatar": true as Bool,
          "explicit": false as Bool,
          "privacy": "Public",
          "status": "Pending"
        ]
        //var SearchParams = [String:Any]()
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: SAVEAVATAR)
        var request = URLRequest(url: fileUrl!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                  case .failure:
                  completion(false, nil)
                  case .success(let data):
                  let dict = JSON(data)
                  let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                  if status == 201 { // User authenticate
                      completion(true,dict)
                  }else {
                      completion(false, dict)
                  }
              }
        }
    }
    
    class func getOptions(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        Alamofire.request(GETOPTIONS, method:.get)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                completion(true,dict)
            }
        }
    }
    
    class func filterUsers(username :String?,kinkRoles :[String]?,genders :[String]?,minAge:Int,maxAge: Int,skip: Int,distance: Int!, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        var p_userName = ""
        var p_kinkRole:[String]? = []
        var p_genders:[String]? = []
        var params: [String: Any] = [:]
        if let username = username{
            p_userName = username
        }
        if let kinkRole = kinkRoles{
            p_kinkRole = kinkRole
        }
        if let genders = genders{
            p_genders = genders
        }
        if let p_kinkRole = p_kinkRole,let p_genders = p_genders{
            params =  [
              "username": p_userName,
              "kinkRoles": p_kinkRole,
              //"identifiesAs": identifieAs,
              "genders": p_genders,
              "age": [
                "min": minAge,
                "max": maxAge
              ],
              "distance":distance ?? 0
            ]
        }
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: FILTERUSER + "\(skip)")
        var request = URLRequest(url: fileUrl!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                  case .failure:
                  completion(false, nil)
                  case .success(let data):
                  let dict = JSON(data)
                  let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
                  if status == 200 { // User authenticate
                      completion(true,dict)
                  }else {
                      completion(false, dict)
                  }
              }
        }
    }
    
    class func filterByUserName(username :String?, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let trimmedString = username!.removeWhitespace()
        print(trimmedString)
        let fileUrl = Foundation.URL(string: FILTERBYUSERNAME + trimmedString)
        if let fileUrl = fileUrl{
            var request = URLRequest(url: fileUrl)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
                Alamofire.request(request).responseJSON { response in
                    switch response.result {
                          case .failure:
                          completion(false, nil)
                          case .success(let data):
                          let dict = JSON(data)
            
                          let status = dict[PARAMS.STATUS_CODE].intValue // 0,1,2
            
                          if status == 200 { // User authenticate
                              completion(true,dict)
                          }else {
                              completion(false, dict)
                          }
                      }
                }
        }else{
            completion(false, nil)
        }
    }
    
    class func uploadToken(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        if let valid = thisuser?.isValid{
            if valid && !deviceTokenString.isEmpty{
                let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                let is_plus_member = thisuser!.hasPlusMembership && UserDefault.getBool(key: PARAMS.FREE_TRIAL, defaultValue: false) ? true : false
                let params = ["user_id" : thisuser!.id ?? "123456789", "token":deviceTokenString, "joined_date":"\(timeNow)", "is_plus_member": "\(is_plus_member)" ] as [String : Any]
                Alamofire.request(UPLOADTOKEN, method:.post, parameters:params)
                .responseJSON { response in
                    switch response.result {
                        case .failure:
                            completion(false, nil)
                        case .success(let data):
                            let datajson = JSON(data as Any)["user_info"].object
                            block_parameters!.clearUserInfo()
                            print(JSON(datajson))
                            block_parameters = BlockParameterModel(JSON(datajson))
                            block_parameters!.saveUserInfo()
                            block_parameters!.loadUserInfo()
                            blockusers.clearUserInfo()
                            blockusers = BlockuserModel(JSON(datajson))
                            blockusers.saveUserInfo()
                            blockusers.loadUserInfo()
                            completion(true, data)
                    }
                }
            }
        }
    }
    
    class func chatRequest(receiver_id: String, requestType: String,msg: String = "", completion :  @escaping (_ success: Bool,_ msg : String?,_ noti_flag:  Int?) -> ()) {
        let params = ["sender_id" : thisuser!.id!, "sender_name":thisuser!.username,"sender_avatar":thisuser!.userAvatar,"sender_age":thisuser!.currentAge,"sender_gender":thisuser!.gender,"sender_kinkrole":thisuser!.kinkRole,"sender_location":thisuser!.location,"sender_pronoun":thisuser!.pronoun, "receiver_id": receiver_id, "requestType":requestType, "msg": msg] as [String : Any]
        Alamofire.request(CHATREQUEST, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(true, nil, nil)
                case .success(let data):
                    let datajson = JSON(data as Any)
                    let msg = datajson["message"].stringValue
                    let send_noti_flag = datajson["send_noti_flag"].intValue
                    completion(true, msg, send_noti_flag)
            }
        }
    }
    
    class func setRecordingEnd_CheckOtherStatus(receiver_id: String,owner: String ,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["sender_id" : thisuser!.id ?? "123456789", "receiver_id": receiver_id, "owner": owner] as [String : Any]
        Alamofire.request(SETRECORDINGEND_OTHERERSTATUS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    completion(true, data)
            }
        }
    }
    
    class func getMembershipInfo(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.id ?? "123456789"] as [String : Any]
        Alamofire.request(GETMEMBERSHIPINFO, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    completion(true, data)
            }
        }
    }
    
    class func delecteAccount(completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: "https://api.datingkinky.com/users/" + thisuser!.id!)
        var request = URLRequest(url: fileUrl!)
        request.httpMethod = "DELETE"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    if JSON(data)["statusCode"].intValue == 200{
                        completion(true, "Success")
                    }else{
                        let error = JSON(data)["error"].stringValue
                        completion(true, error)
                    }
                    
            }
        }
    }
    
    class func report(reason: String, reasonDetails: String,attachments: [String] , completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["item" : thisuser!.id ?? "123456789","purpose": "User", "reason":reason, "reasonDetails": reasonDetails ,"attachments": attachments] as [String : Any]
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: REPORT)
        var request = URLRequest(url: fileUrl!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    completion(true, data)
            }
        }
    }
    
    class func setBlockParameters(isblock_video_verify: String,isblock_chat_request: String,snooze_account: String,isblock_random_chat: String,isblock_video_chat: String,isblock_audio_chat: String ,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.id ?? "0", "isblock_video_verify": isblock_video_verify, "isblock_chat_request": isblock_chat_request, "snooze_account": snooze_account, "isblock_random_chat": isblock_random_chat, "isblock_video_chat": isblock_video_chat, "isblock_audio_chat": isblock_audio_chat] as [String : Any]
        Alamofire.request(SETBLOCK_PARAMETERS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let datajson = JSON(data as Any)["user_info"].object
                    block_parameters!.clearUserInfo()
                    print(JSON(datajson))
                    block_parameters = BlockParameterModel(JSON(datajson))
                    block_parameters!.saveUserInfo()
                    block_parameters!.loadUserInfo()
                    blockusers.clearUserInfo()
                    blockusers = BlockuserModel(JSON(datajson))
                    blockusers.saveUserInfo()
                    blockusers.loadUserInfo()
                    completion(true, data)
            }
        }
    }
    
    class func addBlockUsers(blocks: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.id ?? "0", "blocks": blocks] as [String : Any]
        Alamofire.request(ADD_BLOCKUSERS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let datajson = JSON(data as Any)["user_info"].object
                    
                    blockusers.clearUserInfo()
                    blockusers = BlockuserModel(JSON(datajson))
                    blockusers.saveUserInfo()
                    blockusers.loadUserInfo()
                    completion(true, data)
                    
            }
        }
    }
    
    class func getBlockUsers(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.id ?? "0"] as [String : Any]
        Alamofire.request(GET_BLOCKUSERS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let datajson = JSON(data as Any)["user_info"].object
                    blockusers.clearUserInfo()
                    blockusers = BlockuserModel(JSON(datajson))
                    blockusers.saveUserInfo()
                    blockusers.loadUserInfo()
                    completion(true, data)
                    
            }
        }
    }
    
    class func removeBlockUsers(blocks: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.id ?? "0", "blocks": blocks] as [String : Any]
        Alamofire.request(REMOVE_BLOCKUSERS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let datajson = JSON(data as Any)["user_info"].object
                    blockusers.clearUserInfo()
                    blockusers = BlockuserModel(JSON(datajson))
                    blockusers.saveUserInfo()
                    blockusers.loadUserInfo()
                    completion(true, data)
            }
        }
    }
    
    class func getMyProfile(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: "https://api.datingkinky.com/me")
        var request = URLRequest(url: fileUrl!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    completion(true, data)
            }
        }
    }
    
    class func purchaseMembership(gift_userid: String = thisuser!.id ?? "0", subsType: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let x_auth: String! = UserDefault.getString(key: PARAMS.X_AUTH_TOKEN, defaultValue: "")
        let fileUrl = Foundation.URL(string: "https://api.datingkinky.com/subscriptions/purchase")
        var request = URLRequest(url: fileUrl!)
        let params = ["userId" : gift_userid,"subsType": subsType] as [String : Any]
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = [ "Authorization":  x_auth!]
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    // update membersip info  if success
                    let json = JSON(data as Any)
                    let msg = json["msg"].stringValue.removeWhitespace()
                    let subscription_success = "Subscription Successfully Purchased!"
                    if msg == subscription_success.removeWhitespace(){
                        thisuser!.loadUserInfo()
                        UserDefault.setBool(key: PARAMS.HAS_PLUS_MEMBERSHIP, value: true)
                        thisuser!.loadUserInfo()
                        thisuser!.saveUserInfo()
                        let datajson = JSON(json["data"].object)
                        let str_date = datajson["Str_Date"].stringValue
                        let exp_date = datajson["Exp_Date"].stringValue
                        usermembership.loadUserInfo()
                        UserDefault.setString(key: PARAMS.TIRAL_START, value: str_date)
                        UserDefault.setString(key: PARAMS.TRIAL_END, value: exp_date)
                        usermembership.loadUserInfo()
                        usermembership.saveUserInfo()
                        completion(true, msg)
                    }else{
                        completion(false, msg)
                    }
                    
            }
        }
    }
}
