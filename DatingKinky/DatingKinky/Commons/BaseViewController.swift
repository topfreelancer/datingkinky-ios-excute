//
//  BaseViewController.swift
//  DatingKinky
//
//  Created by top Dev on 09.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showPromotion),name:.show_20mins_promotion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(show2daysbeforePromotion),name:.show_2days_before_promotion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showlastdayPromotion),name:.show_last_day_promotion, object: nil)
        // random chatting part
        //NotificationCenter.default.addObserver(self, selector: #selector(showRandomPopDialog),name: .randomChattingPopDialog, object: nil)
        // general chatting request received
        NotificationCenter.default.addObserver(self, selector: #selector(generalChatRequestReceived),name: .generalChatRequestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoChatRequestReceived),name: .videoChatRequestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(voiceChatRequestReceived),name: .voiceChatRequestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoChatRequestAccepted),name: .videoChatRequestAccepted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(voiceChatRequestAccepted),name: .voiceChatRequestAccepted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoVerifyRequestReceived),name: .videoVerifyRequestReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(videoVerifyRequestAccepted),name: .videoVerifyRequestAccepted, object: nil)
       
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func showPromotion(){
        self.showBottomDlg(storyboard_name: StoryBoards.PLUS, vcname: "FirstDayPromotionVC")
    }
    @objc func show2daysbeforePromotion(){
        self.showBottomDlg(storyboard_name: StoryBoards.PLUS, vcname: "Last2DayPromotionVC")
    }
    @objc func showlastdayPromotion(){
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: "LastDayPromotionVC")
    }
    @objc func showRandomPopDialog()  {
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: "RandomRequestPop")
    }
    @objc func generalChatRequestReceived()  {
        /*let storyboad = UIStoryboard(name: StoryBoards.MAIN, bundle: nil)
        let chatVC = storyboad.instantiateViewController(withIdentifier: VCs.CHAT)
        UIApplication.shared.keyWindow?.rootViewController = chatVC*/
        self.gotoTabIndex(0)
    }
    
    @objc func videoChatRequestReceived()  {
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.VIDEO_CHAT_REQUEST_ACCEPT_POP_VC)
    }
    
    @objc func voiceChatRequestReceived()  {
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.VOICE_CHAT_REQUEST_ACCEPT_POP_VC)
    }
    
    @objc func videoChatRequestAccepted()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: "VideoChatViewController") as! VideoChatViewController
        var roomId = "video_default"
        if let userid = thisuser!.id , let friendid = friendID{
            if userid > friendid{
                roomId = userid + "_" + friendid
            }else{
                roomId = friendid + "_" + userid
            }
        }
        toVC.roomID = "video_" + roomId
        print("this is room id for video chattingroom ===>", toVC.roomID)
        let navViewController = UINavigationController(rootViewController: toVC)
        navViewController.modalPresentationStyle = .fullScreen
        self.present(navViewController, animated: false, completion: nil)
    }
    
    @objc func voiceChatRequestAccepted()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: "VoiceChatViewController") as! VoiceChatViewController
        var roomId = "voice_default"
        if let userid = thisuser!.id , let friendid = friendID{
            if userid > friendid{
                roomId = userid + "_" + friendid
            }else{
                roomId = friendid + "_" + userid
            }
        }
        toVC.roomID = "voice_" + roomId
        print("this is room id for voice chattingroom ===>", toVC.roomID)
        toVC.str_profile = partnerAvatar4ChatRequest
        //self.present(toVC, animated: false, completion: nil)
        let navViewController = UINavigationController(rootViewController: toVC)
        navViewController.modalPresentationStyle = .fullScreen
        self.present(navViewController, animated: false, completion: nil)
    }
    
    @objc func videoVerifyRequestReceived()  {
        /*let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: "VideoVerifyAcceptedNav")
        self.present(toVC, animated: true, completion: nil)*/
        //UIApplication.shared.keyWindow?.rootViewController = toVC
        dismiss_flag = false
        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.VIDEOVERIFY_ACCEPTVC)
        /*videoVerifyAcceptPopUpVC = nil
        dark1 = nil
        
        animVC = .videoVerifyAcceptPop
        videoVerifyAcceptPopUpVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.VIDEOVERIFY_ACCEPTVC) as? VideoVerifyAcceptPopUpVC)
        if let videoVerifyAcceptPopUpVC = videoVerifyAcceptPopUpVC{
            videoVerifyAcceptPopUpVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
        if let dark1 = dark1{
            dark1.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(dark1.view)
            self.addChild(dark1)
        }
        UIView.animate(withDuration: 0.5, animations: {
            if let videoVerifyAcceptPopUpVC = videoVerifyAcceptPopUpVC{
                videoVerifyAcceptPopUpVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(videoVerifyAcceptPopUpVC.view)
                self.addChild(videoVerifyAcceptPopUpVC)
            }
        })*/
    }
    
    @objc func videoVerifyRequestAccepted()  {
        // anything else don't work
    }
    
    func showBottomDlg(storyboard_name: String, vcname: String)  {
        let configuration = NBBottomSheetConfiguration(animationDuration: 0.4, sheetSize: .fixed(Constants.SCREEN_HEIGHT))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        let storyboad = UIStoryboard(name: storyboard_name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: vcname)
        bottomSheetController.present(targetVC, on: self)
    }
    
    func gotoTabIndex(_ index: Int)  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = index
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
}
