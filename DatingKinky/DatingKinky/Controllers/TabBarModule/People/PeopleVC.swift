//
//  PeopleVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import AORangeSlider
import MultiSlider

class PeopleVC: BaseVC {
    @IBOutlet weak var imv_title: UIImageView!
    @IBOutlet weak var imv_thumb: UIImageView!
    
    @IBOutlet weak var btn_space: UIButton!
    @IBOutlet weak var col_peopleShow: UICollectionView!
    
    @IBOutlet weak var uiv_dropDownfilter: UIView!
    
    @IBOutlet weak var cus_distance: MSDropDown!
    @IBOutlet weak var cus_gender: MSDropDown!
    @IBOutlet weak var cus_kinkRole: MSDropDown!
    
    //@IBOutlet weak var multiSlider: MultiSlider!
    @IBOutlet weak var uiv_slider: UIView!
    @IBOutlet weak var edt_userName: UITextField!
    
    // getting chat request has been sent already
    var ds_allChats1 = [AllChatModel]()
    var available_ids = [String]()
    var userlistHandle: UInt?
    
    var skip = 0
    var filterskip = 0
    var str_search_name: String?
    var str_search_distance: String?
    var str_search_gender: [String]?
    var str_search_kinkRole: [String]?
    var int_search_minAge: Int! = 18
    var int_search_maxAge: Int! = 100
    var int_search_distance: Int?
    var isfilterByUserNameState: Bool!
    var gender_Options = [KeyValueModel]()
    var kinkRole_Options = [KeyValueModel]()
    var pronouns_Options = [KeyValueModel]()
    let distance_Options : [KeyValueModel] = [KeyValueModel(key: "0", value: "Clear"),
    KeyValueModel(key: "1", value: "0-10 miles"),
    KeyValueModel(key: "2", value: "0-25 miles"),
    KeyValueModel(key: "3", value: "0-50 miles"),
    KeyValueModel(key: "4", value: "0-100 miles"),
    KeyValueModel(key: "5", value: "0-250 miles"),
    KeyValueModel(key: "6", value: "0-500 miles"),
    KeyValueModel(key: "7", value: "Worldwide")]
    
    var ds_peoples = [PeopleModel]()
    var isThumb: Bool = false
    var dropDownFiterShown: Bool = false
    //var CategoriesList = [ServiceModel]()
    var cellHeight = 40
    var isrequesting = false
    var isFilterrequesting = false
    var isFilterrequesting4EndEdit = false
    var isfilter_success = false
    // MARK: setting filter state for setting skip number when user pull to refresh
    var isFilterState = false
    // MARK: END
    
    var isFirstTime4Slider = true
    
    // refresh control
    var footerView:CustomFooterView?
    var items = [Int]()
    var isLoading:Bool = false
    let footerViewReuseIdentifier = "RefreshFooterView"
    // MARK: setting loading view true show, false hide for normal state
    var firstTime = true
    // MARK: END
    // MARK: setting loading view true show, false hide for filter state
    var firstTime4Filter = true
    // MARK: END
    var lastMinAge = 18
    var lastMaxAge = 100
    
    var isfirstime4gender = true
    var isfirstime4kinkRole = true
    var isfirstime4Distance = true
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addNavBarImage()
        self.addBackButtonNavBar()
        self.setViewIcon()
        self.setSwipe()
        self.setDropDownDataSource()
        self.setUserNameFilter()
        self.edt_userName.delegate = self
        self.col_peopleShow.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        self.createSlider()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideLoadingView()
        self.isfilter_success = false
        self.setDropDownFilter(false)
        self.isfilterByUserNameState = false
        self.isFirstTime4Slider = true
        self.lastMinAge = 18
        self.lastMaxAge = 100
        self.isfirstime4kinkRole = true
        self.isfirstime4gender = true
        self.isfirstime4Distance = true
        self.isrequesting = false
        self.isFilterrequesting = false
        self.firstTime = true
        // register bottom refresh item
        self.skip = 0
        ApiManager.getBlockUsers { (isSuccess, data) in
            let chatRequest = "u" + "\(thisuser!.id ?? "")"
            self.userlistListner(chatRequest)
            self.ds_peoples.removeAll()
            self.getDataSource(self.skip)
        }
    }
    
    func createSlider() {
        let horizontalMultiSlider = MultiSlider()
        horizontalMultiSlider.orientation = .horizontal
        horizontalMultiSlider.minimumValue = 18
        horizontalMultiSlider.maximumValue = 100
        horizontalMultiSlider.outerTrackColor = .lightGray
        horizontalMultiSlider.value = [18, 100]
        horizontalMultiSlider.valueLabelPosition = NSLayoutConstraint.Attribute(rawValue: 10)!
        if #available(iOS 11.0, *) {
            horizontalMultiSlider.tintColor = UIColor.init(named: "color_navbar_top")
        } else {
            horizontalMultiSlider.tintColor = UIColor.blue
        }
        horizontalMultiSlider.trackWidth = 2
        horizontalMultiSlider.snapStepSize = 1
        horizontalMultiSlider.showsThumbImageShadow = false
        self.uiv_slider.addConstrainedSubview(horizontalMultiSlider, constrain: .leading, .top, .bottom, .trailing)
        horizontalMultiSlider.keepsDistanceBetweenThumbs = true
        horizontalMultiSlider.distanceBetweenThumbs = 5
        horizontalMultiSlider.addTarget(self, action: #selector(sliderDragEnded(_:)), for: . touchUpInside)
    }
    
    @objc func sliderDragEnded(_ slider: MultiSlider) {
        //print("thumb \(slider.draggedThumbIndex) moved")
        //print("now minage are at \(slider.value.first ?? 18)")
        //print("now maxage are at \(slider.value.last ?? 100)")
        
        if let minage = slider.value.first, let maxage = slider.value.last{
            self.int_search_minAge = Int(minage)
            self.int_search_maxAge = Int(maxage)
            if lastMinAge != int_search_minAge || lastMaxAge != int_search_maxAge{
                // call api with search condition
                self.ds_peoples.removeAll()
                self.isFilterState = true //
                self.firstTime4Filter = true
                self.getDataSourceWithFilter(0)
                self.setDropDownFilter(false)
            }
        }
        self.lastMinAge = self.int_search_minAge
        self.lastMaxAge = self.int_search_maxAge
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let id = thisuser?.id,let userlistHandle = userlistHandle {
            FirebaseAPI.removeUserlistObserver(userRoomid: "u" + id, userlistHandle)
        }
    }
    
    func setUserNameFilter(){
        setEdtPlaceholder(edt_userName, placeholderText: "Input username", placeColor: UIColor.black, padding: .left(10))
    }
    
    func setDropDownDataSource() {
        self.gender_Options.removeAll()
        self.kinkRole_Options.removeAll()
        self.pronouns_Options.removeAll()
       
        var genderNum = 0
        var kinkRoleNum = 0
        var pronounsNum = 0
        
        for one in DS_genders{
            genderNum += 1
            self.gender_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
            if genderNum == DS_genders.count{
                
                for one in DS_kinkRoles{
                    kinkRoleNum += 1
                    self.kinkRole_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                    if kinkRoleNum == DS_kinkRoles.count{
                        
                        for one in DS_pronouns{
                            pronounsNum += 1
                            self.pronouns_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                            if pronounsNum == DS_pronouns.count{
                                //self.hideLoadingView()
                                self.setUpDropDownFilter()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setUpDropDownFilter() {
        self.cus_gender.keyvalueCount = self.gender_Options.count
        self.cus_gender.delegate = self
        self.cus_gender.keyValues = self.gender_Options
        self.cus_gender.isMultiSelect = true
        self.cus_gender.tag = searchDropDownTag.gender.rawValue
        self.cus_kinkRole.keyvalueCount = self.kinkRole_Options.count
        self.cus_kinkRole.delegate = self
        self.cus_kinkRole.keyValues = self.kinkRole_Options
        self.cus_kinkRole.isMultiSelect = true
        self.cus_kinkRole.tag = searchDropDownTag.kinkRole.rawValue
        self.cus_distance.keyvalueCount = self.distance_Options.count
        self.cus_distance.delegate = self
        self.cus_distance.keyValues = self.distance_Options
        self.cus_distance.isMultiSelect = false
        self.cus_distance.tag = searchDropDownTag.distance.rawValue
    }
    
    func setDropDownFilter(_ status:Bool) {
        self.btn_space.isHidden = !status
        self.dropDownFiterShown = status
        if status{
            self.uiv_dropDownfilter.isHidden = false
        }else{
            self.uiv_dropDownfilter.isHidden = true
        }
    }
    
    func setSwipe()  {
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(topSwipe(_:)))
        upSwipe.direction = .up
        self.uiv_dropDownfilter.addGestureRecognizer(upSwipe)
    }
    
    @objc func topSwipe(_ sender:UISwipeGestureRecognizer) {
        self.setDropDownFilter(false)
    }
    
    func getDataSource(_ skip: Int)  {
        if !isrequesting{
            self.isrequesting = true
            if firstTime{
                self.showLoadingView(vc: self)
            }
            
            ApiManager.getsiteUsers(status: nil, dialingCode: nil, phoneNumberVerified: nil, plusMembers: nil, role: nil, username: nil, emailConfirmed: nil, accountType: nil, onlyDeleted: nil, updatedAt: nil, skip: skip, limit: 20, sort: nil) { (isSuccess, data) in
                if self.firstTime{
                    self.hideLoadingView()
                }
                self.firstTime = false
                if isSuccess{
                    //print(data as Any)
                    //self.ds_peoples.removeAll()
                    let json = JSON(data as Any)
                    //print(json)
                    let dataobjectArr = json["data"].arrayObject
                    if let arr = dataobjectArr{
                        var num = 0
                        for one in arr{
                            
                            num += 1
                            //print(arr.count)
                            //print(num)
                            let onePeople = JSON(one as Any)
                            let id = onePeople["_id"].stringValue
                            if self.getBlockStatus(id){
                                continue
                            }
                            let userName = onePeople["username"].stringValue
                            let realName = onePeople["displayName"].stringValue
                            let email = onePeople["email"].stringValue
                            let currentAge = onePeople["currentAge"].stringValue
                            let isPlusMember = onePeople["hasPlusMembership"].boolValue
                            let lastActive = onePeople["lastActive"].stringValue
                            var timeAgo = ""
                            let now = Date()
                            let seconds = TimeZone.current.secondsFromGMT()
                            _ = seconds / 3600
                            let calendar = Calendar.current
                            if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                                //print(lastDate as Any)
                                let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                                let seconds = ageComponents.second ?? 0
                                //print(seconds)
                                let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                                let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                                let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                                let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                                if secondss > 0{
                                    timeAgo = "\(secondss)" + "s"
                                }
                                if min > 0{
                                    timeAgo = "\(min)" + "m"
                                }
                                if hour > 0{
                                    timeAgo = "\(hour)" + "h"
                                }
                                if day > 0{
                                    timeAgo = "\(day)" + "d"
                                }
                            }
                            
                            let profile = onePeople["profile"].object
                            let profilejson = JSON(profile as Any)
                            let location = profilejson["location"].stringValue
                            let genderObject = profilejson["gender"].object
                            let genderjson = JSON(genderObject as Any)
                            let gender = genderjson["lowerCaseName"].stringValue
                            let kinkRoleObject = profilejson["kinkRole"].object
                            let kinkRolejson = JSON(kinkRoleObject as Any)
                            let kinkRole = kinkRolejson["name"].stringValue
                            
                            let pronounObject = profilejson["pronoun"].object
                            let pronounjson = JSON(pronounObject as Any)
                            let pronoun = pronounjson["name"].stringValue
                            
                            let matchingObject = profilejson["matching"].object
                            let matchingjson = JSON(matchingObject as Any)
                            let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                            
                            var lookingForgenders = [String]()
                            if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                                lookingForgenders.removeAll()
                                for one in lookingForgenderObjectArr{
                                    let jsonOne = JSON(one)
                                    lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            let orientationsObjectArr = matchingjson["orientations"].arrayObject
                            
                            var orientations = [String]()
                            if let orientationsObjectArr = orientationsObjectArr{
                                orientations.removeAll()
                                for one in orientationsObjectArr{
                                    let jsonOne = JSON(one)
                                    orientations.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                            
                            var relationshipStatus = [String]()
                            if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                                relationshipStatus.removeAll()
                                for one in relationShipStatusObjectArr{
                                    let jsonOne = JSON(one)
                                    relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                            
                            var relationshipStyles = [String]()
                            if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                                relationshipStyles.removeAll()
                                for one in relationShipStyleObjectArr{
                                    let jsonOne = JSON(one)
                                    relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            let hereForObjectArr = matchingjson["hereFor"].arrayObject
                            
                            var hereFor = [String]()
                            if let hereForObjectArr = hereForObjectArr{
                                hereFor.removeAll()
                                for one in hereForObjectArr{
                                    let jsonOne = JSON(one)
                                    hereFor.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                            
                            var kinkRoles = [String]()
                            if let hereForObjectArr = kinkroleObjectArr{
                                kinkRoles.removeAll()
                                for one in hereForObjectArr{
                                    let jsonOne = JSON(one)
                                    kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                                }
                            }
                            
                            var identifies = [String]()
                            let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                            if let identifiesArr = identifiesAsArr{
                                for one in identifiesArr{
                                    identifies.append(JSON(one)["name"].stringValue)
                                }
                            }
                            
                            let avatar = onePeople["avatar"].object
                            let avatarjson = JSON(avatar as Any)
                            let file = avatarjson["file"].object
                            let filejson = JSON(file as Any)
                            let url = filejson["url"].object
                            let urljson = JSON(url as Any)
                            let useravatar = urljson["smallPhotoURL"].stringValue
                            self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole: kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false, lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                            
                            if num == arr.count{
                                self.col_peopleShow.reloadData()
                                self.isLoading = false
                                self.isrequesting = false
                                self.isfilter_success = false
                            }
                        }
                    }else{
                        self.isLoading = false
                        self.isrequesting = false
                        self.isfilter_success = false
                    }
                }else{
                    self.isLoading = false
                    self.isrequesting = false
                    self.isfilter_success = false
                }
            }
        }
    }
    
    func getBlockStatus(_ id: String) -> Bool {
        var status = false
        if let blocks = blockusers.getBlockUsers_BlockedUsers_SnoozedUsers(){
            if blocks.count > 0{
                for one in blocks{
                    if one == id{
                        status = true
                        break
                    }
                }
            }
        }
        return status
    }
    
    func getDataSourceWithFilter(_ skip: Int)  {
        if !isFilterrequesting{
            self.isFilterrequesting = true
            
            var kinkRoles: [String]?
            var genders: [String]?
            
            if let filter_useranme = self.edt_userName.text{
                
                if filter_useranme != ""{
                    self.showLoadingView(vc: self)
                    ApiManager.filterByUserName(username: filter_useranme) { (isSuccess, data) in
                        self.hideLoadingView()
                        if isSuccess{
                            //print(data as Any)
                            //self.ds_peoples.removeAll()
                            let json = JSON(data as Any)
                            //print(json)
                            let dataobjectArr = json["data"].arrayObject
                            if let arr = dataobjectArr{
                                
                                var num = 0
                                for one in arr{
                                    num += 1
                                    let onePeople = JSON(one as Any)
                                    let id = onePeople["_id"].stringValue
                                    if self.getBlockStatus(id){
                                        continue
                                    }
                                    let userName = onePeople["username"].stringValue
                                    let realName = onePeople["displayName"].stringValue
                                    let email = onePeople["email"].stringValue
                                    let currentAge = onePeople["currentAge"].stringValue
                                    let isPlusMember = onePeople["hasPlusMembership"].boolValue
                                    let lastActive = onePeople["lastActive"].stringValue
                                    var timeAgo = ""
                                    let now = Date()
                                    print(now)
                                    let calendar = Calendar.current
                                    if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                                        print(lastDate as Any)
                                        let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                                        let seconds = ageComponents.second ?? 0
                                        print(seconds)
                                        let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                                        let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                                        let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                                        let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                                        if secondss > 0{
                                            timeAgo = "\(secondss)" + "s"
                                        }
                                        if min > 0{
                                            timeAgo = "\(min)" + "m"
                                        }
                                        if hour > 0{
                                            timeAgo = "\(hour)" + "h"
                                        }
                                        if day > 0{
                                            timeAgo = "\(day)" + "d"
                                        }
                                    }
                                    
                                    
                                    let profile = onePeople["profile"].object
                                    let profilejson = JSON(profile as Any)
                                    
                                    let location = profilejson["location"].stringValue
                                    let genderObject = profilejson["gender"].object
                                    let genderjson = JSON(genderObject as Any)
                                    let gender = genderjson["lowerCaseName"].stringValue
                                    let kinkRoleObject = profilejson["kinkRole"].object
                                    let kinkRolejson = JSON(kinkRoleObject as Any)
                                    let kinkRole = kinkRolejson["name"].stringValue
                                    
                                    let pronounObject = profilejson["pronoun"].object
                                    let pronounjson = JSON(pronounObject as Any)
                                    let pronoun = pronounjson["name"].stringValue
                                    
                                    let matchingObject = profilejson["matching"].object
                                    let matchingjson = JSON(matchingObject as Any)
                                    let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                                    
                                    var lookingForgenders = [String]()
                                    if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                                        lookingForgenders.removeAll()
                                        for one in lookingForgenderObjectArr{
                                            let jsonOne = JSON(one)
                                            lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let orientationsObjectArr = matchingjson["orientations"].arrayObject
                                    
                                    var orientations = [String]()
                                    if let orientationsObjectArr = orientationsObjectArr{
                                        orientations.removeAll()
                                        for one in orientationsObjectArr{
                                            let jsonOne = JSON(one)
                                            orientations.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                                    
                                    var relationshipStatus = [String]()
                                    if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                                        relationshipStatus.removeAll()
                                        for one in relationShipStatusObjectArr{
                                            let jsonOne = JSON(one)
                                            relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                                    
                                    var relationshipStyles = [String]()
                                    if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                                        relationshipStyles.removeAll()
                                        for one in relationShipStyleObjectArr{
                                            let jsonOne = JSON(one)
                                            relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let hereForObjectArr = matchingjson["hereFor"].arrayObject
                                    
                                    var hereFor = [String]()
                                    if let hereForObjectArr = hereForObjectArr{
                                        hereFor.removeAll()
                                        for one in hereForObjectArr{
                                            let jsonOne = JSON(one)
                                            hereFor.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                                    
                                    var kinkRoles = [String]()
                                    if let hereForObjectArr = kinkroleObjectArr{
                                        kinkRoles.removeAll()
                                        for one in hereForObjectArr{
                                            let jsonOne = JSON(one)
                                            kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    var identifies = [String]()
                                    let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                                    if let identifiesArr = identifiesAsArr{
                                        for one in identifiesArr{
                                            identifies.append(JSON(one)["name"].stringValue)
                                        }
                                    }
                                    
                                    let avatar = onePeople["avatar"].object
                                    let avatarjson = JSON(avatar as Any)
                                    let file = avatarjson["file"].object
                                    let filejson = JSON(file as Any)
                                    let url = filejson["url"].object
                                    let urljson = JSON(url as Any)
                                    let useravatar = urljson["smallPhotoURL"].stringValue
                                                             
                                    //print("one paramers  ==== > \(userName) \n \(realName) \n \(email) \n \(currentAge) \n \(isPlusMember) \n \(location) \n \(gender) \n \(useravatar)")
                                    //1
                                    self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole:kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false, lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                                    
                                    if num == arr.count{
                                        self.col_peopleShow.reloadData()
                                        self.isfilterByUserNameState = true
                                        self.isLoading = false
                                        self.isFilterrequesting = false
                                    }
                                }
                            }else{
                                self.col_peopleShow.reloadData()
                                self.isLoading = false
                                self.isFilterrequesting = false
                                self.isfilter_success = false
                            }
                        }else{
                            self.col_peopleShow.reloadData()
                            self.isLoading = false
                            self.isFilterrequesting = false
                            self.isfilter_success = false
                        }
                    }
                }else{
                    if let str_search_kinkRole = str_search_kinkRole{
                        if str_search_kinkRole.count != 0{
                            kinkRoles = str_search_kinkRole
                        }else{
                            kinkRoles = nil
                        }
                        
                    }else{
                        kinkRoles = nil
                    }
                    
                    if let str_search_gender = str_search_gender{
                        if str_search_gender.count != 0{
                            genders = str_search_gender
                        }else{
                            genders = nil
                        }
                        
                    }else{
                        genders = nil
                    }
                    
                    if firstTime4Filter{
                        self.showLoadingView(vc: self)
                    }
                    
                    ApiManager.filterUsers(username: "" , kinkRoles: kinkRoles , genders: genders , minAge: self.int_search_minAge ?? 18, maxAge: self.int_search_maxAge ?? 100, skip: skip,distance:self.int_search_distance ?? 0 ) { (isSuccess, data) in
                        if self.firstTime4Filter{
                            self.hideLoadingView()
                        }
                        self.firstTime4Filter = false
                        if isSuccess{
                            //print(data as Any)
                            //self.ds_peoples.removeAll()
                            let json = JSON(data as Any)
                            //print(json)
                            let dataobjectArr = json["data"].arrayObject
                            if let arr = dataobjectArr{
                                
                                var num = 0
                                for one in arr{
                                    num += 1
                                    let onePeople = JSON(one as Any)
                                    let id = onePeople["_id"].stringValue
                                    if self.getBlockStatus(id){
                                        continue
                                    }
                                    let userName = onePeople["username"].stringValue
                                    let realName = onePeople["displayName"].stringValue
                                    let email = onePeople["email"].stringValue
                                    let currentAge = onePeople["currentAge"].stringValue
                                    let isPlusMember = onePeople["hasPlusMembership"].boolValue
                                    let lastActive = onePeople["lastActive"].stringValue
                                    var timeAgo = ""
                                    let now = Date()
                                    let calendar = Calendar.current
                                    if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                                        print(lastDate as Any)
                                        let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                                        let seconds = ageComponents.second ?? 0
                                        print(seconds)
                                        let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                                        let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                                        let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                                        let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                                        if secondss > 0{
                                            timeAgo = "\(secondss)" + "s"
                                        }
                                        if min > 0{
                                            timeAgo = "\(min)" + "m"
                                        }
                                        if hour > 0{
                                            timeAgo = "\(hour)" + "h"
                                        }
                                        if day > 0{
                                            timeAgo = "\(day)" + "d"
                                        }
                                    }
                                    let profile = onePeople["profile"].object
                                    let profilejson = JSON(profile as Any)
                                    
                                    let location = profilejson["location"].stringValue
                                    let genderObject = profilejson["gender"].object
                                    let genderjson = JSON(genderObject as Any)
                                    let gender = genderjson["lowerCaseName"].stringValue
                                    let kinkRoleObject = profilejson["kinkRole"].object
                                    let kinkRolejson = JSON(kinkRoleObject as Any)
                                    let kinkRole = kinkRolejson["name"].stringValue
                                    
                                    let pronounObject = profilejson["pronoun"].object
                                    let pronounjson = JSON(pronounObject as Any)
                                    let pronoun = pronounjson["name"].stringValue
                                    
                                    let matchingObject = profilejson["matching"].object
                                    let matchingjson = JSON(matchingObject as Any)
                                    let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                                    
                                    var identifies = [String]()
                                    let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                                    if let identifiesArr = identifiesAsArr{
                                        for one in identifiesArr{
                                            identifies.append(JSON(one)["name"].stringValue)
                                        }
                                    }
                                    
                                    var lookingForgenders = [String]()
                                    if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                                        lookingForgenders.removeAll()
                                        for one in lookingForgenderObjectArr{
                                            let jsonOne = JSON(one)
                                            lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let orientationsObjectArr = matchingjson["orientations"].arrayObject
                                    
                                    var orientations = [String]()
                                    if let orientationsObjectArr = orientationsObjectArr{
                                        orientations.removeAll()
                                        for one in orientationsObjectArr{
                                            let jsonOne = JSON(one)
                                            orientations.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                                    
                                    var relationshipStatus = [String]()
                                    if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                                        relationshipStatus.removeAll()
                                        for one in relationShipStatusObjectArr{
                                            let jsonOne = JSON(one)
                                            relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                                    
                                    var relationshipStyles = [String]()
                                    if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                                        relationshipStyles.removeAll()
                                        for one in relationShipStyleObjectArr{
                                            let jsonOne = JSON(one)
                                            relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let hereForObjectArr = matchingjson["hereFor"].arrayObject
                                    
                                    var hereFor = [String]()
                                    if let hereForObjectArr = hereForObjectArr{
                                        hereFor.removeAll()
                                        for one in hereForObjectArr{
                                            let jsonOne = JSON(one)
                                            hereFor.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                                    
                                    var kinkRoles = [String]()
                                    if let hereForObjectArr = kinkroleObjectArr{
                                        kinkRoles.removeAll()
                                        for one in hereForObjectArr{
                                            let jsonOne = JSON(one)
                                            kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                                        }
                                    }
                                    
                                    let avatar = onePeople["avatar"].object
                                    let avatarjson = JSON(avatar as Any)
                                    let file = avatarjson["file"].object
                                    let filejson = JSON(file as Any)
                                    let url = filejson["url"].object
                                    let urljson = JSON(url as Any)
                                    let useravatar = urljson["smallPhotoURL"].stringValue
                                                             
                                    //print("one paramers  ==== > \(userName) \n \(realName) \n \(email) \n \(currentAge) \n \(isPlusMember) \n \(location) \n \(gender) \n \(useravatar)")
                                    //2
                                    self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole:kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false,lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                                    
                                    if num == arr.count{
                                        self.col_peopleShow.reloadData()
                                        self.isfilterByUserNameState = false
                                        self.isLoading = false
                                        self.isFilterrequesting = false
                                        self.isfilter_success = false
                                    }
                                }
                            }else{
                                self.col_peopleShow.reloadData()
                                self.isLoading = false
                                self.isFilterrequesting = false
                                self.isfilter_success = false
                            }
                        }else{
                            self.col_peopleShow.reloadData()
                            self.isLoading = false
                            self.isFilterrequesting = false
                            self.isfilter_success = false
                        }
                    }
                }
            }
        }
    }
    
    func setViewIcon() {
        if isThumb{
            self.imv_thumb.isHidden = false
            self.imv_title.isHidden = true
        }else{
            self.imv_thumb.isHidden = true
            self.imv_title.isHidden = false
        }
    }
    
    @IBAction func viewBtnClicked(_ sender: Any) {
        if isThumb{
            self.isThumb = false
            self.setViewIcon()
        }else{
            self.isThumb = true
            self.setViewIcon()
        }
        self.col_peopleShow.reloadData()
    }
    
    @IBAction func filterBtnClicked(_ sender: Any) {
        self.dropDownFiterShown = !self.dropDownFiterShown
        self.setDropDownFilter(dropDownFiterShown)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.setDropDownFilter(false)
    }
    
    @IBAction func thumbChatBtnclicked(_ sender: Any) {
        let chatBtn = sender as! UIButton
        self.sendChatRequest(chatBtn.tag)
    }
    
    @IBAction func titleChatBtnclicked(_ sender: Any) {
        let chatBtn = sender as! UIButton
        self.sendChatRequest(chatBtn.tag)
    }
    
    func sendChatRequest(_ index: Int)  {
        selectedUser = self.ds_peoples[index]
        memberName4ChatRequest = self.ds_peoples[index].userName
        let gender = self.ds_peoples[index].gender == "woman" ? "W" : "M"
        let spaces = String(repeating: " ", count: 1)
        let one = self.ds_peoples[index]
        memberProfile4ChatRequest = one.currentAge! + gender
        if let kinkrole = one.userKinkRole, !kinkrole.isEmpty{
            memberProfile4ChatRequest! += spaces + kinkrole
        }
        if let showhim = one.pronoun, !showhim.isEmpty{
            memberProfile4ChatRequest! += spaces + "•" + spaces + showhim
        }
        if let location = one.location, !location.isEmpty{
            memberProfile4ChatRequest! += spaces + "•" + spaces + location + spaces + "•" + spaces + "Last seen"
        }
        
        partnerAge4ChatRequestAccept = self.ds_peoples[index].currentAge
        partnerAvatar4ChatRequest = self.ds_peoples[index].userAvatar
        partnerGender4ChatRequest = gender == "woman" ? "W" : "M"
        partnerKinkRole4ChatRequest = self.ds_peoples[index].userKinkRole
        partnerLocation4ChatRequest = self.ds_peoples[index].location
        partnerPronoun4ChatRequest = self.ds_peoples[index].pronoun
        friendID = self.ds_peoples[index].id
        
        self.evaluateSentStatus(self.ds_peoples[index].id!, ids: self.available_ids, index: index)
    }
    
    func evaluateSentStatus(_ id: String, ids: [String], index: Int) {
        var status = false
        if ids.count > 0{
            var num = 0
            for one in ids{
                num += 1
                if one == id{
                    status = true
                }
                if num == ids.count{
                    if status{
                        self.showToast("You have already contact with this member!")
                        ds_peoples[index].chatRequest = false
                        var indexPaths = [IndexPath]()
                        indexPaths.removeAll()
                        indexPaths.append(IndexPath.init(row: index, section: 0))
                        self.col_peopleShow.reloadItems(at: indexPaths)
                    }else{
                        self.sendChatRequest(row: index)
                    }
                }
            }
        }else{
            self.sendChatRequest(row: index)
        }
    }
    
    func sendChatRequest(row: Int) {
        self.showLoadingView(vc: self)
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        if let receiverUser = selectedUser{
            var requestObject = [String: String]()
            // MARK: for list view for my list object - - listobject
            requestObject["receiver_id"]            = receiverUser.id
            requestObject["receiver_name"]          = receiverUser.userName
            requestObject["receiver_avatar"]        = receiverUser.userAvatar
            requestObject["receiver_currentAge"]    = receiverUser.currentAge
            requestObject["receiver_gender"]        = receiverUser.gender
            requestObject["receiver_kinkRole"]      = receiverUser.userKinkRole
            requestObject["receiver_pronoun"]       = receiverUser.pronoun
            requestObject["receiver_location"]      = receiverUser.location
            requestObject["last_seen"]              = "\(timeNow)" as String
            requestObject["state"]                  = ChattingRuquest.SEND
            
            FirebaseAPI.sendChatRequestListUpdate(requestObject, myid: "u" + thisuser!.id!, partnerid: "u" + receiverUser.id!){
                (status,message) in
                if status{
                    var requestObject1 = [String: String]()
                    // MARK:  for list view for partner's list object - listobject1
                    requestObject1["receiver_id"]             = thisuser!.user?._id
                    requestObject1["receiver_name"]           = thisuser!.username
                    requestObject1["receiver_avatar"]         = thisuser!.userAvatar
                    requestObject1["receiver_currentAge"]     = thisuser!.currentAge
                    requestObject1["receiver_gender"]         = thisuser!.gender
                    requestObject1["receiver_kinkRole"]       = thisuser!.kinkRole
                    requestObject1["receiver_pronoun"]        = thisuser!.pronoun
                    requestObject1["receiver_location"]       = thisuser!.location
                    requestObject1["last_seen"]               = "\(timeNow)" as String
                    requestObject1["state"]                   = ChattingRuquest.PENDING
                    
                    FirebaseAPI.sendChatRequestListUpdate(requestObject1, myid: "u" + receiverUser.id! , partnerid: "u" + thisuser!.id!) { (isSuccess2, data2) in
                        self.hideLoadingView()
                        if isSuccess2{
                            self.showLoadingView(vc: self)
                            ApiManager.chatRequest(receiver_id: receiverUser.id!, requestType: RequestType.chat_send.rawValue) { (isSuccess3, msg,flag) in
                                self.hideLoadingView()
                                if isSuccess3{
                                    if flag == 1{
                                        self.ds_peoples[row].chatRequest = false
                                        self.col_peopleShow.reloadData()
                                        self.showBottomDlg(storyboard_name: StoryBoards.CHAT, vcname: VCs.CHAT_REQUEST_SEND)
                                    }else{
                                        self.showAlerMessage(message: msg ?? "")
                                    }
                                    
                                }
                            }
                        }else{
                            
                        }
                    }
                }
            }
        }
    }
    
    func userlistListner(_ userRoomid : String)  {
       self.ds_allChats1.removeAll()
       self.available_ids.removeAll()
            userlistHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            if let userlist = userlist{
                guard userlist.id != nil else{
                    return
                }
            }

            if let userlist = userlist{
                self.ds_allChats1.append(userlist)
            }

            if self.ds_allChats1.count != 0{
                var ids = [String]()
                for one in self.ds_allChats1{
                    ids.append(one.id!)
                }
                var num = 0
                for  one in ids.removeDuplicates(){
                    num += 1
                    if let allchat = self.getDataFromID(one){
                        self.available_ids.append(allchat.id ?? "")
                    }
                }
            }
        })
    }
    
    func getDataFromID(_ id: String) -> AllChatModel? {
        var returnModel : AllChatModel?
        for one in self.ds_allChats1{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel
    }
}

extension PeopleVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        self.ds_peoples.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.isThumb {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbViewCell", for: indexPath) as! ThumbViewCell
            if indexPath.row <= self.ds_peoples.count - 1{
                cell.entity = self.ds_peoples[indexPath.row]
            }
            cell.chatBtn.tag = indexPath.row
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleViewCell", for: indexPath) as! titleViewCell
            if indexPath.row <= self.ds_peoples.count - 1{
                cell.entity = self.ds_peoples[indexPath.row]
            }
            cell.chatBtn.tag = indexPath.row
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if !self.isThumb {
            ds_peoples[indexPath.row].chatRequest = !ds_peoples[indexPath.row].chatRequest
            var indexPaths = [IndexPath]()
            indexPaths.removeAll()
            indexPaths.append(indexPath)
            collectionView.reloadItems(at: indexPaths)
        }else{
            // goto full profileview
            selectedUser = ds_peoples[indexPath.row]
            self.gotoVC(VCs.FULLPROFILENAV)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let threshold   = 100.0
        let contentOffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let diffHeight = contentHeight - contentOffset
        let frameHeight = scrollView.bounds.size.height
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold   =  min(triggerThreshold, 0.0)
        let pullRatio  = min(abs(triggerThreshold),1.0)
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        //print("pullRatio==>",pullRatio)
        if pullRatio >= 0.01 {
            self.footerView?.animateFinal()
        }
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let diffHeight = contentHeight - contentOffset
        let frameHeight = scrollView.bounds.size.height
        let pullHeight  = abs(diffHeight - frameHeight)
        //print("pullHeight:\(pullHeight)")
        if pullHeight > 0
        {
            //print("start")
            guard let footerView = self.footerView, footerView.isAnimatingFinal else { return }
            //print("load more trigger")
            self.isLoading = true
            self.footerView?.startAnimate()
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                if !self.isFilterState{
                    self.skip += 1
                    self.getDataSource(self.skip)
                }else{
                    if !self.isfilterByUserNameState{
                        if !self.isFilterrequesting{
                            self.filterskip += 1
                            self.firstTime4Filter = false
                            self.getDataSourceWithFilter(self.filterskip)
                        }
                    }
                }
            })
        }
    }
}

extension PeopleVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.isThumb {
                let w = collectionView.frame.size.width
                let h: CGFloat = w * 1.4
                return CGSize(width: w, height: h)
        
        }else{
            let w = collectionView.frame.size.width / 2.05
            let h = w
            return CGSize(width: w, height: h)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.SCREEN_WIDTH - 2 * collectionView.frame.size.width / 2.05 - 20
    }
}

extension PeopleVC: MSDropDownDelegate{
    func dropdownSelected(tagId: Int, answer: String, value: String, isSelected: Bool) {
        switch tagId {
        case searchDropDownTag.gender.rawValue:
            if answer != ""{
                let lookinggender = answer.split(separator: "|")
                var lookingforGender = [String]()
                lookingforGender.removeAll()
                
                for one in lookinggender{
                    let stringone = String(one).trim()
                    for two in DS_genders{
                        if stringone == two.valueName{
                            lookingforGender.append(two.valueID ?? "")
                        }
                    }
                }
                self.isFilterState = true
                self.isFilterrequesting = false
                self.str_search_gender = lookingforGender
                self.firstTime4Filter = true
                self.isfirstime4gender = false
                self.ds_peoples.removeAll()
                self.getDataSourceWithFilter(0)
            }else{
                if !isfirstime4gender{
                    self.firstTime = true
                    self.ds_peoples.removeAll()
                    self.getDataSource(0)
                }
            }
            break
        case searchDropDownTag.distance.rawValue:
            if value != ""{
                if value != "0"{
                    if value == "1"{
                        self.int_search_distance = 10
                    }else if value == "2"{
                        self.int_search_distance = 25
                    }else if value == "3"{
                        self.int_search_distance = 50
                    }else if value == "4"{
                        self.int_search_distance = 100
                    }else if value == "5"{
                        self.int_search_distance = 250
                    }else if value == "6"{
                        self.int_search_distance = 500
                    }else{
                        self.int_search_distance = 0
                    }
                    self.isFilterState = true
                    self.isFilterrequesting = false
                    self.isfirstime4Distance = false
                    self.ds_peoples.removeAll()
                    self.firstTime4Filter = true
                    self.getDataSourceWithFilter(0)
                }else{
                    if !isfirstime4Distance{
                        self.firstTime = true
                        self.ds_peoples.removeAll()
                        self.getDataSource(0)
                    }
                }
            }else{
                if !isfirstime4Distance{
                    self.firstTime = true
                    self.ds_peoples.removeAll()
                    self.getDataSource(0)
                }
            }
            
            break
        case searchDropDownTag.name.rawValue:
            break
        case searchDropDownTag.kinkRole.rawValue:
            if answer != ""{
                let lookingkinkRole = answer.split(separator: "|")
                var lookingforKinkRole = [String]()
                lookingforKinkRole.removeAll()
                
                for one in lookingkinkRole{
                    let stringone = String(one).trim()
                    for two in DS_kinkRoles{
                        if stringone == two.valueName{
                            lookingforKinkRole.append(two.valueID ?? "")
                        }
                    }
                }
                self.isFilterState = true
                self.isFilterrequesting = false
                self.str_search_kinkRole = lookingforKinkRole
                self.firstTime4Filter = true
                self.isfirstime4kinkRole = false
                self.ds_peoples.removeAll()
                self.getDataSourceWithFilter(0)
            }else{
                if !isfirstime4kinkRole{
                    self.firstTime = true
                    self.ds_peoples.removeAll()
                    self.getDataSource(0)
                }
            }
            break
        default:
            print("default")
        }
        self.setDropDownFilter(false)
    }
}

extension PeopleVC: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let userName = textField.text!
        if !isFilterrequesting4EndEdit{
            self.isFilterrequesting = true
            if userName != ""{
                self.showLoadingView(vc: self)
                ApiManager.filterByUserName(username: userName) { (isSuccess, data) in
                    self.hideLoadingView()
                    self.ds_peoples.removeAll()
                    if isSuccess{
                        //print(data as Any)
                        //self.ds_peoples.removeAll()
                        let json = JSON(data as Any)
                        //print(json)
                        let dataobjectArr = json["data"].arrayObject
                        if let arr = dataobjectArr{
                            if arr.count > 0{
                                if arr.count != 1{
                                    var num = 0
                                    for one in arr{
                                        num += 1
                                        let onePeople = JSON(one as Any)
                                        let id = onePeople["_id"].stringValue
                                        if self.getBlockStatus(id){
                                            continue
                                        }
                                        let userName = onePeople["username"].stringValue
                                        let realName = onePeople["displayName"].stringValue
                                        let email = onePeople["email"].stringValue
                                        let currentAge = onePeople["currentAge"].stringValue
                                        let isPlusMember = onePeople["hasPlusMembership"].boolValue
                                        let lastActive = onePeople["lastActive"].stringValue
                                        var timeAgo = ""
                                        let now = Date()
                                        let calendar = Calendar.current
                                        if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                                            print(lastDate as Any)
                                            let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                                            let seconds = ageComponents.second ?? 0
                                            print(seconds)
                                            let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                                            let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                                            let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                                            let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                                            if secondss > 0{
                                                timeAgo = "\(secondss)" + "s"
                                            }
                                            if min > 0{
                                                timeAgo = "\(min)" + "m"
                                            }
                                            if hour > 0{
                                                timeAgo = "\(hour)" + "h"
                                            }
                                            if day > 0{
                                                timeAgo = "\(day)" + "d"
                                            }
                                        }
                                        let profile = onePeople["profile"].object
                                        let profilejson = JSON(profile as Any)
                                        
                                        let location = profilejson["location"].stringValue
                                        let genderObject = profilejson["gender"].object
                                        let genderjson = JSON(genderObject as Any)
                                        let gender = genderjson["lowerCaseName"].stringValue
                                        let kinkRoleObject = profilejson["kinkRole"].object
                                        let kinkRolejson = JSON(kinkRoleObject as Any)
                                        let kinkRole = kinkRolejson["name"].stringValue
                                        
                                        let pronounObject = profilejson["pronoun"].object
                                        let pronounjson = JSON(pronounObject as Any)
                                        let pronoun = pronounjson["name"].stringValue
                                        
                                        let matchingObject = profilejson["matching"].object
                                        let matchingjson = JSON(matchingObject as Any)
                                        let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                                        
                                        var lookingForgenders = [String]()
                                        if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                                            lookingForgenders.removeAll()
                                            for one in lookingForgenderObjectArr{
                                                let jsonOne = JSON(one)
                                                lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let orientationsObjectArr = matchingjson["orientations"].arrayObject
                                        
                                        var orientations = [String]()
                                        if let orientationsObjectArr = orientationsObjectArr{
                                            orientations.removeAll()
                                            for one in orientationsObjectArr{
                                                let jsonOne = JSON(one)
                                                orientations.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                                        
                                        var relationshipStatus = [String]()
                                        if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                                            relationshipStatus.removeAll()
                                            for one in relationShipStatusObjectArr{
                                                let jsonOne = JSON(one)
                                                relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                                        
                                        var relationshipStyles = [String]()
                                        if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                                            relationshipStyles.removeAll()
                                            for one in relationShipStyleObjectArr{
                                                let jsonOne = JSON(one)
                                                relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let hereForObjectArr = matchingjson["hereFor"].arrayObject
                                        
                                        var hereFor = [String]()
                                        if let hereForObjectArr = hereForObjectArr{
                                            hereFor.removeAll()
                                            for one in hereForObjectArr{
                                                let jsonOne = JSON(one)
                                                hereFor.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                                        
                                        var kinkRoles = [String]()
                                        if let hereForObjectArr = kinkroleObjectArr{
                                            kinkRoles.removeAll()
                                            for one in hereForObjectArr{
                                                let jsonOne = JSON(one)
                                                kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        var identifies = [String]()
                                        let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                                        if let identifiesArr = identifiesAsArr{
                                            for one in identifiesArr{
                                                identifies.append(JSON(one)["name"].stringValue)
                                            }
                                        }
                                        
                                        let avatar = onePeople["avatar"].object
                                        let avatarjson = JSON(avatar as Any)
                                        let file = avatarjson["file"].object
                                        let filejson = JSON(file as Any)
                                        let url = filejson["url"].object
                                        let urljson = JSON(url as Any)
                                        let useravatar = urljson["smallPhotoURL"].stringValue
                                                                 
                                        //print("one paramers  ==== > \(userName) \n \(realName) \n \(email) \n \(currentAge) \n \(isPlusMember) \n \(location) \n \(gender) \n \(useravatar)")
                                        //4
                                        self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole:kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false, lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                                        if num == arr.count{
                                            self.col_peopleShow.reloadData()
                                            self.isfilterByUserNameState = true
                                            self.isLoading = false
                                            self.isFilterrequesting = false
                                            self.isfilter_success = true
                                        }
                                    }
                                }else{
                                    let onePeople = JSON(arr.first as Any)
                                    let id = onePeople["_id"].stringValue
                                    if self.getBlockStatus(id){
                                        self.col_peopleShow.reloadData()
                                        self.isfilterByUserNameState = true
                                        self.isLoading = false
                                        self.isFilterrequesting = false
                                        self.isfilter_success = false
                                    }else{
                                        let userName = onePeople["username"].stringValue
                                        let realName = onePeople["displayName"].stringValue
                                        let email = onePeople["email"].stringValue
                                        let currentAge = onePeople["currentAge"].stringValue
                                        let isPlusMember = onePeople["hasPlusMembership"].boolValue
                                        let lastActive = onePeople["lastActive"].stringValue
                                        var timeAgo = ""
                                        let now = Date()
                                        let calendar = Calendar.current
                                        if let lastDate = getDateSpecialFormatFromString(strDate: lastActive){
                                            print(lastDate as Any)
                                            let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                                            let seconds = ageComponents.second ?? 0
                                            print(seconds)
                                            let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                                            let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                                            let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                                            let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                                            if secondss > 0{
                                                timeAgo = "\(secondss)" + "s"
                                            }
                                            if min > 0{
                                                timeAgo = "\(min)" + "m"
                                            }
                                            if hour > 0{
                                                timeAgo = "\(hour)" + "h"
                                            }
                                            if day > 0{
                                                timeAgo = "\(day)" + "d"
                                            }
                                        }
                                        let profile = onePeople["profile"].object
                                        let profilejson = JSON(profile as Any)
                                        
                                        let location = profilejson["location"].stringValue
                                        let genderObject = profilejson["gender"].object
                                        let genderjson = JSON(genderObject as Any)
                                        let gender = genderjson["lowerCaseName"].stringValue
                                        let kinkRoleObject = profilejson["kinkRole"].object
                                        let kinkRolejson = JSON(kinkRoleObject as Any)
                                        let kinkRole = kinkRolejson["name"].stringValue
                                        
                                        let pronounObject = profilejson["pronoun"].object
                                        let pronounjson = JSON(pronounObject as Any)
                                        let pronoun = pronounjson["name"].stringValue
                                        
                                        let matchingObject = profilejson["matching"].object
                                        let matchingjson = JSON(matchingObject as Any)
                                        let lookingForgenderObjectArr = matchingjson["genders"].arrayObject
                                        
                                        var lookingForgenders = [String]()
                                        if let lookingForgenderObjectArr = lookingForgenderObjectArr{
                                            lookingForgenders.removeAll()
                                            for one in lookingForgenderObjectArr{
                                                let jsonOne = JSON(one)
                                                lookingForgenders.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let orientationsObjectArr = matchingjson["orientations"].arrayObject
                                        
                                        var orientations = [String]()
                                        if let orientationsObjectArr = orientationsObjectArr{
                                            orientations.removeAll()
                                            for one in orientationsObjectArr{
                                                let jsonOne = JSON(one)
                                                orientations.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let relationShipStatusObjectArr = matchingjson["relationshipStatuses"].arrayObject
                                        
                                        var relationshipStatus = [String]()
                                        if let relationShipStatusObjectArr = relationShipStatusObjectArr{
                                            relationshipStatus.removeAll()
                                            for one in relationShipStatusObjectArr{
                                                let jsonOne = JSON(one)
                                                relationshipStatus.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let relationShipStyleObjectArr = matchingjson["relationshipStyles"].arrayObject
                                        
                                        var relationshipStyles = [String]()
                                        if let relationShipStyleObjectArr = relationShipStyleObjectArr{
                                            relationshipStyles.removeAll()
                                            for one in relationShipStyleObjectArr{
                                                let jsonOne = JSON(one)
                                                relationshipStyles.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let hereForObjectArr = matchingjson["hereFor"].arrayObject
                                        
                                        var hereFor = [String]()
                                        if let hereForObjectArr = hereForObjectArr{
                                            hereFor.removeAll()
                                            for one in hereForObjectArr{
                                                let jsonOne = JSON(one)
                                                hereFor.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        let kinkroleObjectArr = matchingjson["kinkRoles"].arrayObject
                                        
                                        var kinkRoles = [String]()
                                        if let hereForObjectArr = kinkroleObjectArr{
                                            kinkRoles.removeAll()
                                            for one in hereForObjectArr{
                                                let jsonOne = JSON(one)
                                                kinkRoles.append(jsonOne["lowerCaseName"].stringValue)
                                            }
                                        }
                                        
                                        var identifies = [String]()
                                        let identifiesAsArr = profilejson["identifiesAs"].arrayObject
                                        if let identifiesArr = identifiesAsArr{
                                            for one in identifiesArr{
                                                identifies.append(JSON(one)["name"].stringValue)
                                            }
                                        }
                                        
                                        let avatar = onePeople["avatar"].object
                                        let avatarjson = JSON(avatar as Any)
                                        let file = avatarjson["file"].object
                                        let filejson = JSON(file as Any)
                                        let url = filejson["url"].object
                                        let urljson = JSON(url as Any)
                                        let useravatar = urljson["smallPhotoURL"].stringValue
                                                                 
                                        //print("one paramers  ==== > \(userName) \n \(realName) \n \(email) \n \(currentAge) \n \(isPlusMember) \n \(location) \n \(gender) \n \(useravatar)")
                                        //4
                                        self.ds_peoples.append(PeopleModel(id,realName: realName, userName: userName, email: email,pronoun: pronoun, userAvatar: useravatar, gender: gender, currentAge: currentAge, location: location, userkinkRole: kinkRole, lookingForGender: lookingForgenders, orientation: orientations, status: relationshipStatus, relationshipStyle: relationshipStyles, hereFor: hereFor, kinkRole:kinkRoles, identifiAs: identifies, isPlusMember: isPlusMember, chatRequest: false, lastActive: "Last Seen" + " " + timeAgo + " " + "ago"))
                                        self.col_peopleShow.reloadData()
                                        self.isfilterByUserNameState = true
                                        self.isLoading = false
                                        self.isFilterrequesting = false
                                        self.isfilter_success = true
                                    }
                                }
                            }else{
                                self.col_peopleShow.reloadData()
                                self.isLoading = false
                                self.isFilterrequesting = false
                                self.isfilter_success = false
                            }
                        }else{
                            self.col_peopleShow.reloadData()
                            self.isLoading = false
                            self.isFilterrequesting = false
                            self.isfilter_success = false
                        }
                    }else{
                        self.col_peopleShow.reloadData()
                        self.isLoading = false
                        self.isFilterrequesting = false
                        self.isfilter_success = false
                    }
                }
            }else{
                if self.ds_peoples.count == 0 || self.isfilter_success{
                    self.ds_peoples.removeAll()
                    self.firstTime = true
                    self.getDataSource(0)
                }
            }
        }
    }
}

enum searchDropDownTag: Int{
    case name = 2001
    case distance = 2002
    case gender = 2003
    case kinkRole = 2004
}


