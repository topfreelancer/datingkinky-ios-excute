//
//  FirstDayPromotionVC.swift
//  DatingKinky
//
//  Created by top Dev on 10/31/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class FirstDayPromotionVC: BaseVC {

    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_extracontent: TTTAttributedLabel!
    @IBOutlet weak var lbl_bottom_text: UILabel!
    let readmoreRange = NSRange(location: 68, length: 23)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        setReadMoreLabel()
        let downSwipe1 = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown))
        downSwipe1.direction = .down
        view.addGestureRecognizer(downSwipe1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @objc func swipeDown() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUI()  {
        self.lbl_title.text = "Hi There!\nWelcome to Dating Kinky."
        self.lbl_content.text = "Welcom to Dating Kinky!\n\nI know you're new here, adn probably still talking a look around. There's a lot to do and crazy cool kinksters to do it with, so I won't interrupt you for too long.\n\nI just wanted to get your attention and clue you into a few things that my help:"
        self.lbl_bottom_text.text = "Bottom line, you don't need to do any of this, thought. Just enjoy your time on Dating Kinky!"
        self.lbl_bottom_text.font = self.lbl_bottom_text.font.italic
    }
    
    func setReadMoreLabel()  {
        if #available(iOS 11.0, *) {
            let color = UIColor.init(named: "color_people_title")
            //let newURL = "https://perkx.co/#/"
                    
            self.lbl_extracontent.text = "Oh, and as a welcome, I've given you a week of our PLUS membership (read more about it here) to introduce you to some extra features we offer our supporters."
            self.lbl_extracontent.linkAttributes = [NSAttributedString.Key.foregroundColor: color]
            self.lbl_extracontent.addLink(to: URL(string: Constants.READ_MORE_LINK), with: readmoreRange)
            
            self.lbl_extracontent.addLink(to: <#T##URL!#>, with: <#T##NSRange#>)
            self.lbl_extracontent.delegate = self
        } else {
            //let newURL = "https://perkx.co/#/"
            let color = UIColor.blue
            self.lbl_extracontent.text = "Oh, and as a welcome, I've given you a week of our PLUS membership (read more about it here) to introduce you to some extra features we offer our supporters."
            self.lbl_extracontent.linkAttributes = [NSAttributedString.Key.foregroundColor: color]
            self.lbl_extracontent.addLink(to: URL(string: Constants.READ_MORE_LINK), with: readmoreRange)
            self.lbl_extracontent.delegate = self
        }
        
        
                
    }
    
    @IBAction func getStartedLinkClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.GETSTARTED_VIDEO_TOUR_LINK)
    }
    
    @IBAction func collShitLinkClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.COOL_SHIT_SITES_LINK)
    }
    
    @IBAction func datingKinkyFreeLinkClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.IS_DATINGKINKY_REALLY_FREE_LINK)
    }
    
    @IBAction func speedWorldLinkClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.SPREAD_WORLD_DATINGKINKY_LINK)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        //closePromotion(.first_day_promotion_vc)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func coolBtnClicked(_ sender: Any) {
        //closePromotion(.first_day_promotion_vc)
        self.dismiss(animated: true, completion: nil)
    }
}


extension FirstDayPromotionVC: TTTAttributedLabelDelegate{
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if label == self.lbl_extracontent{
            self.gotoWebViewWithProgressBarModal(Constants.READ_MORE_LINK)
        }
    }
}
