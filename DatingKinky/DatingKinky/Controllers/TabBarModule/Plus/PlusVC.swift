//
//  PlusVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class PlusVC: BaseVC {

    @IBOutlet weak var lbl_best_value: UILabel!
    @IBOutlet weak var lbl_sixmonth: UILabel!
    @IBOutlet weak var lbl_one_month: UILabel!
    @IBOutlet weak var cons_one_width: NSLayoutConstraint!
    @IBOutlet weak var cons_gift_width: NSLayoutConstraint!
    @IBOutlet weak var uiv_gift: UIView!
    @IBOutlet weak var uiv_plus: UIView!
    @IBOutlet weak var lbl_gift_6month: UILabel!
    @IBOutlet weak var lbl_gift_oneyear: UILabel!
    @IBOutlet weak var lbl_gift_onemonth: UILabel!
    @IBOutlet weak var imv_userprofile: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_usercontent: UILabel!
    @IBOutlet weak var lbl_userage: UILabel!
    @IBOutlet weak var scr_total: UIScrollView!
    var is_gift: Bool = false
    var gift_userid: String?
    var str_user_profile: String?
    var str_user_username: String?
    var str_user_content: String?
    var str_user_age: String?
    var membership: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBarImage()
        self.addBackButtonNavBar()
        setUI()
        initPurchase()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.membership = nil
    }
    
    func initPurchase() {
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            //self?.hideLoadingView()
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "DatingKinky", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("purchased succeed")
                    //let userdefault = UserDefaults.standard
                    //userdefault.set(true, forKey: Const.KEY_ISPURCHASED)
//                    self!.startDownload()
                    if self!.is_gift{
                        if let gift_user = self!.gift_userid{
                            self!.setMemberShip4Gift(self!.membership, giftuser: gift_user)
                        }
                    }else{
                        self!.setMemberShip(self!.membership)
                    }
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            
            } else if type == .restored {
                
                let alertView = UIAlertController(title: "DatingKinky", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("restore succeed")
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            } else {
                print(type.message())
                if let message = type.message(){
                    self!.showToast(message)
                }
                
                //self?.showAlert("failed")
            }
        }
    }
    
    func setMembership() {
        /*self.showLoadingView()
        ApiRequest.shared.set_membership(selectedPlan) { (code, message, value) in
            self.hideLoadingView()
            if code == Const.CODE_SUCCESS {
                self.showAlert("You have succeeded to purchase membership plan")
            } else {
                self.showAlert(message!)
            }
        }*/
    }
    
    func setUI() {
        if self.is_gift{
            self.uiv_gift.isHidden = false
            self.uiv_plus.isHidden = true
            self.scr_total.isScrollEnabled = false
            self.cons_gift_width.constant = (Constants.SCREEN_WIDTH - 60) / 3
            self.lbl_gift_6month.text = "One-time charge, no renewals."
            self.lbl_gift_oneyear.text = "One-time charge, no renewals."
            self.lbl_gift_onemonth.text = "One-time charge, no renewals."
            self.lbl_gift_onemonth.font = self.lbl_gift_onemonth.font.italic
            self.lbl_gift_6month.font = self.lbl_gift_6month.font.italic
            self.lbl_gift_oneyear.font = self.lbl_gift_oneyear.font.italic
            if let profile = self.str_user_profile{
                setImageWithURL(profile, imv:self.imv_userprofile)
            }
            if let username = self.str_user_username{
                self.lbl_username.text = username
            }
            if let usercontent = self.str_user_content{
                self.lbl_usercontent.text = usercontent
            }
            if let userage = self.str_user_age{
                self.lbl_userage.text = userage
            }
        }else{
            self.uiv_gift.isHidden = true
            self.uiv_plus.isHidden = false
            self.scr_total.isScrollEnabled = true
            self.cons_one_width.constant = (Constants.SCREEN_WIDTH - 60) / 3
            self.lbl_best_value.text = "Renews automatically every\n13months."
            self.lbl_best_value.font = self.lbl_best_value.font.italic
            self.lbl_sixmonth.text = "Renews automatically every\n6 months."
            self.lbl_sixmonth.font = self.lbl_sixmonth.font.italic
            self.lbl_one_month.text = "Cancel anytime! Easy \nmonthly payments."
            self.lbl_one_month.font = self.lbl_one_month.font.italic
        }
    }
    
    @IBAction func sixMonthBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsSM
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_6MONTH_PRODUCT_ID)
    }
    @IBAction func yearBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsOY
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_1YEAR_PRODUCT_ID)
    }
    @IBAction func onemonthBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsPM
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_1MONTH_PRODUCT_ID)
    }
    
    func setMemberShip(_ membership: String?)  {
        if let membership = membership{
            self.showLoadingView(vc: self)
            ApiManager.purchaseMembership(subsType: membership) { (isSuccess, data) in
                //print(data)
                self.hideLoadingView()
                if let msg = data{
                    self.showToast(msg)
                }
            }
        }
    }
    
    func setMemberShip4Gift(_ membership: String?, giftuser: String?)  {
        if let membership = membership, let giftuser = giftuser{
            self.showLoadingView(vc: self)
            ApiManager.purchaseMembership(gift_userid: giftuser,subsType: membership) { (isSuccess, data) in
                //print(data)
                self.hideLoadingView()
                if let msg = data{
                    self.showToast(msg)
                }
            }
        }
    }
    
    @IBAction func btnGift6monthBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsSM
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_6MONTH_PRODUCT_ID)
    }
    @IBAction func btnGift1yearBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsOY
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_1YEAR_PRODUCT_ID)
    }
    @IBAction func btnGift1monthBtnClicked(_ sender: Any) {
        self.membership = PlusMembershipType.SubsPM
        IAPHandler.shared.purchaseMyProduct(strProductID: AUTO_RENEW_SUBSCRIPTION_1MONTH_PRODUCT_ID)
    }
}
