//
//  VideoVerifyAcceptPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 10/21/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import ZiggeoSwiftFramework
import SwiftyJSON
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class VideoVerifyAcceptPopUpVC : BaseVC, ZiggeoVideosDelegate{

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var cus_activity: UIActivityIndicatorView!
    @IBOutlet weak var uiv_back: UIView!
    var m_ziggeo: Ziggeo! = nil
    let pathList = Database.database().reference().child("list")
    var partnerListroomId = ""
    var mestatusroomID = ""
    var meListroomId = "u" + "\(thisuser!.id!)"
    var messageNum: Int = 0
    var isBlock: String? = "false"
    let reset_path4content = Database.database().reference().child("message")
    var ds_peoples = [PeopleModel]()
    var messsageNum4VideoRecording = "0"
    
    var my_chatting_roomid = ""
    var partner_chatting_roomid = ""
    let ref = Database.database().reference()
    
    //static let CONV_REFERENCE = ""
    let MESSAGE = "message"
    let LIST = "list"
    let STATUS = "status"
    let CHAT_REQUEST = "request"
    let VERIFICATION = "verification"
    var valueHandle: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_content.text = "To video verify with \(request_user_name ?? ""). They have already recorded and approved a 20-second video for you. If you'd like to see that video and verify yourself, record your own video, approve it, and BOTH videos will be unlocked, and you will have 5 minutes to view before deletion.If you choose to decline, that's cool. Their video will simply be deleted."
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        
        m_ziggeo = Ziggeo(token: Constants.ziggeo_app_token)
        m_ziggeo.enableDebugLogs = true
        m_ziggeo.videos.delegate = self
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            if let userid = friendID{
                self.showLoadingView(vc: self)
                ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, msg,flag ) in
                    self.hideLoadingView()
                    DispatchQueue.main.async {
                        //self.closeMenu(.videoVerifyAcceptPop)
                        dismiss_flag = true
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if dismiss_flag{
            self.dismiss(animated: false, completion: nil)
        }else{
            self.uiv_dlg.isHidden = false
            // set message part
            if let friendid = request_user_id{
                my_chatting_roomid   = "\(thisuser!.id!)" + "_" +  friendid
                partner_chatting_roomid = friendid + "_" + "\(thisuser!.id!)"
                partnerListroomId = "u" + friendid
                mestatusroomID = friendid + "_" + "\(thisuser!.id!)"
            }
            cus_activity.isHidden = true
            uiv_back.isHidden = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, msg,flag) in
                self.hideLoadingView()
                DispatchQueue.main.async {
                    dismiss_flag = true
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func doitBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.chat_verifyaccept.rawValue) { (isSuccess, msg,flag) in
                self.hideLoadingView()
                if isSuccess{
                    if flag == 1{
                        self.gotoRecord()
                    }else if flag == 0{
                        self.showAlerMessage(message: msg ?? "")
                    }
                    
                }
            }
        }
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, msg,flag) in
                self.hideLoadingView()
                //self.dismiss(animated: true, completion: nil)
                DispatchQueue.main.async {
                    dismiss_flag = true
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func waitBtnClicked(_ sender: Any) {
        if let userid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: userid, requestType: RequestType.chat_verifyreject.rawValue) { (isSuccess, msg,flag) in
                self.hideLoadingView()
                //self.dismiss(animated: true, completion: nil)self.closeMenu(.videoVerifyAcceptPop)
                DispatchQueue.main.async {
                    dismiss_flag = true
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func gotoRecord() {
        DispatchQueue.main.async {
            dismiss_flag = true
            let recorder = ZiggeoRecorder(application: self.m_ziggeo);
            recorder.coverSelectorEnabled = false;
            recorder.recordedVideoPreviewEnabled = true;
            recorder.cameraFlipButtonVisible = true;
            recorder.cameraDevice = UIImagePickerController.CameraDevice.front;
            recorder.maxRecordedDurationSeconds = 20; //infinite
            let customPreview = CustomPreviewController();
            recorder.videoPreview = customPreview;
            customPreview.previewDelegate = recorder;
            recorder.modalPresentationStyle = .fullScreen
            self.present(recorder, animated: true, completion: nil);
        }
    }
    
    public func videoUploadComplete(_ sourcePath: String, token: String, response: URLResponse?, error: NSError?, json:  NSDictionary?) {
        if let friendid = friendID{
            ApiManager.setRecordingEnd_CheckOtherStatus(receiver_id: friendid, owner: PARAMS.RECEIVER) { (recordingSuccess, data) in
                if recordingSuccess{
                    let other_status = JSON(data as Any)["other_status"].intValue
                    var roomid = ""
                    if thisuser!.id! > friendid{
                        roomid = friendid + "_" + thisuser!.id!
                    }else{
                        roomid = thisuser!.id! + "_" + friendid
                    }
                    if other_status == 0{// receiver recording complete
                        // send message action for partner
                        self.valueHandle = FirebaseAPI.getPartnerVideoTokenValue(roomid) { (verifymodel) in
                            if verifymodel.sender_status == "1"{
                                self.sendMessage(token, partner_token: verifymodel.sender_video_value)
                            }
                        }
                        
                    }else{// receiver recording not completed
                        // save action
                        self.ref.child(self.VERIFICATION).child(roomid).child("receiver_status").setValue("1")
                        self.ref.child(self.VERIFICATION).child(roomid).child("receiver_video_value").setValue(token)
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("server error")
                }
            }
        }
    }
    
    func sendMessage(_ metoken: String,partner_token: String )  {
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        // for partner
        chatObject["message"]     = "video verification"
        chatObject["image"]       = metoken
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId  = friendID! + "_" + "\(thisuser!.id!)"
        ref.child(MESSAGE).child(roomId).child(friendID!).setValue(chatObject) { (error, dataRef) in
            if let error = error {
                print(error)
            } else {
                let roomId1  = "\(thisuser!.id!)" + "_" + friendID!
                var chatObject1 = [String: String]()
                // for partner
                chatObject1["message"]     = "video verification"
                chatObject1["image"]       = partner_token
                chatObject1["photo"]       = partnerAvatar4ChatRequest
                chatObject1["sender_id"]   = friendID!
                chatObject1["time"]        = "\(timeNow)" as String
                chatObject1["name"]        = memberName4ChatRequest
                self.ref.child(self.MESSAGE).child(roomId1).child(thisuser!.id!).setValue(chatObject1) { (error, dataRef) in
                    if let error = error {
                        print(error)
                    } else {
                        var roomid = ""
                        if thisuser!.id! > friendID!{
                            roomid = friendID! + "_" + thisuser!.id!
                        }else{
                            roomid = thisuser!.id! + "_" + friendID!
                        }
                        self.ref.child(self.VERIFICATION).child(roomid).removeValue()
                        dismiss_flag = true
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    /*func sendMessage4Partner(_ token: String)  {
        if let userid = friendID{
            let roomId   = userid + "_" + "\(thisuser!.id!)"
            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
            var chatObject = [String: String]()
            chatObject["message"]     = "video verification"
            chatObject["image"]       = token
            chatObject["photo"]       = thisuser!.userAvatar
            chatObject["sender_id"]   = "\(thisuser!.id!)"
            chatObject["time"]        = "\(timeNow)" as String
            chatObject["name"]        = "\(thisuser!.username)"
            FirebaseAPI.sendMessage(chatObject, roomId) { (status, message) in
                if status{
                    self.resetContentAsFullPath(message)
                }else{
                    print("firebase error")
                }
            }
        }
    }
    
    func resetContentAsFullPath(_ full_path: String) {
        let fullpath_arr = full_path.split(separator: "/")
        let roomid = fullpath_arr[3]
        let lastpath = fullpath_arr.last
        if let lastpath = lastpath{
            reset_path4content.child(String(roomid)).child(String(lastpath)).child("message").setValue(lastpath)
            DispatchQueue.main.async {
                dismiss_flag = true
                self.dismiss(animated: true, completion: nil)
            }
        }
    }*/
}

