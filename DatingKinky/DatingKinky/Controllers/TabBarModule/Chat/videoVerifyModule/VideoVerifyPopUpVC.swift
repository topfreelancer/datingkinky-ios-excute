//
//  VideoVerifyPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import ZiggeoSwiftFramework
import SwiftyJSON
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class VideoVerifyPopUpVC: BaseVC, ZiggeoVideosDelegate{

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var uiv_back: UIView!
    @IBOutlet weak var cus_activity: UIActivityIndicatorView!
    var valueHandle: UInt?
    
    let ref = Database.database().reference()
    
    //static let CONV_REFERENCE = ""
    let MESSAGE = "message"
    let LIST = "list"
    let STATUS = "status"
    let CHAT_REQUEST = "request"
    let VERIFICATION = "verification"
    
    
    var message_num4videorecording = "0"
    var m_ziggeo: Ziggeo! = nil
    let pathList = Database.database().reference().child("list")
    var partnerListroomId = ""
    var mestatusroomID = ""
    var meListroomId = "u" + "\(thisuser!.id!)"
    var messageNum: Int = 0
    var isBlock: String? = "false"
    let reset_path4content = Database.database().reference().child("message")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(videoVerifyRequestRejected),name: .videoVerifyRequestRejected, object: nil)
        
        self.lbl_content.text = "Dating Kinky's Video Verification is a 20-second video that requires mutual consent & sharing.Once you've recorded your video and approved it, your chat mate will be asked if they want to video verify. If they agree, they will have 3 minutes to record and approve their video. Then, both videos will be made available for 5 minutes for each of you before being deleted."
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        m_ziggeo = Ziggeo(token: Constants.ziggeo_app_token)
        m_ziggeo.enableDebugLogs = true
        m_ziggeo.videos.delegate = self
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func videoVerifyRequestRejected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.uiv_dlg.isHidden = false
        // set message part
        if let friendid = friendID{
            partnerListroomId = "u" + friendid
        }
        mestatusroomID = friendID! + "_" + "\(thisuser!.id!)"
        self.uiv_back.isHidden = true
        self.cus_activity.isHidden = true
    }
    
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    @IBAction func doitBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
        if let friendid = friendID{
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.chat_verifysend.rawValue) { (isSuccess, msg,flag) in
                if isSuccess{
                    if flag == 1{
                        self.setVerificationNodes()
                    }else{
                        //self.showAlerMessage(message: msg ?? "")
                        if let membername = memberName4ChatRequest{
                            self.showAlerMessage(message: membername + " " + "does not accept video verification requests.")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func waitBtnClicked(_ sender: Any) {
        self.closeMenu(.videoVerifyPop)
    }
    
    func setVerificationNodes() {
        if let meid = thisuser?.id,let partnerid = friendID{
            var verifyObject = [String: String]()
            var roomid = ""
            if meid > partnerid{
                roomid = partnerid + "_" + meid
            }else{
                roomid = meid + "_" + partnerid
            }
            //self.ref.child(self.VERIFICATION).child(roomid).removeValue()
            verifyObject["sender_status"] = "0"
            verifyObject["receiver_status"] = "0"
            verifyObject["sender_video_value"] = ""
            verifyObject["receiver_video_value"] = ""
            FirebaseAPI.setVerificationValues(verifyObject, roomid) { (isSuccess, data) in
                if isSuccess{
                    DispatchQueue.main.async {
                        let recorder = ZiggeoRecorder(application: self.m_ziggeo);
                        recorder.coverSelectorEnabled = false;
                        recorder.recordedVideoPreviewEnabled = true;
                        recorder.cameraFlipButtonVisible = true;
                        recorder.cameraDevice = UIImagePickerController.CameraDevice.front;
                        recorder.maxRecordedDurationSeconds = 20; //infinite
                        recorder.showLightIndicator = false
                        let customPreview = CustomPreviewController();
                        recorder.videoPreview = customPreview;
                        customPreview.previewDelegate = recorder;
                        recorder.modalPresentationStyle = .fullScreen
                        self.present(recorder, animated: true, completion: nil);
                    }
                }
            }
        }
    }
    
    public func videoUploadComplete(_ sourcePath: String, token: String, response: URLResponse?, error: NSError?, json:  NSDictionary?) {
        // set recordig end status and check receiver's status
        if let friendid = friendID{
            ApiManager.setRecordingEnd_CheckOtherStatus(receiver_id: friendid, owner: PARAMS.SENDER) { (recordingSuccess, data) in
                if recordingSuccess{
                    let other_status = JSON(data as Any)["other_status"].intValue
                    var roomid = ""
                    if thisuser!.id! > friendid{
                        roomid = friendid + "_" + thisuser!.id!
                    }else{
                        roomid = thisuser!.id! + "_" + friendid
                    }
                    if other_status == 0{// receiver recording complete
                        // send message action for partner
                        self.valueHandle = FirebaseAPI.getPartnerVideoTokenValue(roomid) { (verifymodel) in
                            if verifymodel.receiver_status == "1"{
                                self.sendMessage(token, partner_token: verifymodel.receiver_video_value)
                            }
                        }
                    }else{
                        self.ref.child(self.VERIFICATION).child(roomid).child("sender_status").setValue("1")
                        self.ref.child(self.VERIFICATION).child(roomid).child("sender_video_value").setValue(token)
                        self.closeMenu(.videoVerifyPop)
                    }
                }else{
                    print("server error")
                }
            }
        }
    }
    
    func sendMessage(_ metoken: String,partner_token: String )  {
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        // for partner
        chatObject["message"]     = "video verification"
        chatObject["image"]       = metoken
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId  = friendID! + "_" + "\(thisuser!.id!)"
        ref.child(MESSAGE).child(roomId).child(friendID!).setValue(chatObject) { (error, dataRef) in
            if let error = error {
                print(error)
            } else {
                let roomId1  = "\(thisuser!.id!)" + "_" + friendID!
                var chatObject1 = [String: String]()
                // for partner
                chatObject1["message"]     = "video verification"
                chatObject1["image"]       = partner_token
                chatObject1["photo"]       = partnerAvatar4ChatRequest
                chatObject1["sender_id"]   = friendID!
                chatObject1["time"]        = "\(timeNow)" as String
                chatObject1["name"]        = memberName4ChatRequest
                self.ref.child(self.MESSAGE).child(roomId1).child(thisuser!.id!).setValue(chatObject1) { (error, dataRef) in
                    if let error = error {
                        print(error)
                    } else {
                        var roomid = ""
                        if thisuser!.id! > friendID!{
                            roomid = friendID! + "_" + thisuser!.id!
                        }else{
                            roomid = thisuser!.id! + "_" + friendID!
                        }
                        self.ref.child(self.VERIFICATION).child(roomid).removeValue()
                        self.closeMenu(.videoVerifyPop)
                    }
                }
            }
        }
    }
}
