//
//  VideoChatRequestAcceptPopVC.swift
//  DatingKinky
//
//  Created by top Dev on 09.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class VideoChatRequestAcceptPopVC: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var lbl_privacyContent: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_content.text = "\(request_user_name ?? "") has requested to video chat with you, and of course, it's based on your consent."
        self.lbl_privacyContent.text = "We care about your privacy here on Dating Kinky! Please consider that even thought videos can be deleted, that there are way to save them(like recording with another device). So, if your privacy/anonymity is important to you, please"
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func guideCheckBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBarModal(Constants.VIDEOVERIFY_TERMS_LINK)
    }
    
    @IBAction func connnectBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.video_chat_accept.rawValue) { (isSuccess, msg, flag) in
                self.hideLoadingView()
                let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                let toVC = storyBoard.instantiateViewController( withIdentifier: "VideoChatViewController") as! VideoChatViewController
                var roomId = "video_default"
                if let userid = thisuser!.id , let friendid = friendID{
                    if userid > friendid{
                        roomId = userid + "_" + friendid
                    }else{
                        roomId = friendid + "_" + userid
                    }
                }
                toVC.roomID = "video_" + roomId
                print("this is room id for video chattingroom ===>", toVC.roomID)
                let navViewController = UINavigationController(rootViewController: toVC)
                navViewController.modalPresentationStyle = .fullScreen
                self.present(navViewController, animated: false, completion: nil)
            }
        }
    }
    @IBAction func noBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.video_chat_reject.rawValue) { (isSuccess, msg, flag) in
                self.hideLoadingView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func turnoffBtnClicked(_ sender: Any) {
        self.gotoTabIndex(4)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        if let friendid = request_user_id{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.video_chat_reject.rawValue) { (isSuccess, msg, flag) in
                self.hideLoadingView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
