//
//  BlockPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit


class BlockPopUpVC: BaseVC {
    
    var partnerid: String?
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_content.text = "Sorry to hear it's not going smashingly.\n \nBut it's simple fix! What would you like to do?"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.uiv_dlg.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.closeMenu(.blockPop)
    }
    
    @IBAction func reportBtnClicked(_ sender: Any) {
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.CHAT, controllerName: "ReportVC")
        self.closeMenu(.blockPop)
        self.navigationController?.pushViewController(tovc, animated: true)
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.CHAT, controllerName: VCs.BLOCKPOPUP1) as! BlockPopUpVC1
        tovc.partner_id = self.partnerid
            
        let configuration = NBBottomSheetConfiguration(animationDuration: 0.4, sheetSize: .fixed(Constants.SCREEN_HEIGHT))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        
        bottomSheetController.present(tovc, on: self)
    }
}
