//
//  MessageSendVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Firebase
import Foundation
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import Photos
import DKImagePickerController
import DKCamera
import ISEmojiView
import SwiftyGif
import Kingfisher
import MBProgressHUD
import IQKeyboardManagerSwift
import ZiggeoSwiftFramework

var isGifShow: Bool = false
@available(iOS 11.0, *)
class MessageSendVC: BaseViewController {
    
    @IBOutlet weak var edt_msgSend: UITextView!
    @IBOutlet weak var tbl_Chat: UITableView!
    @IBOutlet weak var uiv_postView: UIView!
    @IBOutlet weak var imv_Post: UIImageView!
    @IBOutlet weak var uiv_totalAttach: UIView!
    @IBOutlet weak var cons_btm_edtmsgSend: NSLayoutConstraint!
    @IBOutlet weak var cons_btm_attachedView: NSLayoutConstraint!
    @IBOutlet private weak var emojiView: EmojiView! {
        didSet {
            emojiView.delegate = self
        }
    }
    @IBOutlet weak var uiv_msgSend: UIView!
    @IBOutlet weak var uiv_emojiBack: UIView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_ueserProfileContent: UILabel!
    //@IBOutlet weak var cons_edtMessageSend: NSLayoutConstraint!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var uiv_general_top: UIView!
    
    let cellSpacingHeight: CGFloat = 10 // cell line spacing must use section instead of row
    var messageSendHandle: UInt?
    var newStatusHandle: UInt?
    var changeHandle: UInt?
    var msgdataSource =  [ChatModel]()
    var statuesList =  [StatusModel]()
    
    
    let ref = Database.database().reference()
    let pathList = Database.database().reference().child("list")
    let pathStatus = Database.database().reference().child("status")
    let requestPath = Database.database().reference().child("request")
    let reset_path4content = Database.database().reference().child("message")
    
    
    var partnerOnline = false
    var messageNum: Int = 0
    var isBlock: String = "false"
    var downloadURL: URL?
    var gifURL: String?
    var working4imagesend = false
    var working4textsend = false
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    var isattachedViewShow = false
    var isemojiViewShow = false
    var isworkingForGif = false
    var isgifSending = false
    var isdosending = false
    var isImagesending = false
    var partner: UserModel!
    
    var animator = CPImageViewerAnimator()
    var imagePicker: ImagePicker1!
    var ds_attachMenuData  = [AttachModel]()
    var images4AttachMenu = ["waring","attach","gift","","gifBtn","emoji","phone","videoCamera","videoCheck","eye"]
    var colorBack: [UIColor] = [UIColor.init(named: "color_decline")!,UIColor.init(named: "color_icon_back")!,UIColor.init(named: "color_icon_back")!, .clear,UIColor.init(named: "color_iconBack")!,UIColor.init(named: "color_iconBack")!,UIColor.init(named: "color_iconBack")!,UIColor.init(named: "color_iconBack")!,UIColor.init(named: "color_iconBack")!,UIColor.init(named: "color_iconBack")!]
    // add custom part for only loading custom keyboard
    var hud: MBProgressHUD?
    var iskeyboardshow: Bool = false
    var keyboardSize: CGFloat!
    //var issendingKeyboard: Bool = false
    var m_ziggeo: Ziggeo! = nil
    lazy var functions = Functions.functions()
    var str_lastseen_4gift: String?
    var video_chat_pop :VideoPopDlg!
    var voice_chat_pop :VoicePopDlg!
    var navigation: UINavigationController!
    // firebase room id and parameter
    var me_chatroom_id = ""
    var mestatusroomID = ""
    var partnerstatusroomID = ""
    var meListroomId = ""
    var partnerListroomId = ""
    // set flag for current chatting
    var is_current_block = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.msgdataSource.removeAll()
        self.tbl_Chat.reloadData()
        self.navigationController?.isNavigationBarHidden = false
        if let userid = thisuser!.id, let friendid = friendID{
            mestatusroomID = friendid + "_" + userid
            partnerstatusroomID = userid + "_" + friendid
            meListroomId = "u" + userid
            partnerListroomId = "u" + friendid
            me_chatroom_id = userid + "_" + friendid
        }
        self.setOnlinestatus4me()
        self.fireBaseNewChatListner(me_chatroom_id)
        self.setAttachedView(isShow: isattachedViewShow)
        self.setEmojiView(isShow: isemojiViewShow)
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveGif(_:)),name: .gifSend, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openGalleryObj),name: .opneGallery, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView),name: .reloadTableView, object: nil)
        
        IQKeyboardManager.shared.enable = false
        self.iskeyboardshow = false
        isGifShow = false
        self.is_current_block = false
        //self.issendingKeyboard = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edt_msgSend.delegate = self
        currentVC = VCs.MESSAGESEND
        self.setupUI()
        self.setTableView()
        
        self.uiv_emojiBack.addTapGesture(tapNumber: 1, target: self, action: #selector(tapEmojiBack))
        edt_msgSend.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 36)
        self.imagePicker = ImagePicker1(presentationController: self, delegate: self)
        self.setDataSource4Attach()
        let downSwipeAction = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeDown(_:)))
        /*let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))*/
        downSwipeAction.direction = .down
        self.uiv_dlg.addGestureRecognizer(downSwipeAction)
        self.addObservers()
        self.uiv_general_top.addTapGesture(tapNumber: 1, target: self, action: #selector(tapTopView))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .gifSend, object: nil)
        NotificationCenter.default.removeObserver(self, name: .opneGallery, object: nil)
        NotificationCenter.default.removeObserver(self, name: .reloadTableView, object: nil)
        
        if let messageSendHandle = messageSendHandle{
            FirebaseAPI.removeChattingRoomObserver(me_chatroom_id, messageSendHandle)
        }
        self.setOfflinestatus4me()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewDidLayoutSubviews() {// other screen came back here screen again.
        IQKeyboardManager.shared.enable = false
    }
    
    @objc func tapTopView(gesture: UITapGestureRecognizer) -> Void {
        if let last_content = self.msgdataSource.last?.msgContent,let senderName = memberName4ChatRequest, last_content == Constants.BLOCK_CONTENT_FIRST + "\n\n\n" + " " + senderName + Constants.BLOCK_CONTENT_LAST{
            self.getPartnerTotalMessageAndBlockState { (isSuccess, totalnum, isBlock) in
                if isSuccess{
                    if isBlock == "true"{
                        self.showToast("Sorry! You have been blocked from this user.")
                    }else{
                        if let friendid = friendID, let senderName = memberName4ChatRequest{
                            let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.SETTINGS, controllerName: "YourProfileVC") as! YourProfileVC
                            tovc.is_myprofle = false
                            tovc.user_id = friendid
                            tovc.user_name = senderName
                            self.navigationController?.pushViewController(tovc, animated: true)
                        }
                    }
                }
            }
        }else{
            if let friendid = friendID, let senderName = memberName4ChatRequest{
                let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.SETTINGS, controllerName: "YourProfileVC") as! YourProfileVC
                tovc.is_myprofle = false
                tovc.user_id = friendid
                tovc.user_name = senderName
                self.navigationController?.pushViewController(tovc, animated: true)
            }
        }
    }
    
    func addObservers()  {
        // setting notification for keyboard hidding action
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        m_ziggeo = Ziggeo(token: Constants.ziggeo_app_token)
        NotificationCenter.default.addObserver(self, selector: #selector(showVideoVerifyPopDialog),name: .showVideoVerifyPopDialog, object: nil)
    }
    
    
    @objc func showVideoVerifyPopDialog()  {
        self.tbl_Chat.reloadData()
        animVC = .videoVerifyAcceptPop
        creatNav(.videoVerifyAcceptPop)
        openMenu(.videoVerifyAcceptPop)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Keyboard will show and hide notification action
    @objc func keyboardWillShow(notification: NSNotification) {
        if !self.iskeyboardshow && !isGifShow{
            self.iskeyboardshow = true
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y -= (keyboardSize.height - view.safeAreaBottom)
                self.keyboardSize = keyboardSize.height - view.safeAreaBottom
            }
            print("ths is the show====>",self.view.origin.y)
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.iskeyboardshow{
            self.view.frame.origin.y += self.keyboardSize
            self.iskeyboardshow = false
            print("ths is the hide====>",self.view.origin.y)
        }
    }
    
    @objc func handleSwipeDown(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.uiv_postView.isHidden = true
            self.imageFils.removeAll()
            self.imv_Post.image = nil
        }
    }
    
    func setDataSource4Attach()  {
        self.ds_attachMenuData.removeAll()
        for i in 0 ... 9{
            self.ds_attachMenuData.append(AttachModel(str_image: self.images4AttachMenu[i], color_back: colorBack[i]))
        }
    }
    
    @objc func tapEmojiBack(gesture: UITapGestureRecognizer) -> Void {
        self.isemojiViewShow = false
        self.setEmojiView(isShow: false)
    }
    
    @objc func receiveGif(_ notification: NSNotification){
        if let dict = notification.userInfo as NSDictionary?, let selectedGifURL = dict["selected_gif_url"] as? String{
            self.gifURL = selectedGifURL
            self.sendGifWithOnlineCheck4Partner()
            isGifShow = false
        }
    }
    
    @objc func openGalleryObj(){
        self.openGallery()
    }
    
    @objc func reloadTableView(){
        self.tbl_Chat.scrollToBottomRow()
        DispatchQueue.main.async {
              [weak self] in
            self?.tbl_Chat.reloadData()}
    }
    
    func setStatusAccept4partner(_ state: String, partnerId: String, completion: @escaping (_ success: Bool) -> ()) {
        requestPath.child("u" + partnerId).child("u" + thisuser!.id!).child("state").setValue(state)
        completion(true)
    }
    
    func setAttachedView(isShow: Bool) {
        if isShow{
            self.uiv_totalAttach.isHidden = false
            self.cons_btm_edtmsgSend.constant = 0
        }else{
            self.uiv_totalAttach.isHidden = true
            self.cons_btm_edtmsgSend.constant = -50
        }
    }
    
    func setEmojiView(isShow: Bool) {
        if isShow{
            self.emojiView.isHidden = false
            self.uiv_emojiBack.isHidden = false
            self.cons_btm_attachedView.constant = 0
        }else{
            self.emojiView.isHidden = true
            self.uiv_emojiBack.isHidden = true
            self.cons_btm_attachedView.constant = -236
        }
    }
    
    func setupUI() {
        self.addNavBarImage()
        self.addLogoutButtonNavBar()
        self.addLeftButton4NavBar()
        self.uiv_postView.isHidden = true
        if let chattingUserName = memberName4ChatRequest, let chattingUserProfile = memberProfile4ChatRequest{
            self.lbl_userName.text = chattingUserName
            if let lastSeenChatRequests = lastSeenChatRequest{
                self.lbl_ueserProfileContent.text = chattingUserProfile + ":" + lastSeenChatRequests
                self.str_lastseen_4gift = lastSeenChatRequests
            }
        }
    }
    
    func addLogoutButtonNavBar() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "logout")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedLogout), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.rightBarButtonItem = barButtonItemBack
    }
    
    @objc func addTappedLogout() {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let loginNav = storyboad.instantiateViewController(withIdentifier: VCs.LOGINNAV)
        UserDefault.setBool(key: PARAMS.LOGOUT, value: true)
        thisuser!.clearUserInfo()
        UIApplication.shared.keyWindow?.rootViewController = loginNav
    }
    
    func sendMessage4Partner(_ token: String)  {
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        chatObject["message"]     = "video verification"
        chatObject["image"]       = token
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId  = friendID! + "_" + "\(thisuser!.id!)"
        FirebaseAPI.sendMessage(chatObject, roomId) { (status, message) in
            if status{
                self.resetContentAsFullPath(message)
            }else{
                print("firebase error")
            }
        }
    }
    
    func resetContentAsFullPath(_ full_path: String) {
        let fullpath_arr = full_path.split(separator: "/")
        let roomid = fullpath_arr[3]
        let lastpath = fullpath_arr.last
        if let lastpath = lastpath{
            reset_path4content.child(String(roomid)).child(String(lastpath)).child("message").setValue(lastpath)
        }
    }
    
    func setTableView() {
        tbl_Chat.delegate = self
        tbl_Chat.dataSource = self
        self.tbl_Chat.allowsSelection = true
        self.tbl_Chat.separatorStyle = .none
        self.tbl_Chat.estimatedRowHeight = 80
    }
    
    /*func setUnreadMessageNum4me(){
        let usersRef = self.pathList.child(self.meListroomId).child(self.partnerListroomId)
            usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild("messageNum"){
               self.pathList.child(self.meListroomId).child(self.partnerListroomId).child("messageNum").setValue("0")
            }else{
                print("false")
            }
        })
    }*/
    
    func setOnlinestatus4me()  {
        pathStatus.child(mestatusroomID).removeValue()
        var statusObject = [String: String]()
        statusObject["online"] = "online"
        statusObject["sender_id"] = "\(thisuser!.id!)"
        statusObject["time"] = "\(Int(NSDate().timeIntervalSince1970) * 1000)"
        pathStatus.child(mestatusroomID).childByAutoId().setValue(statusObject)
       // }
    }
    
    func getPartnerOnlineStatus(completion :  @escaping (_ success: Bool, _ onineval: Bool) -> ()){
        let usersRef = pathStatus.child(partnerstatusroomID)
        let queryRef = usersRef.queryOrdered(byChild: "sender_id")
        queryRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists(){
                for snap in snapshot.children {
                    let userSnap = snap as! DataSnapshot
                    //let uid = userSnap.key //the uid of each user
                    let userDict = userSnap.value as! [String:AnyObject]
                    let online = userDict["online"] as! String
                    if online != ""{
                        let status = online == "online" ? true : false
                        completion(true, status)
                    }else{
                        completion(true, false)
                    }
                }
            }else{
                completion(true, false)
            }
        })
    }
    
    func getPartnerTotalMessageAndBlockState(completion :  @escaping (_ success: Bool, _ number: String, _ isBlock: String) -> ()){
            let usersRef = pathList.child(partnerListroomId).child(meListroomId)
            usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.hasChild("messageNum") && snapshot.hasChild("isBlock"){
             if let userDict = snapshot.value as? [String:AnyObject]{
                if let number = userDict["messageNum"] as? String, let isBlock = userDict["isBlock"] as? String{
                    completion(true,number, isBlock)
                }
             }else{
                 completion(true,"0", "false")
             }
            }else{
                completion(true,"0", "false")
            }
        })
    }
    
    func fireBaseNewChatListner(_ roomId : String)  {
        self.messageSendHandle = FirebaseAPI.setMessageListener(roomId){ [self] (chatModel) in
            self.msgdataSource.append(chatModel)
            self.tbl_Chat.reloadData()
            self.tbl_Chat.scrollToBottomRow()
        }
    }
    
    func setOfflinestatus4me()  {
        pathStatus.child(mestatusroomID).removeValue()
        var statusObject = [String: String]()
        statusObject["online"] = "offline"
        statusObject["sender_id"] = "\(thisuser!.id!)"
        statusObject["time"] = "\(Int(NSDate().timeIntervalSince1970) * 1000)"
        pathStatus.child(mestatusroomID).childByAutoId().setValue(statusObject)
    }
    
    func openGallery() {
        self.imagePicker.present(from: view)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
         self.assets.removeAll()
        self.assets = assets
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.uiv_postView.isHidden = false
                    self.gotoUploadProfile(image)
                })
            }
        }
        else {}
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            self.uiv_postView.isHidden = false
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_Post.image = image
            
            self.uploadFile2Firebase(imageFils.first!) { (isSuccess, downloadURL) in
               
                if isSuccess{
                    guard  let downloadURL = downloadURL else {
                        return
                    }
                    self.downloadURL = downloadURL
                }else{
                }
            }
        }
    }
    
    func setNewUnreadMessage4partner(number: Int, completion: @escaping (_ success: Bool) -> ()) {
        pathList.child(partnerListroomId).child(meListroomId).child("messageNum").setValue("\(number)")
        completion(true)
    }
    
    func checkValid() -> Bool {
        self.view.endEditing(true)
        if edt_msgSend.text!.isEmpty {
            return false
        }
        return true
    }
    
    func doSend(_ online: Bool) {
        if  self.isBlock == "true"{
            self.showToast("Sorry! You have been blocked from this user.")
            self.edt_msgSend.text! = ""
            self.isdosending = false
            self.working4textsend = false
            
        }else{
            if !isdosending{
                isdosending = true
                let msgcontent = edt_msgSend.text!
                self.edt_msgSend.text! = ""
                if msgcontent != ""{
                    let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                     var chatObject = [String: String]()
                     // MARK: for message object for partner - chatObject
                     chatObject["message"]     = msgcontent
                     chatObject["image"]       = ""
                     chatObject["photo"]       = thisuser!.userAvatar
                     chatObject["sender_id"]   = "\(thisuser!.id!)"
                     chatObject["time"]        = "\(timeNow)" as String
                     chatObject["name"]        = "\(thisuser!.username)"
                    let roomId1   = friendID! + "_" + "\(thisuser!.id!)"
                     // MARK: for partner message send action
                     FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
                         if status {
                            if online{
                               chatObject["read_time"] = "\(timeNow)" as String
                            }
                            let roomId2   = "\(thisuser!.id!)" + "_" +  friendID!
                             // MARK: for message object for me - chatObject same object
                             FirebaseAPI.sendMessage(chatObject, roomId2) { (status, message) in
                                
                                 if status {
                                     var listObject = [String: String]()
                                     listObject["id"]   = "\(friendID ?? "0")"
                                     // MARK: for list view for my list object - - listobject
                                     listObject["message"]        = msgcontent
                                     if let friendid = friendID{
                                        listObject["sender_id"]     = friendid
                                     }
                                     if let senderName = memberName4ChatRequest{
                                        listObject["sender_name"]    = senderName
                                     }
                                     if let senderPhoto = partnerAvatar4ChatRequest{
                                        listObject["sender_photo"]  = senderPhoto
                                     }
                                     if let senderAge = partnerAge4ChatRequestAccept{
                                        listObject["sender_age"]  = senderAge
                                     }
                                     if let senderGender = partnerGender4ChatRequest{
                                        listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                                     }
                                     if let senderKinkrole = partnerKinkRole4ChatRequest{
                                        listObject["sender_kinkRole"]  = senderKinkrole
                                     }
                                     if let senderLocation = partnerLocation4ChatRequest{
                                        listObject["sender_location"]  = senderLocation
                                     }
                                     if let senderPronoun = partnerPronoun4ChatRequest{
                                        listObject["sender_pronoun"]  = senderPronoun
                                     }
                                     listObject["time"]           = "\(timeNow)" as String
                                     let messageNumLocal = "\(self.messageNum + 1)"
                                     listObject["messageNum"]         = messageNumLocal
                                     listObject["isBlock"]            = "false"
                                     //listObject["isBlocked"]            = "false"
                                     FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + listObject["id"]!){
                                         (status,message) in
                                         if status{
                                             var listObject1 = [String: String]()
                                             let partnerid   = "\(friendID ?? "0")"
                                             // MARK:  for list view for partner's list object - listobject1
                                             listObject1["id"]              = "\(thisuser!.id!)"
                                             listObject1["message"]         = msgcontent
                                             listObject1["sender_id"]       = "\(thisuser!.id!)"
                                             listObject1["sender_name"]     = "\(thisuser!.username)"
                                             listObject1["sender_photo"]    = thisuser!.userAvatar
                                             listObject1["sender_age"]      = thisuser!.currentAge
                                             listObject1["sender_gender"]   = thisuser!.gender  == "woman" ? "W" : "M"
                                             listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                             listObject1["sender_location"] = thisuser!.location
                                             listObject1["sender_pronoun"]  = thisuser!.pronoun
                                             listObject1["time"]            = "\(timeNow)" as String
                                             listObject1["messageNum"]      = messageNumLocal
                                             listObject1["isBlock"]         = self.isBlock
                                             
                                             if !online{
                                                ApiManager.chatRequest(receiver_id: friendID!, requestType: RequestType.general_chat.rawValue, msg: msgcontent) { (isSuccess, msg,flag) in
                                                    FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                       (status,message) in
                                                       if status{
                                                           self.isdosending = false
                                                           self.working4textsend = false
                                                       }
                                                    }
                                                }
                                             }else{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                   (status,message) in
                                                   if status{
                                                       self.isdosending = false
                                                       self.working4textsend = false
                                                   }
                                                }
                                             }
                                         }
                                     }
                                 } else {
                                 }
                             }
                         } else {
                         }
                     }
                }
            }
        }
    }
    
    func uploadFile2Firebase(_ localFile: String, completion: @escaping (_ success: Bool, _ path: URL?) -> ())  {
        self.showLoadingView(vc: self)
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let riversRef = storageRef.child("/" + "\(randomString(length: 2))" + ".jpg")
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        _ = riversRef.putFile(from: URL(fileURLWithPath: localFile), metadata: metadata) { metadata, error in
            self.hideLoadingView()
            guard metadata != nil else {
                completion(false, nil)
                return
            }
            riversRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    completion(false,nil)
                    return
                }
                completion(true, downloadURL)
            }
        }
    }
    
    func imgageSend(_ online: Bool)  {
        if !working4imagesend{
            self.showLoadingView(vc: self)
            working4imagesend = true
            guard let downloadRL = self.downloadURL else {
                return
            }
             let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
             var chatObject = [String: String]()
             // MARK: for message object for partner - chatObject
             chatObject["message"]     = ""
             chatObject["image"]       = "\(downloadRL)"
             chatObject["photo"]       = thisuser!.userAvatar
             chatObject["sender_id"]   = "\(thisuser!.id!)"
             chatObject["time"]        = "\(timeNow)" as String
             chatObject["name"]        = "\(thisuser!.username)"
             let roomId1   = friendID! + "_" + "\(thisuser!.id!)"
             // MARK: for partner message send action
             FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
                 if status {
                    if online{
                       chatObject["read_time"] = "\(timeNow)" as String
                    }
                    let roomId2   = "\(thisuser!.id!)" + "_" +  friendID!
                     // MARK:  // MARK: for message object for me send action - chatObject same object
                     FirebaseAPI.sendMessage(chatObject, roomId2) { (status, message) in
                         if status {
                            var listObject = [String: String]()
                            listObject["id"]   = "\(friendID ?? "0")"
                             // MARK: for list view for my list object - - listobject
                             listObject["message"]       = "Shared a file"
                             if let friendid = friendID{
                                listObject["sender_id"]     = friendid
                             }
                             if let senderName = memberName4ChatRequest{
                                listObject["sender_name"]    = senderName
                             }
                             if let senderPhoto = partnerAvatar4ChatRequest{
                                listObject["sender_photo"]  = senderPhoto
                             }
                             if let senderAge = partnerAge4ChatRequestAccept{
                                listObject["sender_age"]  = senderAge
                             }
                             if let senderGender = partnerGender4ChatRequest{
                                listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                             }
                             if let senderKinkrole = partnerKinkRole4ChatRequest{
                                listObject["sender_kinkRole"]  = senderKinkrole
                             }
                             if let senderLocation = partnerLocation4ChatRequest{
                                 listObject["sender_location"]  = senderLocation
                             }
                             if let senderPronoun = partnerPronoun4ChatRequest{
                                 listObject["sender_pronoun"]  = senderPronoun
                             }
                             listObject["time"]          = "\(timeNow)" as String
                             let messageNumLocal = "\(self.messageNum + 1)"
                             listObject["messageNum"]        = messageNumLocal
                             listObject["isBlock"]            = "false"
                             FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + listObject["id"]!){
                                 (status,message) in
                                 if status{
                                     var listObject1 = [String: String]()
                                     let partnerid   = "\(friendID ?? "0")"
                                     // MARK:  for list view for partner's list object - listobject1
                                     listObject1["id"]              = "\(thisuser!.id!)"
                                     listObject1["message"]         = "Shared a file"
                                     listObject1["sender_id"]       = "\(thisuser!.id!)"
                                     listObject1["sender_name"]     = "\(thisuser!.username)"
                                     listObject1["sender_photo"]    = thisuser!.userAvatar
                                     listObject1["sender_age"]      = thisuser!.currentAge
                                     listObject1["sender_gender"]   = thisuser!.gender == "woman" ? "W" : "M"
                                     listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                     listObject1["sender_location"] = thisuser!.location
                                     listObject1["sender_pronoun"]  = thisuser!.pronoun
                                     listObject1["time"]            = "\(timeNow)" as String
                                     listObject1["messageNum"]      =  messageNumLocal
                                     listObject1["isBlock"]         = self.isBlock
                                      FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                        (status,message) in
                                        if status{
                                           self.hideLoadingView()
                                           self.working4imagesend = false
                                           self.isImagesending = false
                                           self.uiv_postView.isHidden = true
                                           self.imageFils.removeAll()
                                           self.imv_Post.image = nil
                                           self.tbl_Chat.scrollToBottomRow()
                                        }
                                    }
                                     /*if !online{
                                         self.messageNum += 1
                                         listObject1["messageNum"]      =  "\(self.messageNum)"
                                         /*ApiManager.submitFCM(friend_id: partnerid, notitext: "Shared a file") { (isSuccess, data) in
                                            if isSuccess{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbl_Chat.scrollToBottomRow()
                                                    }
                                                }
                                            }else{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbl_Chat.scrollToBottomRow()
                                                    }
                                                }
                                            }
                                        }*/
                                        // add for testing start part
                                        FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                            (status,message) in
                                            if status{
                                               self.hideLoadingView()
                                               self.working4imagesend = false
                                               self.uiv_postView.isHidden = true
                                               self.imageFils.removeAll()
                                               self.imv_Post.image = nil
                                               self.tbl_Chat.scrollToBottomRow()
                                            }
                                        }
                                        // end for testing part
                                     }else{
                                          listObject1["messageNum"]      =  "0"
                                          FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                            (status,message) in
                                            if status{
                                               self.hideLoadingView()
                                               self.working4imagesend = false
                                               self.uiv_postView.isHidden = true
                                               self.imageFils.removeAll()
                                               self.imv_Post.image = nil
                                               self.tbl_Chat.scrollToBottomRow()
                                            }
                                        }
                                     }*/
                                 }
                             }
                         } else {
                             //print(message)
                         }
                     }
                 } else {
                     //print(message)
                 }
             }
        }
    }
    
    func gifSend(_ online: Bool)  {
        if !isgifSending{
            isgifSending = true
            self.showLoadingView(vc: self)
            guard let gifurl = self.gifURL else {
                return
            }
             let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
             var chatObject = [String: String]()
             // MARK: for message object for partner - chatObject
             chatObject["message"]     = ""
             chatObject["image"]       = "\(gifurl)"
             chatObject["photo"]       = thisuser!.userAvatar
             chatObject["sender_id"]   = "\(thisuser!.id!)"
             chatObject["time"]        = "\(timeNow)" as String
             chatObject["name"]        = "\(thisuser!.username)"
             let roomId1   = friendID! + "_" + "\(thisuser!.id!)"
            // MARK: for partner message send action
             FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
                 if status {
                    if online{
                       chatObject["read_time"] = "\(timeNow)" as String
                    }
                    let roomId2   = "\(thisuser!.id!)" + "_" +  friendID!
                     // MARK: for message object for me - chatObject same object
                     FirebaseAPI.sendMessage(chatObject, roomId2) { (status, message) in
                         if status {
                            var listObject = [String: String]()
                            listObject["id"]   = "\(friendID ?? "0")"
                             // MARK: for list view for my list object - - listobject
                             listObject["message"]       = "Shared a file"
                             if let friendid = friendID{
                                listObject["sender_id"]     = friendid
                             }
                             if let senderName = memberName4ChatRequest{
                                listObject["sender_name"]    = senderName
                             }
                             if let senderPhoto = partnerAvatar4ChatRequest{
                                listObject["sender_photo"]  = senderPhoto
                             }
                             if let senderAge = partnerAge4ChatRequestAccept{
                                 listObject["sender_age"]  = senderAge
                              }
                              if let senderGender = partnerGender4ChatRequest{
                                 listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                              }
                              if let senderKinkrole = partnerKinkRole4ChatRequest{
                                 listObject["sender_kinkRole"]  = senderKinkrole
                              }
                              if let senderLocation = partnerLocation4ChatRequest{
                                  listObject["sender_location"]  = senderLocation
                              }
                              if let senderPronoun = partnerPronoun4ChatRequest{
                                  listObject["sender_pronoun"]  = senderPronoun
                              }
                             listObject["time"]          = "\(timeNow)" as String
                             let messageNumLocal = "\(self.messageNum + 1)"
                             listObject["messageNum"]    = messageNumLocal
                             listObject["isBlock"]            = "false"
                             FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + listObject["id"]!){
                                 (status,message) in
                                 if status{
                                     var listObject1 = [String: String]()
                                     let partnerid   = "\(friendID ?? "0")"
                                     // MARK:  for list view for partner's list object - listobject1
                                     listObject1["id"]              = "\(thisuser!.id!)"
                                     listObject1["message"]         = "Shared a file"
                                     listObject1["sender_id"]       = "\(thisuser!.id!)"
                                     listObject1["sender_name"]     = "\(thisuser!.username)"
                                     listObject1["sender_photo"]    = thisuser!.userAvatar
                                     listObject1["sender_age"]      = thisuser!.currentAge
                                     listObject1["sender_gender"]   = thisuser!.gender == "woman" ? "W" : "M"
                                     listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                     listObject1["sender_location"] = thisuser!.location
                                     listObject1["sender_pronoun"]  = thisuser!.pronoun
                                     listObject1["time"]            = "\(timeNow)" as String
                                     listObject1["messageNum"]      =  messageNumLocal
                                     listObject1["isBlock"]         = self.isBlock
                                      FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                        (status,message) in
                                        if status{
                                           self.hideLoadingView()
                                           self.working4imagesend = false
                                           self.uiv_postView.isHidden = true
                                           self.imageFils.removeAll()
                                           self.imv_Post.image = nil
                                           self.tbl_Chat.scrollToBottomRow()
                                           self.isworkingForGif = false
                                           self.isgifSending = false
                                           NotificationCenter.default.post(name:.gifSendSuccess, object: nil)
                                        }
                                    }
                                     /*if !online{
                                         self.messageNum += 1
                                         listObject1["messageNum"]      =  "\(self.messageNum)"
                                         /*ApiManager.submitFCM(friend_id: partnerid, notitext: "Shared a file") { (isSuccess, data) in
                                            if isSuccess{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbl_Chat.scrollToBottomRow()
                                                    }
                                                }
                                            }else{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbl_Chat.scrollToBottomRow()
                                                    }
                                                }
                                            }
                                        }*/
                                        // add for testing start part
                                        FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                            (status,message) in
                                            if status{
                                               self.hideLoadingView()
                                               self.working4imagesend = false
                                               self.uiv_postView.isHidden = true
                                               self.imageFils.removeAll()
                                               self.imv_Post.image = nil
                                               self.tbl_Chat.scrollToBottomRow()
                                               self.isworking = -1
                                               NotificationCenter.default.post(name:.gifSendSuccess, object: nil)
                                            }
                                        }
                                        // end for testing part
                                     }else{
                                          listObject1["messageNum"]      =  "0"
                                          FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                            (status,message) in
                                            if status{
                                               self.hideLoadingView()
                                               self.working4imagesend = false
                                               self.uiv_postView.isHidden = true
                                               self.imageFils.removeAll()
                                               self.imv_Post.image = nil
                                               self.tbl_Chat.scrollToBottomRow()
                                               self.isworking = -1
                                               NotificationCenter.default.post(name:.gifSendSuccess, object: nil)
                                            }
                                        }
                                     }*/
                                 }
                             }
                         } else {
                             //print(message)
                         }
                     }
                 } else {
                     //print(message)
                 }
             }
        }
    }
    
    func sendGifWithOnlineCheck4Partner() {
        if !isworkingForGif{
            isworkingForGif = true
            self.getPartnerTotalMessageAndBlockState{ (isSuccess, num, isBlock) in
                if isSuccess{
                    self.messageNum = num.toInt() ?? 0
                    self.isBlock = isBlock
                    print("messageNum==>",self.messageNum)
                    self.getPartnerOnlineStatus { (isSuccess, online) in
                        if isSuccess{
                            print("successstate==>",online)
                            self.gifSend(online)
                        }else{
                            print("failedstate===>",online)
                            self.showToast("Network issue")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if !working4textsend{
            if checkValid() {
                self.getPartnerTotalMessageAndBlockState { (isSuccess, num, isBlock) in
                    if isSuccess{
                        self.messageNum = num.toInt() ?? 0
                        self.isBlock = isBlock
                        print("messageNum==>",self.messageNum)
                        self.getPartnerOnlineStatus { (isSuccess, online) in
                            if isSuccess{
                                print("successstate==>",online)
                                self.doSend(online)
                            }else{
                                print("failedstate===>",online)
                                //self.showToast("Network issue")
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func postImageBtnclicked(_ sender: Any) {
        if !isImagesending{
            isImagesending = true
            self.getPartnerTotalMessageAndBlockState { (isSuccess, num, isBlock) in
                if isSuccess{
                    self.messageNum = num.toInt() ?? 0
                    self.isBlock = isBlock
                    print("messageNum==>",self.messageNum)
                    self.getPartnerOnlineStatus { (isSuccess, online) in
                        if isSuccess{
                            print("successstate==>",online)
                            self.imgageSend(online)
                        }else{
                            print("failedstate===>",online)
                            self.showToast("Network issue")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func attachBtnClicked(_ sender: Any) {
        //self.edt_msgSend.becomeFirstResponder()
        self.isattachedViewShow = !isattachedViewShow
        self.setAttachedView(isShow: self.isattachedViewShow)
        self.isemojiViewShow = false
        self.setEmojiView(isShow: self.isemojiViewShow)
    }
    
    @IBAction func cancelImageBtnClicked(_ sender: Any) {
        self.uiv_postView.isHidden = true
        self.imageFils.removeAll()
        self.imv_Post.image = nil
    }
    
    func waringBtnClicked() {
        animVC = .blockPop
        creatNav(.blockPop, partnerid: friendID)
        openMenu(.blockPop)
    }
    
    func attachPinBtnClicked() {
        animVC = .picturePop
        creatNav(.picturePop)
        openMenu(.picturePop)
        //self.openGallery()
    }
    
    func giftBtnClicked() {
        // send gift action
        let plusVC = self.createViewControllerwithStoryBoardName(StoryBoards.MAIN, controllerName: VCs.PLUS) as! PlusVC
        plusVC.is_gift = true
        if let friend_id = friendID,let chattingUserName = memberName4ChatRequest, let avatar = partnerAvatar4ChatRequest,let lastSeenChatRequests = self.str_lastseen_4gift{
            plusVC.gift_userid = friend_id
            plusVC.str_user_username = chattingUserName
            plusVC.str_user_content = "You are going to send gift to  \(chattingUserName) with plus membership."
            plusVC.str_user_profile = avatar
            plusVC.str_user_age = lastSeenChatRequests
        }
        self.navigationController?.pushViewController(plusVC, animated: false)
    }
    
    func gotoNavPlusVC() {
        let plusVC = self.createViewControllerwithStoryBoardName(StoryBoards.MAIN, controllerName: VCs.PLUS) as! PlusVC
        plusVC.is_gift = false
        self.navigationController?.pushViewController(plusVC, animated: false)
    }
    
    func gotoNextVC(_ index: Int)  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = index
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    func createViewControllerwithStoryBoardName(_ storyBoardName: String, controllerName:
    String) -> UIViewController{
       let storyboad = UIStoryboard(name: storyBoardName, bundle: nil)
       let targetVC = storyboad.instantiateViewController(withIdentifier: controllerName)
       return targetVC
    }
    
    func showChatPop(_ pop_type: ChatType )  {
        let configuration = NBBottomSheetConfiguration(animationDuration: 0.4, sheetSize: .fixed(Constants.SCREEN_HEIGHT))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        
        if pop_type == .videopop{
            self.video_chat_pop = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: "VideoPopDlg") as! VideoPopDlg)
            if let avatar = partnerAvatar4ChatRequest{
                video_chat_pop.str_iamge_url = avatar
            }
            bottomSheetController.present(self.video_chat_pop, on: self)
            
        }else if pop_type == .voicepop{
            self.voice_chat_pop = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: "VoicePopDlg") as! VoicePopDlg)
            if let avatar = partnerAvatar4ChatRequest{
                voice_chat_pop.str_iamge_url = avatar
            }
            bottomSheetController.present(self.voice_chat_pop, on: self)
        }
    }
    
    func gifBtnClicked() {
        if thisuser!.hasPlusMembership{
            isGifShow = true
            animVC = .gifSearch
            self.creatNav(.gifSearch)
            self.openMenu(.gifSearch)
        }else{
            self.gotoNavPlusVC()
        }
        // for testing part
        /*isGifShow = true
        animVC = .gifSearch
        self.creatNav(.gifSearch)
        self.openMenu(.gifSearch)*/
    }
    
    func emojiBtnClicked() {
        if thisuser!.hasPlusMembership{
            self.isemojiViewShow = !isemojiViewShow
            self.setEmojiView(isShow: self.isemojiViewShow)
        }else{
            self.gotoNavPlusVC()
        }
        /*self.isemojiViewShow = !isemojiViewShow
        self.setEmojiView(isShow: self.isemojiViewShow)*/
    }
    
    func videoCameraBtnClicked() {
        // goto video popup dialog
        if thisuser!.hasPlusMembership{
            self.showChatPop(.videopop)
        }else{
            self.gotoNavPlusVC()
        }
        //self.showChatPop(.videopop)
    }
    
    func phonecallBtnClicked() {
        if thisuser!.hasPlusMembership{
            self.showChatPop(.voicepop)
        }else{
            self.gotoNavPlusVC()
        }
        //self.showChatPop(.voicepop)
    }
    
    func verifyCheckBtnClicked() {
        if thisuser!.hasPlusMembership{
            animVC = .videoVerifyPop
            creatNav(.videoVerifyPop)
            openMenu(.videoVerifyPop)
        }else{
            self.gotoNavPlusVC()
        }
        /*animVC = .videoVerifyPop
        creatNav(.videoVerifyPop)
        openMenu(.videoVerifyPop)*/
    }
    
    func eyeBtnClicked() {
        if thisuser!.hasPlusMembership{
            //print("read receipt false")
            var read_receipt = UserDefault.getBool(key: PARAMS.READ_RECEIPT, defaultValue: true)
            read_receipt = !read_receipt
            UserDefault.setBool(key: PARAMS.READ_RECEIPT, value: read_receipt)
            UserDefault.Sync()
            self.tbl_Chat.reloadData()
        }else{
            //self.gotoNavPlusVC()
            print("turn on and off read receiption")
        }
    }
    
    func createPlayer(_ token: String)->AVPlayer {
        let player = ZiggeoPlayer(application: m_ziggeo, videoToken: "a4ca332c013022c099834b326246597b");
        return player;
    }
    
    @IBAction func meBtnClicked(_ sender: Any) {
        print("me")
        let button = sender as! UIButton
        let indexpathrow = button.tag
        if self.msgdataSource[indexpathrow].image != "" && !self.msgdataSource[indexpathrow].image.contains(".gif"){
            if self.msgdataSource[indexpathrow].image.contains("https://firebasestorage"){
                if self.msgdataSource[indexpathrow].me{
                    let url = URL(string: self.msgdataSource[indexpathrow].image)
                    KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    let controller = CPImageViewer()
                    controller.image = image
                    self.navigationController?.delegate = self.animator
                    self.present(controller, animated: true, completion: nil)
                    })
                }
            }else{
                if self.msgdataSource[indexpathrow].me{
                    if !self.msgdataSource[indexpathrow].image.isEmpty && self.msgdataSource[indexpathrow].msgContent == "video verification"{
                        ZiggeoPlayer.createPlayerWithAdditionalParams(application: m_ziggeo, videoToken: self.msgdataSource[indexpathrow].image, params: ["client_auth":Constants.ziggeo_client_token]) { (player:ZiggeoPlayer?) in
                            DispatchQueue.main.async {
                                let playerController: AVPlayerViewController = AVPlayerViewController();
                                playerController.player = player;
                                self.present(playerController, animated: true, completion: nil);
                                playerController.player?.play();
                                do {
                                    try AVAudioSession.sharedInstance().setActive(false)
                                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, options: .defaultToSpeaker)
                                    try AVAudioSession.sharedInstance().setActive(true)
                                } catch let error as NSError {
                                    print("AVAudioSession error: \(error.localizedDescription)")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func youBtnClicked(_ sender: Any) {
        print("you")
        let button = sender as! UIButton
        let indexpathrow = button.tag
        if self.msgdataSource[indexpathrow].image != "" && !self.msgdataSource[indexpathrow].image.contains(".gif"){
            if self.msgdataSource[indexpathrow].image.contains("https://firebasestorage"){
                if !self.msgdataSource[indexpathrow].me{
                    let url = URL(string: self.msgdataSource[indexpathrow].image)
                    KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        //print(image)
                        let controller = CPImageViewer()
                        controller.image = image
                        self.navigationController?.delegate = self.animator
                        self.present(controller, animated: true, completion: nil)
                        //self.present(controller, animated: true, completion: nil)
                    })
                }
            }else{
                if !self.msgdataSource[indexpathrow].me{
                    if  !self.msgdataSource[indexpathrow].image.isEmpty && self.msgdataSource[indexpathrow].msgContent == "video verification" {
                        var roomid = ""
                        if let friend_id = friendID{
                            roomid = "\(thisuser!.id!)" + "_" + friend_id
                        }
                        functions.httpsCallable("scheduleDelete").call(["roomId": roomid,"messageId": thisuser!.id!]) { (result, error) in
                          if let error = error as NSError? {
                            if error.domain == FunctionsErrorDomain {
                            }
                            print("error")
                          }
                            ZiggeoPlayer.createPlayerWithAdditionalParams(application: self.m_ziggeo, videoToken: self.msgdataSource[indexpathrow].image, params: ["client_auth":Constants.ziggeo_client_token]) { (player:ZiggeoPlayer?) in
                                DispatchQueue.main.async {
                                    let playerController: AVPlayerViewController = AVPlayerViewController();
                                    playerController.player = player;
                                    self.present(playerController, animated: true, completion: nil);
                                    playerController.player?.play();
                                    do {
                                        try AVAudioSession.sharedInstance().setActive(false)
                                        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, options: .defaultToSpeaker)
                                        try AVAudioSession.sharedInstance().setActive(true)
                                    } catch let error as NSError {
                                        print("AVAudioSession error: \(error.localizedDescription)")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        self.uiv_postView.isHidden = true
        self.imageFils.removeAll()
        self.imv_Post.image = nil
    }
}

@available(iOS 11.0, *)
extension MessageSendVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.msgdataSource.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // check currrent block status
        if self.msgdataSource[indexPath.section].image == ""{
            let cell = tbl_Chat.dequeueReusableCell(withIdentifier: "ChatCell", for:indexPath) as! ChatCell
            cell.selectionStyle = .none
            cell.entity = msgdataSource[indexPath.section]
            
            if self.msgdataSource[indexPath.section].me{
                cell.meView.isHidden = false
                cell.youView.isHidden = true
                cell.imv_partner.isHidden = true
                cell.youlblTime.isHidden = true
                cell.melblTime.isHidden = false
                cell.lbl_readReceipt.isHidden = false
            }else{
                cell.meView.isHidden = true
                cell.youView.isHidden = false
                cell.imv_partner.isHidden = false
                cell.youlblTime.isHidden = false
                cell.melblTime.isHidden = true
                cell.lbl_readReceipt.isHidden = true
            }
            return cell
        }else{
            if self.msgdataSource[indexPath.section].image.contains(".gif"){
                let cell = tbl_Chat.dequeueReusableCell(withIdentifier: "chatGifCell", for:indexPath) as! chatGifCell
                cell.selectionStyle = .none
                cell.entity = msgdataSource[indexPath.section]
                cell.btn_me.tag = indexPath.section
                cell.btn_you.tag = indexPath.section
                if self.msgdataSource[indexPath.section].me{
                    cell.uiv_me.isHidden = false
                    cell.uiv_you.isHidden = true
                    cell.imv_partner.isHidden = true
                    cell.btn_me.isHidden = false
                    cell.btn_you.isHidden = true
                }else{
                    cell.uiv_me.isHidden = true
                    cell.uiv_you.isHidden = false
                    cell.imv_partner.isHidden = false
                    cell.btn_me.isHidden = true
                    cell.btn_you.isHidden = false
                }
                return cell
            }else{
                if msgdataSource[indexPath.section].image.contains("https://firebasestorage") {
                    let cell = tbl_Chat.dequeueReusableCell(withIdentifier: "chatImageCell", for:indexPath) as! chatImageCell
                    cell.selectionStyle = .none
                    cell.entity = msgdataSource[indexPath.section]
                    cell.btn_me.tag = indexPath.section
                    cell.btn_you.tag = indexPath.section
                    if self.msgdataSource[indexPath.section].me{
                        cell.uiv_me.isHidden = false
                        cell.uiv_you.isHidden = true
                        cell.imv_partner.isHidden = true
                        cell.btn_me.isHidden = false
                        cell.btn_you.isHidden = true
                    }else{
                        cell.uiv_me.isHidden = true
                        cell.uiv_you.isHidden = false
                        cell.imv_partner.isHidden = false
                        cell.btn_me.isHidden = true
                        cell.btn_you.isHidden = false
                    }
                    return cell
                }else{
                    let cell = tbl_Chat.dequeueReusableCell(withIdentifier: "chatVideoCell", for:indexPath) as! chatVideoCell
                    cell.selectionStyle = .none
                    cell.didSet( msgdataSource[indexPath.section])
                    cell.btn_me.tag = indexPath.section
                    cell.btn_you.tag = indexPath.section
                    if self.msgdataSource[indexPath.section].me{
                        cell.uiv_me.isHidden = false
                        cell.uiv_you.isHidden = true
                        cell.imv_partner.isHidden = true
                        cell.btn_me.isHidden = false
                        cell.btn_you.isHidden = true
                    }else{
                        cell.uiv_me.isHidden = true
                        cell.uiv_you.isHidden = false
                        cell.imv_partner.isHidden = false
                        cell.btn_me.isHidden = true
                        cell.btn_you.isHidden = false
                    }
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 100
        }else{
            return cellSpacingHeight
        }
    }
}

@available(iOS 11.0, *)
extension MessageSendVC : EmojiViewDelegate{
    
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        self.edt_msgSend.insertText(emoji)
    }
    
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        self.edt_msgSend.deleteBackward()
    }
}

@available(iOS 11.0, *)
extension MessageSendVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.tbl_Chat.scrollToBottomRow()
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        self.tbl_Chat.scrollToBottomRow()
    }
}

@available(iOS 11.0, *)
extension MessageSendVC: ImagePickerDelegate1{
    
    func didSelect(image: UIImage?) {
        self.gotoUploadProfile(image)
    }
}

 
//MARK: UICollectionViewDataSource
@available(iOS 11.0, *)
extension MessageSendVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ds_attachMenuData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachCell", for: indexPath) as! AttachCell
        cell.entity = ds_attachMenuData[indexPath.row]
        cell.uiv_back.cornerRadius = collectionView.frame.size.height * 0.4
        if indexPath.row != 3{
            cell.lbl_plus.isHidden = true
        }else{
            cell.lbl_plus.isHidden = false
        }
        return cell
    }
}

@available(iOS 11.0, *)
extension MessageSendVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:// warning
            self.waringBtnClicked()
            break
        case 1:// attach
            self.attachPinBtnClicked()
            break
        case 2:// gift
            self.giftBtnClicked()
            break
        case 3:// blank
            //print("plusBtnClicked")
            self.gotoNavPlusVC()
            break
        case 4:// gif
            self.gifBtnClicked()
            break
        case 5:// emoji
            self.emojiBtnClicked()
            break
        case 6:// phonecall
            self.phonecallBtnClicked()
            break
        case 7:// videocall
            self.videoCameraBtnClicked()
            break
        case 8:// verify
            self.verifyCheckBtnClicked()
            break
        case 9:// eye
            self.eyeBtnClicked()
            break
        default:
            print("clicked")
        }
        // TODO: goto detail show vc
    }
}

@available(iOS 11.0, *)
extension MessageSendVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.frame.size.height * 0.8
        let w = h
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
}

@available(iOS 11.0, *)
extension MessageSendVC{
    
    func showLoadingView(vc: UIViewController, label: String = "") {
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        if label != "" {
            hud!.label.text = label
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.bezelView.color = .clear
        hud!.bezelView.style = .solidColor
    }
    
    func hideLoadingView() {
       if let hud = hud {
           hud.hide(animated: true)
       }
    }
    
    func addNavBarImage() {
        let navController = navigationController!
        let image = UIImage(named: "nabar_top_title") //Your logo url here
        let imageView = UIImageView(image: image)
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth / 2, height: bannerHeight / 2)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    func addLeftButton4NavBar() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "btn_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedLeftBtn), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom:8, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func addTappedLeftBtn() {
        if let currentVC = currentVC{
            if currentVC == VCs.MESSAGESEND{
                if let friendid = friendID{
                    self.removeNode4me4finish(friendid)
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
                    //toVC.selectedIndex = 1
                    //toVC.modalPresentationStyle = .fullScreen
                    UIApplication.shared.keyWindow?.rootViewController = toVC
                    /*self.setBaseStatus4me("finish", partnerId: friendid) { (isSuccess) in
                        if isSuccess{
                            DispatchQueue.main.async {
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
                                //toVC.selectedIndex = 1
                                //toVC.modalPresentationStyle = .fullScreen
                                UIApplication.shared.keyWindow?.rootViewController = toVC
                            }
                        }
                    }*/
                }
            }
        }
    }
    
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func removeNode4me4finish(_ partnerId: String){
        requestPath.child("u" + thisuser!.id!).child("u" + partnerId).removeValue()
    }
    
    func creatNav(_ vcType: AnimVC, partnerid: String? = "") {
        if vcType == .photoCrop{
            cropPhotoVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CropPhotoVC") as! CropPhotoVC)
            cropPhotoVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .moreLinkRole{
            moreRoleVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreLinkRoleVC") as! MoreLinkRoleVC)
            moreRoleVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .moreIdentify{
            moreIdentify = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreIdentifyVC") as! MoreIdentifyVC)
            moreIdentify.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .datePicker{
            datePickerVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC)
            datePickerVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .gifSearch{
            gifSearchVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.GIFSEARCH) as! SearchVC)
            gifSearchVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .blockPop{
            blockPopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.BLOCKPOPUP) as! BlockPopUpVC)
            blockPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            blockPopVC.partnerid = partnerid
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .blockPop1{
            blockPopVC1 = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.BLOCKPOPUP1) as! BlockPopUpVC1)
            blockPopVC1.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            /*darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)*/
        }else if vcType == .picturePop{
            picturePopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.PICTUREATTACHPOPUP) as! PictureAttachVC)
            picturePopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }else if vcType == .videoVerifyPop{
            videoVerifyPopVC = (UIStoryboard(name: StoryBoards.CHAT, bundle: nil).instantiateViewController(withIdentifier: VCs.VIDEOVERIFYPOP) as! VideoVerifyPopUpVC)
            videoVerifyPopVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
            darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }
    }
    
    func openMenu(_ vcType: AnimVC){
        if vcType == .photoCrop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                cropPhotoVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(cropPhotoVC.view)
                self.addChild(cropPhotoVC)
            })
        }else if vcType == .moreLinkRole{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                moreRoleVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(moreRoleVC.view)
                self.addChild(moreRoleVC)
            })
        }else if vcType == .moreIdentify{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                moreIdentify.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(moreIdentify.view)
                self.addChild(moreIdentify)
            })
        }else if vcType == .datePicker{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                datePickerVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(datePickerVC.view)
                self.addChild(datePickerVC)
            })
        }else if vcType == .gifSearch{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                gifSearchVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(gifSearchVC.view)
                self.addChild(gifSearchVC)
            })
        }else if vcType == .blockPop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(blockPopVC.view)
                self.addChild(blockPopVC)
            })
        }else if vcType == .blockPop1{
            /*darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)*/
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                blockPopVC1.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(blockPopVC1.view)
                self.addChild(blockPopVC1)
            })
        }else if vcType == .picturePop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                picturePopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(picturePopVC.view)
                self.addChild(picturePopVC)
            })
        }else if vcType == .videoVerifyPop{
            darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(darkVC.view)
            self.addChild(darkVC)
            _ = self.navigationController?.navigationBar.height
            UIView.animate(withDuration: 0.5, animations: {
                videoVerifyPopVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                self.view.addSubview(videoVerifyPopVC.view)
                self.addChild(videoVerifyPopVC)
            })
        }
    }
}

enum ChatType {
    case videopop
    case voicepop
}
