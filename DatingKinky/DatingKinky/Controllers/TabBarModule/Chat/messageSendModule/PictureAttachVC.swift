//
//  PictureAttachVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class PictureAttachVC: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var lbl_messageRefer: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_content.text = "This is the kinky dating site, and you're an adult, so it's entirely possibly your image is explicit. Your chat mate will be alerted before they view. It's good idea to get consent before sending naughty stuff. Still want to send?"
        self.lbl_messageRefer.text = "(You'll only see this message once per conversation.)"
        self.lbl_messageRefer.font = self.lbl_messageRefer.font.italic
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        
        self.closeMenu(.picturePop)
    }
    
    @IBAction func consentBtnClicked(_ sender: Any) {
        
        self.closeMenu(.picturePop)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            NotificationCenter.default.post(name:.opneGallery, object: nil)
        }
    }
    
    @IBAction func holdoffBtnClicked(_ sender: Any) {
        
        self.closeMenu(.picturePop)
    }
}
