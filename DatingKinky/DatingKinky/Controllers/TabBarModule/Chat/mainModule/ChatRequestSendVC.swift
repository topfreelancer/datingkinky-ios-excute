//
//  ChatRequestPopUpVC.swift
//  DatingKinky
//
//  Created by top Dev on 8/8/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ChatRequestSendVC: BaseVC {

    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_dlg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let memebername = memberName4ChatRequest{
            self.lbl_content.text = "\(memebername) has been asked if they would like to chat with you. if they agree, you'll get a notification.\n \nIf not, better luck next time!"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    /*@objc func chatRequest(){
        if let memebername = memberName4ChatRequest{
            self.lbl_content.text = "\(memebername) has been asked if they would like to chat with you. if they agree, you'll get a notification.\n \nIf not, better luck next time!"
        }
    }*/
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        selectedUser = nil
        friendID = nil
        memberProfile4ChatRequest = nil
        memberName4ChatRequest = nil
        partnerAge4ChatRequestAccept = nil
        partnerAvatar4ChatRequest = nil
        partnerGender4ChatRequest = nil
        partnerKinkRole4ChatRequest = nil
        partnerLocation4ChatRequest = nil
        partnerPronoun4ChatRequest = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gotitBtnClicked(_ sender: Any) {
        selectedUser = nil
        friendID = nil
        memberProfile4ChatRequest = nil
        memberName4ChatRequest = nil
        partnerAge4ChatRequestAccept = nil
        partnerAvatar4ChatRequest = nil
        partnerGender4ChatRequest = nil
        partnerKinkRole4ChatRequest = nil
        partnerLocation4ChatRequest = nil
        partnerPronoun4ChatRequest = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        
        selectedUser = nil
        friendID = nil
        memberProfile4ChatRequest = nil
        memberName4ChatRequest = nil
        partnerAge4ChatRequestAccept = nil
        partnerAvatar4ChatRequest = nil
        partnerGender4ChatRequest = nil
        partnerKinkRole4ChatRequest = nil
        partnerLocation4ChatRequest = nil
        partnerPronoun4ChatRequest = nil
        self.dismiss(animated: true, completion: nil)
    }
}
