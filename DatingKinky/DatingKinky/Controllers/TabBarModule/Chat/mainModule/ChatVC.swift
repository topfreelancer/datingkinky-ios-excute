//
//  ChatVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import YHPlaceHolderView

var random_count = 0
var is_random_switch = false
class ChatVC: BaseVC {
    
    @IBOutlet weak var cons_h_chatReqest: NSLayoutConstraint!
    @IBOutlet weak var cons_h_colRecentChats: NSLayoutConstraint!
    @IBOutlet weak var cons_h_allChats: NSLayoutConstraint!
    
    @IBOutlet weak var col_recentChats: UICollectionView!
    @IBOutlet weak var col_chatRequest: UICollectionView!
    @IBOutlet weak var col_allChats: UICollectionView!
  
    @IBOutlet weak var lbl_chatRequestNum: UILabel!
    @IBOutlet weak var lbl_totalChat: UILabel!
    @IBOutlet weak var lbl_reenceChat: UILabel!
    
    var isRandomChat = false
    
    var ds_recentChats = [AllChatModel]()
    var ds_originalchatRequests = [ChatRequestModel]()
    var ds_chatRequests = [ChatRequestModel]()
    //var ds_allChats1 = [AllChatModel]()
    var ds_allChats = [AllChatModel]()
    var userlistHandle: UInt?
    var chatRequestHandle: UInt?
    var randomUserlistHandle: UInt?
    let requestPath = Database.database().reference().child("request")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //NotificationCenter.default.removeObserver(self)
        if let id = thisuser?.id, let valueChangedHandle = chatRequestHandle , let userlistHandle = userlistHandle {
            FirebaseAPI.removeChattingRequestObserver(userRoomid: "u" + id, valueChangedHandle)
            FirebaseAPI.removeUserlistObserver(userRoomid: "u" + id, userlistHandle)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showLoadingView(vc: self)
        ApiManager.getBlockUsers { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let chatRequest = "u" + "\(thisuser!.id ?? "")"
                if block_parameters!.snooze_account != "1" && block_parameters!.isblock_chat_request != "1"{
                    self.chatRequestListner(chatRequest)
                }
                self.userlistListner(chatRequest)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func chatRequestListner(_ userRoomid : String)  {
        self.ds_originalchatRequests.removeAll()
        self.ds_chatRequests.removeAll()
        self.col_chatRequest.reloadData()
        //self.showLoadingView(vc: self)HUDHUD()
        chatRequestHandle = FirebaseAPI.getChatRequest(userRoomid : userRoomid ,handler: { (userlist) in
            //print("requestlist == ",userlist)
                guard userlist.receiver_id != nil else{
                    //self.hideLoadingView()
                    return
                }

                self.ds_originalchatRequests.append(userlist)

                if self.ds_originalchatRequests.count != 0{
                    var ids = [String]()
                    for one in self.ds_originalchatRequests{
                        ids.append(one.receiver_id!)
                    }
                    //print(ids.removeDuplicates())
                    var num = 0
                    
                    for  one in ids.removeDuplicates(){
                        num += 1
                        if let chatRequestModel = self.getChatRequestUserFromIDandSeperateWithPending(one){
                            self.ds_chatRequests.append(chatRequestModel)
                        }

                        if num == ids.removeDuplicates().count{
                            //dump(self.msgDatasource1, name: "msgDatasource1====>")
                            //dump(self.msgDatasource, name: "msgDatasource====.")
                            if self.ds_chatRequests.count == 0{
                                //self.uiv_chatRequestPlaceHolder.isHidden = false
                                self.lbl_chatRequestNum.text = "0"
                                self.col_chatRequest.reloadData()
                            }else{
                                self.lbl_chatRequestNum.text = "\(self.ds_chatRequests.count)"
                                self.col_chatRequest.reloadData()
                            }
                        }
                    }
                }else{
                    self.lbl_chatRequestNum.text = "0"
                    self.col_chatRequest.reloadData()
                }
            }
        )
    }
    
    func getChatRequestUserFromIDandSeperateWithPending(_ id: String) -> ChatRequestModel? {
        var returnModel: ChatRequestModel? = nil
        for one in self.ds_originalchatRequests{
            if id == one.receiver_id && one.state == "pending"{
                returnModel = one
            }
        }
        return returnModel
    }
    
    func userlistListner(_ userRoomid : String)  {
        //self.ds_allChats1.removeAll()
        self.ds_allChats.removeAll()
        self.ds_recentChats.removeAll()
        self.col_allChats.reloadData()
        self.lbl_totalChat.text = "0"
        self.lbl_reenceChat.text = "0"
        self.col_recentChats.reloadData()
        //self.showLoadingView(vc: self)HUDHUD()
            userlistHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            if let userlist = userlist{
                guard userlist.id != nil else{
                    //self.hideLoadingView()
                    return
                }
            }
            if let userlist = userlist{
                if userlist.isBlock != "true" && !self.getBlockStatus(userlist.id!){
                    self.ds_allChats.append(userlist)
                }
            }
                
            if self.ds_allChats.count == 0{
                self.lbl_totalChat.text = "0"
                self.lbl_reenceChat.text = "0"
                self.col_allChats.reloadData()
                self.col_recentChats.reloadData()
            }else{
                var num = 0
                for one in self.ds_allChats{
                    num += 1
                    self.ds_recentChats.append(one)
                    if num == self.ds_allChats.count || num == 30{
                        self.col_recentChats.reloadData()
                        self.lbl_reenceChat.text = "\(self.ds_recentChats.count)"
                    }
                }
                self.lbl_totalChat.text = "\(self.ds_allChats.count)"
                self.col_allChats.reloadData()
                
            }
            /*if let userlist = userlist{
                 self.ds_allChats1.append(userlist)
            }*/

            /*if self.ds_allChats1.count != 0{
                var ids = [String]()
                for one in self.ds_allChats1{
                    ids.append(one.id!)
                }
                //print(ids.removeDuplicates())
                var num = 0
                for  one in ids.removeDuplicates(){
                    num += 1
                    if let allchat = self.getDataFromID(one){
                        if allchat.isBlock != "true" && !self.getBlockStatus(one){
                            self.ds_allChats.append(allchat)
                        }
                    }

                    if num == ids.removeDuplicates().count{
                        //dump(self.msgDatasource1, name: "msgDatasource1====>")
                        //dump(self.msgDatasource, name: "msgDatasource====.")
                        var num = 0
                        for one in self.ds_allChats{
                            num += 1
                            self.ds_recentChats.append(one)
                            if num < 30 || num == self.ds_allChats.count{
                                if self.ds_recentChats.count == 0{
                                    self.lbl_reenceChat.text = "0"
                                }else{
                                    self.lbl_reenceChat.text = "\(self.ds_recentChats.count)"
                                    self.col_recentChats.reloadData()
                                }
                            }
                        }
                        if self.ds_allChats.count == 0{
                            self.lbl_totalChat.text = "0"
                            self.col_allChats.reloadData()
                            self.col_recentChats.reloadData()
                        }else{
                            self.lbl_totalChat.text = "\(self.ds_allChats.count)"
                            self.col_allChats.reloadData()
                            self.col_recentChats.reloadData()
                        }
                    }
                }
            }else{
                self.lbl_totalChat.text = "0"
                self.lbl_reenceChat.text = "0"
            }*/
        })
    }
    
    func getBlockStatus(_ id: String) -> Bool {
        var status = false
        if let blocks = blockusers.getBlockUsers_BlockedUsers(){
            if blocks.count > 0{
                for one in blocks{
                    if one == id{
                        status = true
                        break
                    }
                }
            }
        }
        return status
    }
    
    /*func getDataFromID(_ id: String) -> AllChatModel? {
        var returnModel : AllChatModel?
        for one in self.ds_allChats1{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel
    }*/
    
    func setUI()  {
        
        self.addNavBarImage()
        self.addBackButtonNavBar()
        self.cons_h_colRecentChats.constant = (Constants.SCREEN_WIDTH - 40) / 3.5 * 1.4
        self.cons_h_chatReqest.constant = (Constants.SCREEN_WIDTH - 40) / 1.2 * 0.55
        self.cons_h_allChats.constant = Constants.SCREEN_HEIGHT - 200
        //self.lbl_plusOnly.font = lbl_plusOnly.font.italic
        //self.lbl_randomChatContent.font = lbl_randomChatContent.font.italic
        
        self.col_chatRequest.enablePlaceHolderView = true
        self.col_recentChats.enablePlaceHolderView = true
        self.col_allChats.enablePlaceHolderView = true
        
        let placeholderview = recentChatPlaceHolder().loadNib() 
        
        placeholderview.frame = CGRect(x: (Constants.SCREEN_WIDTH - 40) / 2 - 40, y: 50, width: 80, height: 40)
        self.col_chatRequest.yh_PlaceHolderView = placeholderview
        
        let recentPlaceholder = chatPlaceHolder().loadNib()
        
        recentPlaceholder.frame = CGRect(x: (Constants.SCREEN_WIDTH - 40) / 2 - 40, y: 30, width: 80, height: 40)
        self.col_recentChats.yh_PlaceHolderView = recentPlaceholder
        
        
        let allchatplaceHolder = chatPlaceHolder().loadNib()
        
        allchatplaceHolder.frame = CGRect(x: (Constants.SCREEN_WIDTH - 40) / 2 - 40, y: (Constants.SCREEN_HEIGHT - 200) / 2 - 30, width: 80, height: 40)
        self.col_allChats.yh_PlaceHolderView = allchatplaceHolder
    }
    
    @IBAction func chatBtnClicked(_ sender: Any) {
        // send chatting request push notification
        // first goto chatting room and set the node state as "accept" for partner
        let button = sender as! UIButton
        let index = button.tag
        memberName4ChatRequest = self.ds_chatRequests[index].userName
        partnerGender4ChatRequest = self.ds_chatRequests[index].userGender
        partnerLocation4ChatRequest = self.ds_chatRequests[index].userLocation
        partnerPronoun4ChatRequest = self.ds_chatRequests[index].userPronoun
        let gender = self.ds_chatRequests[index].userGender == "woman" ? "W" : "M"
        let spaces = String(repeating: " ", count: 1)
        let one = self.ds_chatRequests[index]
        memberProfile4ChatRequest = one.userCurrentAge! + gender
        if let kinkrole = one.userKinkRole, !kinkrole.isEmpty{
            memberProfile4ChatRequest! += spaces + kinkrole
        }
        if let showhim = one.userPronoun, !showhim.isEmpty{
            memberProfile4ChatRequest! += spaces + "•" + spaces + showhim
        }
        if let location = one.userLocation, !location.isEmpty{
            memberProfile4ChatRequest! += spaces + "•" + spaces + location + spaces + "•" + spaces + "Last seen"
        }
        partnerAge4ChatRequestAccept = self.ds_chatRequests[index].userCurrentAge
        partnerAvatar4ChatRequest = self.ds_chatRequests[index].userAvatar
        partnerKinkRole4ChatRequest = self.ds_chatRequests[index].userKinkRole
        friendID = self.ds_chatRequests[index].receiver_id
        
        // last seen show function
        let lastSeenTimeStamp = self.ds_chatRequests[index].lastSeen
        lastSeenChatRequest = ""
        let now = Date()
        let calendar = Calendar.current
        if let lastDate = getDateAndTime(lastSeenTimeStamp!){
            print(lastDate as Any)
            let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
            let seconds = ageComponents.second ?? 0
            print(seconds)
            let day = secondsToHoursMinutesSeconds(seconds: seconds).0
            let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
            let min = secondsToHoursMinutesSeconds(seconds: seconds).2
            let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
            lastSeenChatRequest! = " " + "\(secondss)" + "s"
            if min > 0{
                lastSeenChatRequest! = " " + "\(min)" + "m"
            }
            if hour > 0{
                lastSeenChatRequest! = " " + "\(hour)" + "h"
            }
            if day > 0{
                lastSeenChatRequest! = "\(day)" + "d"
            }
        }
        // send push notification for accept chatting
        self.showLoadingView(vc: self)
        ApiManager.chatRequest(receiver_id: self.ds_chatRequests[index].receiver_id!, requestType: RequestType.chat_accept.rawValue) { (isSuccess, msg,flag) in
            self.hideLoadingView()
            if isSuccess{
                // set firebase node value
                if flag == 1{
                    self.setStatus4partner("accept", partnerId: friendID!) { (isSuccess1) in
                        if isSuccess1{
                            self.removeNode4me4accept(friendID!)
                            // create list node
                            // here must update list and goto message send vc
                            // MARK: for list view for my list object - - listobject
                            var listObject = [String: String]()
                            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                            listObject["id"]   = friendID!
                            listObject["message"]        = "Hi!"
                            if let friendid = friendID{
                               listObject["sender_id"]     = friendid
                            }
                            if let senderName = memberName4ChatRequest{
                               listObject["sender_name"]    = senderName
                            }
                            if let senderPhoto = partnerAvatar4ChatRequest{
                               listObject["sender_photo"]  = senderPhoto
                            }
                            if let senderAge = partnerAge4ChatRequestAccept{
                               listObject["sender_age"]  = senderAge
                            }
                            if let senderGender = partnerGender4ChatRequest{
                               listObject["sender_gender"] = senderGender == "woman" ? "W" : "M"
                            }
                            if let senderKinkrole = partnerKinkRole4ChatRequest{
                               listObject["sender_kinkRole"]  = senderKinkrole
                            }
                            if let senderLocation = partnerLocation4ChatRequest{
                               listObject["sender_location"]  = senderLocation
                            }
                            if let senderPronoun = partnerPronoun4ChatRequest{
                               listObject["sender_pronoun"]  = senderPronoun
                            }
                            listObject["time"]           = "\(timeNow)" as String
                            let messageNumLocal = "0"
                            listObject["messageNum"]         = messageNumLocal
                            listObject["isBlock"]            = "false"
                            FirebaseAPI.sendListUpdate(listObject, "u" + "\(thisuser!.id!)", partnerid: "u" + friendID!){
                                (status,message) in
                                if status{
                                    var listObject1 = [String: String]()
                                    let partnerid   = "\(friendID ?? "0")"
                                    // MARK:  for list view for partner's list object - listobject1
                                    listObject1["id"]              = "\(thisuser!.id!)"
                                    listObject1["message"]         = "Hi!"
                                    listObject1["sender_id"]       = "\(thisuser!.id!)"
                                    listObject1["sender_name"]     = "\(thisuser!.username)"
                                    listObject1["sender_photo"]    = thisuser!.userAvatar
                                    listObject1["sender_age"]      = thisuser!.currentAge
                                    listObject1["sender_gender"]   = thisuser!.gender  == "woman" ? "W" : "M"
                                    listObject1["sender_kinkRole"] = thisuser!.kinkRole
                                    listObject1["sender_location"] = thisuser!.location
                                    listObject1["sender_pronoun"]  = thisuser!.pronoun
                                    listObject1["time"]            = "\(timeNow)" as String
                                    listObject1["messageNum"]      = messageNumLocal
                                    listObject1["isBlock"]         = "false"
                                    FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(thisuser!.id!)"){
                                       (status,message) in
                                       if status{
                                            self.gotoStoryBoardVC(StoryBoards.CHAT, name: VCs.MESSAGESENDNAV, fullscreen: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    self.showAlerMessage(message: msg ?? "")
                }
                
            }else{
                self.showToast("Network issue")
            }
        }
    }
    
    @IBAction func moreBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        let userid = self.ds_chatRequests[index].receiver_id
        let username = self.ds_chatRequests[index].userName
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.SETTINGS, controllerName: "YourProfileVC") as! YourProfileVC
        tovc.is_myprofle = false
        tovc.user_id = userid
        tovc.user_name = username
        self.navigationController?.pushViewController(tovc, animated: true)
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        let ref = Database.database().reference()
        let userchattingRoom = "u" + thisuser!.id!
        let partnerid = "u" + self.ds_allChats[index].id!
        
        ref.child("list").child(userchattingRoom).child(partnerid).removeValue{
            error, _ in
            print(error as Any)
        }
        ref.child("list").child(partnerid).child(userchattingRoom).removeValue{
            error, _ in
            print(error as Any)
        }
        ref.child("message").child(thisuser!.id! + "_" + self.ds_allChats[index].id!).removeValue{
            error, _ in
            print(error as Any)
        }
        ref.child("message").child(self.ds_allChats[index].id! + "_" + thisuser!.id!).removeValue{
            error, _ in
            print(error as Any)
        }
        if ds_allChats.count > index{
            self.ds_allChats.remove(at: index)
            self.col_allChats.reloadData()
            self.lbl_totalChat.text = "\(self.ds_allChats.count)"
        }
        if ds_recentChats.count > index{
            self.ds_recentChats.remove(at: index)
            self.col_recentChats.reloadData()
            self.lbl_reenceChat.text = "\(self.ds_recentChats.count)"
        }
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        /* requestPath.child("u" + partnerId).child("u" + thisuser!.id!).child("state").setValue(state)*/
        let button = sender as! UIButton
        let index = button.tag
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
         var chatObject = [String: String]()
         // MARK: for message object for partner - chatObject
        chatObject["message"]     = Constants.BLOCK_CONTENT_FIRST + "\n\n\n" + " " + thisuser!.username + Constants.BLOCK_CONTENT_LAST
        chatObject["image"]       = ""
        chatObject["photo"]       = thisuser!.userAvatar
        chatObject["sender_id"]   = "\(thisuser!.id!)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(thisuser!.username)"
        let roomId1   = self.ds_allChats[index].id! + "_" + "\(thisuser!.id!)"
        // MARK: for partner message send action
        FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
            if status {
                let ref = Database.database().reference()
                let userchattingRoom = "u" + thisuser!.id!
                let partnerid = "u" + self.ds_allChats[index].id!
                ref.child("list").child(userchattingRoom).child(partnerid).child("isBlock").setValue("true")
                self.showLoadingView(vc: self)
                ApiManager.addBlockUsers(blocks: self.ds_allChats[index].id!) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        //dump(blockusers,name: "thisisblockusers")
                        DispatchQueue.main.async {
                            if self.ds_allChats.count > index{
                                self.ds_allChats.remove(at: index)
                                self.col_allChats.reloadData()
                                self.lbl_totalChat.text = "\(self.ds_allChats.count)"
                            }
                            if self.ds_recentChats.count > index{
                                self.ds_recentChats.remove(at: index)
                                self.col_recentChats.reloadData()
                                self.lbl_reenceChat.text = "\(self.ds_recentChats.count)"
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnDennyClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        ApiManager.chatRequest(receiver_id: self.ds_chatRequests[index].receiver_id!, requestType:RequestType.chat_reject.rawValue) { (isSuccess, msg,flag) in
            if isSuccess{
                let ref = Database.database().reference()
                let userchattingRoom = "u" + thisuser!.id!
                let partnerid = "u" + self.ds_chatRequests[index].receiver_id!
                ref.child("request").child(userchattingRoom).child(partnerid).removeValue()
                ref.child("request").child(partnerid).child(userchattingRoom).removeValue()
                if self.ds_chatRequests.count > index{
                    self.ds_chatRequests.remove(at: index)
                    self.col_chatRequest.reloadData()
                    self.lbl_chatRequestNum.text = "\(self.ds_chatRequests.count)"
                }
            }
        }
    }
    
    @IBAction func spaceBtnClicked(_ sender: Any) {
        print("space")
        let button = sender as! UIButton
        let index = button.tag
        self.ds_allChats[index].showOverlay = false
        var indexPaths = [IndexPath]()
        indexPaths.removeAll()
        indexPaths.append(IndexPath.init(row: index, section: 0))
        col_allChats.reloadItems(at: indexPaths)
    }
    
    func removeNode4me4accept(_ partnerId: String){
        requestPath.child("u" + thisuser!.id!).child("u" + partnerId).removeValue()
    }
    
    func setStatus4partner(_ state: String, partnerId: String, completion: @escaping (_ success: Bool) -> ()) {
        requestPath.child("u" + partnerId).child("u" + thisuser!.id!).child("state").setValue(state)
        completion(true)
    }
    
    func setStatus4me(_ state: String, partnerId: String, completion: @escaping (_ success: Bool) -> ()) {
        requestPath.child("u" + thisuser!.id!).child("u" + partnerId).child("state").setValue(state)
        completion(true)
    }
}

extension ChatVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == col_recentChats{
            return self.ds_recentChats.count
        }else if collectionView == col_chatRequest{
            return self.ds_chatRequests.count
        }else{
            return self.ds_allChats.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == col_recentChats{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentChatCell", for: indexPath) as! RecentChatCell
            cell.entity = self.ds_recentChats[indexPath.row]
            //cell.roundShadow(cornerRadius: 8)
            return cell
        }else if collectionView == col_chatRequest{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatRequestCell", for: indexPath) as! ChatRequestCell
            cell.entity = self.ds_chatRequests[indexPath.row]
            //cell.roundShadow(cornerRadius: 8)
            cell.btn_chat.tag = indexPath.row
            cell.btn_denny.tag = indexPath.row
            cell.btn_more.tag = indexPath.row
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllChatCell", for: indexPath) as! AllChatCell
            cell.entity = self.ds_allChats[indexPath.row]
            //cell.delegate = self
            //cell.addBlurToView()
            let leftSwipe = MySwipeGesture(target: self, action: #selector(handleSwipeLeft(_:)))
            leftSwipe.direction = .left
            leftSwipe.cell = cell
            cell.addGestureRecognizer(leftSwipe)
            cell.btn_spaceBtn.tag = indexPath.row
            cell.btn_delete.tag = indexPath.row
            cell.btn_decline.tag = indexPath.row
            return cell
        }
    }
    
    @objc func handleSwipeLeft(_ sender:MySwipeGesture) {
        if (sender.direction == .left) {
            print("swipe left")
            if let cell = sender.cell as? AllChatCell{
                cell.uiv_overlay.isHidden = false
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == col_recentChats{
            memberName4ChatRequest = self.ds_recentChats[indexPath.row].userName
            let gender = self.ds_recentChats[indexPath.row].gender!
            partnerLocation4ChatRequest = self.ds_recentChats[indexPath.row].location
            partnerPronoun4ChatRequest = self.ds_recentChats[indexPath.row].pronoun
            let spaces = String(repeating: " ", count: 1)
            let one = self.ds_recentChats[indexPath.row]
            memberProfile4ChatRequest = one.currentAge! + gender
            if let kinkrole = one.kinkRole, !kinkrole.isEmpty{
                memberProfile4ChatRequest! += spaces + kinkrole
            }
            if let showhim = one.pronoun, !showhim.isEmpty{
                memberProfile4ChatRequest! += spaces + "•" + spaces + showhim
            }
            if let location = one.location, !location.isEmpty{
                memberProfile4ChatRequest! += spaces + "•" + spaces + location + spaces + "•" + spaces + "Last seen"
            }
            
            partnerAge4ChatRequestAccept = self.ds_recentChats[indexPath.row].currentAge
            partnerAvatar4ChatRequest = self.ds_recentChats[indexPath.row].userAvatar
            partnerGender4ChatRequest = gender
            partnerKinkRole4ChatRequest = self.ds_recentChats[indexPath.row].kinkRole
            
            let lastSeenTimeStamp = self.ds_recentChats[indexPath.row].lastSeen
            lastSeenChatRequest = ""
            let now = Date()
            let calendar = Calendar.current
            if let lastDate = getDateAndTime(lastSeenTimeStamp!){
                print(lastDate as Any)
                let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                let seconds = ageComponents.second ?? 0
                print(seconds)
                let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                lastSeenChatRequest! = " " + "\(secondss)" + "s"
                if min > 0{
                    lastSeenChatRequest! = " " + "\(min)" + "m"
                }
                if hour > 0{
                    lastSeenChatRequest! = " " + "\(hour)" + "h"
                }
                if day > 0{
                    lastSeenChatRequest! = "\(day)" + "d"
                }
            }
            friendID = self.ds_recentChats[indexPath.row].id
            self.gotoStoryBoardVC(StoryBoards.CHAT, name: VCs.MESSAGESENDNAV, fullscreen: true)
        }else if collectionView == col_allChats{
            memberName4ChatRequest = self.ds_allChats[indexPath.row].userName
            let gender = self.ds_allChats[indexPath.row].gender!
            partnerLocation4ChatRequest = self.ds_allChats[indexPath.row].location
            partnerPronoun4ChatRequest = self.ds_allChats[indexPath.row].pronoun
            let spaces = String(repeating: " ", count: 1)
            let lastSeenTimeStamp = self.ds_allChats[indexPath.row].lastSeen
            lastSeenChatRequest = ""
            let now = Date()
            let calendar = Calendar.current
            if let lastDate = getDateAndTime(lastSeenTimeStamp!){
                print(lastDate as Any)
                let ageComponents = calendar.dateComponents([.second], from: lastDate, to: now)
                let seconds = ageComponents.second ?? 0
                print(seconds)
                let day = secondsToHoursMinutesSeconds(seconds: seconds).0
                let hour = secondsToHoursMinutesSeconds(seconds: seconds).1
                let min = secondsToHoursMinutesSeconds(seconds: seconds).2
                let secondss = secondsToHoursMinutesSeconds(seconds: seconds).3
                lastSeenChatRequest! = " " + "\(secondss)" + "s"
                if min > 0{
                    lastSeenChatRequest! = " " + "\(min)" + "m"
                }
                if hour > 0{
                    lastSeenChatRequest! = " " + "\(hour)" + "h"
                }
                if day > 0{
                    lastSeenChatRequest! = "\(day)" + "d"
                }
            }
            let one = self.ds_allChats[indexPath.row]
            memberProfile4ChatRequest = one.currentAge! + gender
            if let kinkrole = one.kinkRole, !kinkrole.isEmpty{
                memberProfile4ChatRequest! += spaces + kinkrole
            }
            if let showhim = one.pronoun, !showhim.isEmpty{
                memberProfile4ChatRequest! += spaces + "•" + spaces + showhim
            }
            if let location = one.location, !location.isEmpty{
                memberProfile4ChatRequest! += spaces + "•" + spaces + location + spaces + "•" + spaces + "Last seen"
            }
            partnerAge4ChatRequestAccept = self.ds_allChats[indexPath.row].currentAge
            partnerAvatar4ChatRequest = self.ds_allChats[indexPath.row].userAvatar
            partnerGender4ChatRequest = gender
            partnerKinkRole4ChatRequest = self.ds_allChats[indexPath.row].kinkRole
            
            friendID = self.ds_allChats[indexPath.row].id
            self.gotoStoryBoardVC(StoryBoards.CHAT, name: VCs.MESSAGESENDNAV, fullscreen: true)
        }
    }
}

extension ChatVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.col_recentChats{
            let w = collectionView.frame.size.width / 3.5
            let h: CGFloat = w * 1.55
            return CGSize(width: w, height: h)
        }else if collectionView == self.col_chatRequest{
            let w = collectionView.frame.size.width / 1.2
            let h: CGFloat = w * 0.55
            return CGSize(width: w, height: h)
        }else{
            let w = collectionView.frame.size.width
            let h: CGFloat = 120
            return CGSize(width: w, height: h)
        }
    }
}


class MySwipeGesture: UISwipeGestureRecognizer {
    var cell: UICollectionViewCell?
}
