//
//  VoiceChattingPop.swift
//  DatingKinky
//
//  Created by top Dev on 16.11.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation
import Rippl

class VoicePopDlg: BaseVC{

    @IBOutlet weak var imv_record: UIImageView!
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var imv_profile: UIImageView!
    var str_iamge_url: String?
    var timer: Timer?
    var repeat_count = 0
    
    @IBOutlet weak var ripple: Rippl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.ripple.fillColor = .clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(voiceChatRequestRejected),name: .voiceChatRequestRejected, object: nil)
        if let url = self.str_iamge_url{
            setImageWithURL(url, imv: self.imv_profile)
        }
        let downSwipee = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipess(_:)))
        downSwipee.direction = .down
        view.addGestureRecognizer(downSwipee)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func voiceChatRequestRejected() {
        self.timer?.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSwipess(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
    }
    
    @objc func fireTimer() {
        runCount += 1
        self.ripple.fillColor = UIColor.init(named: "color_icon_back")!
        self.ripple.animateImpact(strength: 8.0, duration: 2.0)
        if runCount == 60 {
            timer?.invalidate()
            self.ripple.fillColor = .clear
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func shareBtnClicked(_ sender: Any) {
        
        if let friendid = friendID{
            self.showLoadingView(vc: self)
            ApiManager.chatRequest(receiver_id: friendid, requestType: RequestType.voice_chat_send.rawValue) { [self] (isSuccess, msg,flag) in
                self.hideLoadingView()
                if isSuccess{
                    if flag == 1{
                        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.fireTimer), userInfo: nil, repeats: true)
                        
                        /*let storyBoard : UIStoryboard = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
                        let toVC = storyBoard.instantiateViewController( withIdentifier: "VoiceChatViewController") as! VoiceChatViewController
                        var roomId = "voice_default"
                        if let userid = thisuser!.id , let friendid = friendID{
                            if userid > friendid{
                                roomId = userid + "_" + friendid
                            }else{
                                roomId = friendid + "_" + userid
                            }
                        }
                        toVC.roomID = "voice_" + roomId
                        print("this is room id for voice chattingroom ===>", toVC.roomID)
                        toVC.str_profile = self.str_iamge_url
                        //self.present(toVC, animated: false, completion: nil)
                        let navViewController = UINavigationController(rootViewController: toVC)
                        navViewController.modalPresentationStyle = .fullScreen
                        self.present(navViewController, animated: false, completion: nil)*/
                    }else{
                        self.showAlerMessage(message: msg ?? "")
                    }
                }
            }
        }
    }
    @IBAction func endBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func topBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
