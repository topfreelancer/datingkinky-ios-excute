//
//  SettingsVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

class SettingsVC: BaseVC {
    
    @IBOutlet weak var cons_bottom_help: NSLayoutConstraint!
    @IBOutlet weak var uiv_dlg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBarImage()
        self.addBackButtonNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    @IBAction func uploadBtnClicked(_ sender: Any) {
        let storyboad = UIStoryboard(name: StoryBoards.SETTINGS, bundle: nil)
        let messageNav = storyboad.instantiateViewController(withIdentifier: "AvatarNav")
        UIApplication.shared.keyWindow?.rootViewController = messageNav
    }
    
    @IBAction func prpfileBtnClicked(_ sender: Any) {
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.SETTINGS, controllerName: "YourProfileVC")
        self.navigationController?.pushViewController(tovc, animated: true)
    }
    
    @IBAction func acccountBtnClicked(_ sender: Any) {
        let tovc = self.createViewControllerwithStoryBoardName(StoryBoards.SETTINGS, controllerName: "AccouontInfoVC")
        self.navigationController?.pushViewController(tovc, animated: true)
    }
    
    @IBAction func preferenceBtnClicked(_ sender: Any) {
        let storyboad = UIStoryboard(name: StoryBoards.SETTINGS, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: "PreferenceVC")
        self.navigationController?.pushViewController(targetVC, animated: true)
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        showDeleteDlg()
    }
    
    func showDeleteDlg()  {
        let configuration = NBBottomSheetConfiguration(animationDuration: 0.4, sheetSize: .fixed(Constants.SCREEN_HEIGHT))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        let delteDlg = (UIStoryboard(name: StoryBoards.SETTINGS, bundle: nil).instantiateViewController(withIdentifier: "DeleteVC") as! DeleteVC)
        bottomSheetController.present(delteDlg, on: self)
    }
    
    @IBAction func helpBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBar(Constants.CONTACTUS_LINK)
    }
}
