//
//  PreferenceVC.swift
//  DatingKinky
//
//  Created by top Dev on 19.11.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import YHPlaceHolderView
import SwiftyJSON

class PreferenceVC: BaseVC {
    
    @IBOutlet weak var col_peopleShow: UICollectionView!
    var ds_peoples = [BlockedUserModel]()
    @IBOutlet weak var uiv_random_chat: UIView!
    @IBOutlet weak var uiv_video_chat: UIView!
    @IBOutlet weak var uiv_audio_chat: UIView!
    @IBOutlet weak var uiv_video_verify: UIView!
    @IBOutlet weak var cons_top_uiv_verify: NSLayoutConstraint!
    @IBOutlet weak var cons_height: NSLayoutConstraint!
    
    @IBOutlet weak var swt_random_chat: UISwitch!
    @IBOutlet weak var swt_blockvideo_chat: UISwitch!
    @IBOutlet weak var swt_blockaudio_chat: UISwitch!
    @IBOutlet weak var swt_blockvideo_verify: UISwitch!
    @IBOutlet weak var swt_blockchat_request: UISwitch!
    @IBOutlet weak var swt_snooze_account: UISwitch!
    
    var ds_allChats1 = [AllChatModel]()
    var ds_allChats = [AllChatModel]()
    var userlistHandle: UInt?
    
    var isblock_video_verify: String = "0"
    var isblock_chat_request: String = "0"
    var snooze_account: String = "0"
    var isblock_random_chat: String = "0"
    var isblock_video_chat: String = "0"
    var isblock_audio_chat: String = "0"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarShow()
        getDataSource()
        getBlockParameters()
        setUI(thisuser!.hasPlusMembership)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavBarImage()
        //self.addBackButtonNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.cons_height.constant = self.col_peopleShow.frame.size.width / 2.05 * ceil(CGFloat(self.ds_peoples.count) / 2)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //NotificationCenter.default.removeObserver(self)
        if let id = thisuser?.id, let userlistHandle = userlistHandle {
            FirebaseAPI.removeUserlistObserver(userRoomid: "u" + id, userlistHandle)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getBlockParameters()  {
        self.isblock_video_verify = block_parameters!.isblock_video_verify ?? "0"
        self.isblock_chat_request = block_parameters!.isblock_chat_request  ?? "0"
        self.snooze_account = block_parameters!.snooze_account ?? "0"
        self.isblock_random_chat = block_parameters!.isblock_random_chat ?? "0"
        self.isblock_video_chat = block_parameters!.isblock_video_chat ?? "0"
        self.isblock_audio_chat = block_parameters!.isblock_audio_chat ?? "0"
        self.setBlockSwitch()
    }
    
    func setBlockSwitch() {
        self.swt_blockvideo_verify.isOn = self.isblock_video_verify == "1" ? true : false
        self.swt_blockchat_request.isOn = self.isblock_chat_request == "1" ? true : false
        self.swt_snooze_account.isOn = self.snooze_account == "1" ? true : false
        self.swt_random_chat.isOn = self.isblock_random_chat == "1" ? true : false
        self.swt_blockvideo_chat.isOn = self.isblock_video_chat == "1" ? true : false
        self.swt_blockaudio_chat.isOn = self.isblock_audio_chat == "1" ? true : false
    }
    
    func setUI(_ is_show: Bool) {
        if is_show{
            uiv_random_chat.isHidden = false
            uiv_video_chat.isHidden = false
            uiv_audio_chat.isHidden = false
            if #available(iOS 11.0, *) {
                uiv_video_verify.backgroundColor = UIColor.init(named: "color_plus_back")
            } else {
                uiv_video_verify.backgroundColor = UIColor.blue
            }
            cons_top_uiv_verify.priority = .defaultLow
        }else{
            uiv_random_chat.isHidden = true
            uiv_video_chat.isHidden = true
            uiv_audio_chat.isHidden = true
            uiv_video_verify.backgroundColor = .clear
            cons_top_uiv_verify.constant = 0
        }
    }
    
    func getDataSource()  {
        let chatRequest = "u" + "\(thisuser!.id ?? "")"
        userlistListner(chatRequest)
    }
    
    func userlistListner(_ userRoomid : String)  {
       self.ds_allChats1.removeAll()
        //self.showLoadingView(vc: self)HUDHUD()
            userlistHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            if let userlist = userlist{
                guard userlist.id != nil else{
                    //self.hideLoadingView()
                    return
                }
            }

            if let userlist = userlist{
                self.ds_allChats1.append(userlist)
            }

            if self.ds_allChats1.count != 0{
                var ids = [String]()
                for one in self.ds_allChats1{
                    ids.append(one.id!)
                }
                //print(ids.removeDuplicates())
                var num = 0
                self.ds_allChats.removeAll()
                self.ds_peoples.removeAll()
                for  one in ids.removeDuplicates(){
                    num += 1
                    if let allchat = self.getDataFromID(one){
                        self.ds_allChats.append(allchat)
                    }

                    if num == ids.removeDuplicates().count{
                        //dump(self.msgDatasource1, name: "msgDatasource1====>")
                        //dump(self.msgDatasource, name: "msgDatasource====.")
                        var num = 0
                        for one in self.ds_allChats{
                            num += 1
                            if one.isBlock == "true"{
                                self.ds_peoples.append(BlockedUserModel(id: one.id!, userAvatar: one.userAvatar ?? ""))
                            }
                            
                            if num == self.ds_allChats.count{
                                self.col_peopleShow.reloadData()
                            }
                        }
                        if self.ds_allChats.count == 0{
                            self.showToast("There is no blocked user.")
                        }
                    }
                }
            }
        })
    }
    
    func getDataFromID(_ id: String) -> AllChatModel? {
        var returnModel : AllChatModel?
        for one in self.ds_allChats1{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel
    }
    
    @IBAction func unBlockBtnclicked(_ sender: Any) {
        let unBlockBtn = sender as! UIButton
        
        let ref = Database.database().reference()
        let userchattingRoom = "u" + thisuser!.id!
        if let partnerid = self.ds_peoples[unBlockBtn.tag].id{
            ref.child("list").child(userchattingRoom).child("u" + partnerid).child("isBlock").setValue("false")
            self.showLoadingView(vc: self)
            ApiManager.removeBlockUsers(blocks: partnerid) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    dump(blockusers,name: "thisisblockusers")
                    DispatchQueue.main.async {
                        self.ds_peoples.remove(at: unBlockBtn.tag)
                        self.cons_height.constant =             self.col_peopleShow.frame.size.width / 2.05 * ceil(CGFloat(self.ds_peoples.count) / 2)
                        self.col_peopleShow.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func clickRandChatSwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.isblock_random_chat = switchh.isOn == true ? "1" : "0"
        print(self.isblock_random_chat)
        self.setRandomSwitches()
    }
    
    @IBAction func clickBlockVideoChatSwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.isblock_video_chat = switchh.isOn == true ? "1" : "0"
        print(self.isblock_video_chat)
        self.setRandomSwitches()
    }
    
    @IBAction func clickBlockAudioSwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.isblock_audio_chat = switchh.isOn == true ? "1" : "0"
        print(self.isblock_audio_chat)
        self.setRandomSwitches()
    }
    
    @IBAction func clickVideoVerifySwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.isblock_video_verify = switchh.isOn == true ? "1" : "0"
        print(self.isblock_video_verify)
        self.setRandomSwitches()
    }
    
    @IBAction func clickBlockChatRequestSwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.isblock_chat_request = switchh.isOn == true ? "1" : "0"
        print(self.isblock_chat_request)
        self.setRandomSwitches()
    }
    
    @IBAction func clickSnoozeSwitch(_ sender: Any) {
        let switchh = sender as! UISwitch
        self.snooze_account = switchh.isOn == true ? "1" : "0"
        print(self.snooze_account)
        self.setRandomSwitches()
    }
    
    func setRandomSwitches() {
        self.showLoadingView(vc: self)
        ApiManager.setBlockParameters(isblock_video_verify: self.isblock_video_verify, isblock_chat_request: self.isblock_chat_request, snooze_account: self.snooze_account, isblock_random_chat: self.isblock_random_chat, isblock_video_chat: self.isblock_video_chat, isblock_audio_chat: self.isblock_audio_chat) { (isSuccess, data) in
            self.hideLoadingView()
        }
    }
}

extension PreferenceVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.ds_peoples.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlockedCell", for: indexPath) as! BlockedCell
        if indexPath.row <= self.ds_peoples.count - 1{
            cell.entity = self.ds_peoples[indexPath.row]
        }
        cell.unBlockBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension PreferenceVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width / 2.05
        let h = w
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.SCREEN_WIDTH - 2 * collectionView.frame.size.width / 2.05 - 20
    }
}
