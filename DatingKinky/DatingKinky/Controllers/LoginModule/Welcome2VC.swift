//
//  Welcome2VC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import Kingfisher

class Welcome2VC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        self.gotoVC(VCs.LOGINNAV)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.gotoNavPresent(VCs.WELCOME_3, fullscreen: true)
    }
}


