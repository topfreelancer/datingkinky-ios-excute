//
//  MoreLinkRoleVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/27/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
//  for lookingforVC
class MoreLinkRoleVC: BaseVC {
    
    @IBOutlet weak var col_kinkMultiOptions: UICollectionView!
    @IBOutlet weak var uiv_dialogView: UIView!
    var ds_mulitCheck = [MultiCheckModel]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.ds_mulitCheck.removeAll()
        var num = -1
        
        for one in DS_kinkRoles{
            num += 1
            self.ds_mulitCheck.append(MultiCheckModel(one.valueName!, checkId: one.valueID!, checked: false))
            if num == DS_kinkRoles.count{
                lookingforRole = self.ds_mulitCheck
                self.col_kinkMultiOptions.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        uiv_dialogView.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @IBAction func onSpaceBtnClicked(_ sender: Any) {
        
        self.closeMenu(.moreLinkRole)
    }
    
    @IBAction func topbtnClicked(_ sender: Any) {
        
        self.closeMenu(.moreLinkRole)
    }
}

extension MoreLinkRoleVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ds_mulitCheck.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultiCheckCell", for: indexPath) as! MultiCheckCell
        cell.entity = ds_mulitCheck[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultiCheckCell", for: indexPath) as! MultiCheckCell
        cell.entity = ds_mulitCheck[indexPath.row]
        if cell.cus_checkBox.isOn{
            ds_mulitCheck[indexPath.row].checked = false
        }else{
            ds_mulitCheck[indexPath.row].checked = true
        }
        var indexPaths = [IndexPath]()
        indexPaths.removeAll()
        indexPaths.append(indexPath)
        collectionView.reloadItems(at: indexPaths)
        lookingforRole.removeAll()
        lookingforRole = self.ds_mulitCheck
    }
}

extension MoreLinkRoleVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (collectionView.frame.size.width - 20) / 2.0
        let h: CGFloat = 30
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
