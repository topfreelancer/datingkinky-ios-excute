//
//  ForgotVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

var resetEmail = ""
class ForgotVC: BaseVC {

    @IBOutlet weak var edt_email: UITextField!
    var str_emamil = "''"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEdtPlaceholder(edt_email, placeholderText: "", placeColor: .lightGray, padding: .left(8))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarShow()
    }
    
    @IBAction func resetBtnClicked(_ sender: Any) {
        self.str_emamil = self.edt_email.text!
        if self.str_emamil == ""{
            self.showAlerMessage(message: Messages.EMAIL_REQUIRE)
            return
        }else{
            self.showLoadingView(vc: self)
            ApiManager.resetPassword4Token(email: str_emamil) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    resetEmail = self.str_emamil
                    self.showResetDialog()
                }else{
                    let json = JSON(data as Any)
                    let response = json["error"].object
                    let error = JSON(response as Any)
                    let msg = error["msg"].stringValue
                    self.showAlerMessage(message: msg)
                }
            }
        }
    }
    
    func showResetDialog(){
        let alertController = UIAlertController(title: "Reset Password", message:"We have sent reset confirm request via your email. You can change password via mailbox or else change here by using token from mailbox.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            self.gotoNavPresent(VCs.RESET_PWD, fullscreen: true)
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
