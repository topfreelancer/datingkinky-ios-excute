//
//  RegisterLookingFor.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AORangeSlider
import SwiftyJSON

class RegisterLookingFor: BaseVC {
    
    @IBOutlet weak var indicatorSlider: AORangeSlider!
    @IBOutlet weak var edt_lookingfor: UITextField!
    
    @IBOutlet weak var cus_lookingfor: MSDropDown!
    @IBOutlet weak var cus_gender: MSDropDown!
    @IBOutlet weak var cus_distance: MSDropDown!
    //@IBOutlet weak var cus_kinkroles: MSDropDown!
    var tellusChecked = [MultiCheckModel]()
    var lookingforChecked = [MultiCheckModel]()
    var lookingfor_Options = [KeyValueModel]()
    var gender_Options = [KeyValueModel]()
    
    var str_lookingforHere = ""
    var str_lookingforGender = ""
    var str_lookingforDistance = ""
    
    let distance_Options : [KeyValueModel] = [KeyValueModel(key: "1", value: "0-10 miles"),
    KeyValueModel(key: "2", value: "0-25 miles"),
    KeyValueModel(key: "3", value: "0-50 miles"),
    KeyValueModel(key: "4", value: "0-100 miles"),
    KeyValueModel(key: "5", value: "0-250 miles"),
    KeyValueModel(key: "6", value: "0-500 miles"),
    KeyValueModel(key: "7", value: "Worldwide")]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupIndicatorSlider()
        NotificationCenter.default.addObserver(self, selector: #selector(getMoreLinkRole),name: NSNotification.Name(rawValue: "getMoreLinkRole"), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.setDropDownDataSource()
        animVC = .moreLinkRole
        creatNav(.moreLinkRole)
    }
    
    @objc func getMoreLinkRole(){
       // set action
       
    }
    
    func setDropDownDataSource()  {
        self.gender_Options.removeAll()
        self.lookingfor_Options.removeAll()
        
        var genderNum = 0
        var lookingForNum = 0
        
        for one in DS_genders{
            genderNum += 1
            self.gender_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
            if genderNum == DS_genders.count{
            
                for one in DS_hereFor{
                    lookingForNum += 1
                    self.lookingfor_Options.append(KeyValueModel(key: one.valueID!, value: one.valueName!))
                    if lookingForNum == DS_hereFor.count{
                        self.setDelegate4DropDowns()
                    }
                }
            }
            
        }
    }
     
    func setDelegate4DropDowns(){
        
        self.cus_lookingfor.keyvalueCount = self.lookingfor_Options.count
        self.cus_lookingfor.delegate = self
        self.cus_lookingfor.keyValues = self.lookingfor_Options
        self.cus_lookingfor.isMultiSelect = true
        self.cus_lookingfor.tag = dropDownTag.lookingfor.rawValue
        
        self.cus_gender.keyvalueCount = self.gender_Options.count
        self.cus_gender.delegate = self
        self.cus_gender.keyValues = self.gender_Options
        self.cus_gender.isMultiSelect = true
        self.cus_gender.tag = dropDownTag.lookingGender.rawValue
        
        self.cus_distance.keyvalueCount = self.distance_Options.count
        self.cus_distance.delegate = self
        self.cus_distance.keyValues = self.distance_Options
        self.cus_distance.isMultiSelect = false
        self.cus_distance.tag = dropDownTag.lookingDistance.rawValue
    }
        
    func setupIndicatorSlider() {

        let contentView = indicatorSlider.superview!

        indicatorSlider.minimumValue = 18
        indicatorSlider.maximumValue = 100
        indicatorSlider.lowValue = 30
        indicatorSlider.highValue = 50
        indicatorSlider.minimumDistance = 10

        let lowLabel = UILabel()
        contentView.addSubview(lowLabel)
        lowLabel.textAlignment = .center
        lowLabel.frame = CGRect(x:0, y:0, width: 60, height: 20)
        lowLabel.font = .systemFont(ofSize: 15)

        let highLabel = UILabel()
        contentView.addSubview(highLabel)
        highLabel.textAlignment = .center
        highLabel.frame = CGRect(x: 0, y: 0, width: 60, height: 20)
        highLabel.font = .systemFont(ofSize: 15)

        indicatorSlider.valuesChangedHandler = { [weak self] in
            guard let `self` = self else {
                return
            }
            let lowCenterInSlider = CGPoint(x:self.indicatorSlider.lowCenter.x, y: self.indicatorSlider.lowCenter.y)
            let highCenterInSlider = CGPoint(x:self.indicatorSlider.highCenter.x, y: self.indicatorSlider.highCenter.y)
            let lowCenterInView = self.indicatorSlider.convert(lowCenterInSlider, to: contentView)
            let highCenterInView = self.indicatorSlider.convert(highCenterInSlider, to: contentView)

            lowLabel.center = lowCenterInView
            highLabel.center = highCenterInView
            lowLabel.text = String(format: "%.0f", self.indicatorSlider.lowValue)
            highLabel.text = String(format: "%.0f", self.indicatorSlider.highValue)
        }
    }
    
    @IBAction func kinkRoleBtnClicked(_ sender: Any) {
        self.openMenu(.moreLinkRole)
    }
    
    @IBAction func gotoChatVC(_ sender: Any) {
        /*self.showLoadingView(vc: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.hideLoadingView()
            //self.gotoVC(VCs.HOME_TAB_BAR)
            self.gotoVC(VCs.LOGINNAV)
        }*/
        self.tellusChecked.removeAll()
        self.lookingforChecked.removeAll()
        
        self.tellusChecked = tellUsRole
   
        self.lookingforChecked = lookingforRole
        
        
        tellUsRole.removeAll()
        lookingforRole.removeAll()
        
        var tellusOptions = [String]()
        for one in self.tellusChecked{
            if one.checked{
                tellusOptions.append(one.str_checkId)
            }
        }
        
        
        var lookingforOptions = [String]()
        for one in self.lookingforChecked{
            if one.checked{
                lookingforOptions.append(one.str_checkId)
            }
        }
        
        if self.str_lookingforHere == ""{
            self.showAlerMessage(message: Messages.SELECT_LOOKINGFOR_HERE)
            return
        }
        if self.str_lookingforGender == ""{
            self.showAlerMessage(message: Messages.SELECT_LOOKINGFOR_GENDER)
            return
        }
        
        if self.str_lookingforDistance == ""{
            self.showAlerMessage(message: Messages.SELECT_LOOKINGFOR_DISTANCE)
            return
        }else{
            print(str_yourGender, str_primaryKinkRole, str_yourPronouns, str_yourSexualOrientations, str_yourRelationShipStyle, str_yourRelationShipStatus, str_aboutYou, str_aboutYourRelationShipStatus)
            print("****************************************************************************************")
            print(self.str_lookingforHere, self.str_lookingforGender)
            print("****************************************************************************************")
            dump(tellusOptions, name: "telluscheck")
            dump(lookingforOptions, name: "lookingforRolecheck")
            
            let lookingforhere = self.str_lookingforHere.split(separator: "|")
            var lookingforArray = [String]()
            lookingforArray.removeAll()
            
            for one in lookingforhere{
                let stringone = String(one).trim()
                for two in DS_hereFor{
                    if stringone == two.valueName{
                        lookingforArray.append(two.valueID ?? "")
                    }
                }
            }
            
            // add upload action
            
            let coordinatemodel = CoordinateModel(lat: UserDefault.getString(key: PARAMS.USER_LAT,defaultValue: "0") ?? "0", lng: UserDefault.getString(key: PARAMS.USER_LNG,defaultValue: "0") ?? "0")
            
            let updateUsermodel = UpdateUserModel(aboutMe: str_aboutYou, aboutUs: str_aboutYourRelationShipStatus, aboutYou: str_aboutYou, firstMeeting: "", gender: str_yourGender, hereFor: lookingforArray, kinkRole: str_primaryKinkRole, location: UserDefault.getString(key: PARAMS.USER_LOCATION,defaultValue: "United States") ?? "United States", orientation: str_yourSexualOrientations, pronoun: str_yourPronouns, relationshipStatus: str_yourRelationShipStatus, relationshipStyle: str_yourRelationShipStyle, relationshipStyleDetails: "unknown", coordinates: coordinatemodel, appearance: nil, kinkStyle: nil, lifeStyle: nil)
            
            self.showLoadingView(vc: self)
            ApiManager.updateUserProfile(updateuserModel: updateUsermodel) { (isSuccess, data) in
                self.hideLoadingView()
                let json = JSON(data as Any)
                //let jsondata = json["data"].object
                if isSuccess{
                    ApiManager.getPhotoURL { (isSuccess, data) in
                        if isSuccess{
                            let dataJSON = JSON(data as Any)
                            let data = JSON(dataJSON["data"].object as Any)
                            let user = JSON(data["user"].object)
                            let currentAge = user["currentAge"].stringValue
                            let avatar = JSON(user["avatar"].object)
                            let file = JSON(avatar["file"].object)
                            let url = JSON(file["url"].object)
                            let str_avatar = url["smallPhotoURL"].stringValue
                            
                            let profile = JSON(user["profile"].object)
                            let gender = JSON(profile["gender"].object)["lowerCaseName"].stringValue
                            let pronoun = JSON(profile["pronoun"].object)["name"].stringValue
                            let location = profile["location"].stringValue
                            let kinkRole = JSON(profile["kinkRole"].object)["lowerCaseName"].stringValue
                            
                            thisuser?.userAvatar = str_avatar
                            thisuser?.currentAge = currentAge
                            thisuser?.gender = gender
                            thisuser?.pronoun = pronoun
                            thisuser?.location = location
                            thisuser?.kinkRole = kinkRole
                            thisuser!.saveUserInfo()
                            thisuser!.loadUserInfo()
                            /*if let id = thisuser?.id{
                                self.chatRequestValueChangedListner( "u" + id)
                            }*/
                            
                            self.gotoNextVC1()
                        }else{
                            self.gotoNextVC1()
                        }
                    }
                }else{
                    let response = json["error"].object
                    let error = JSON(response as Any)
                    let msg = error["msg"].stringValue
                    self.showAlerMessage(message: msg)
                }
            }
            
        }
    }
    @IBAction func skipBtnClicked(_ sender: Any) {
        ApiManager.getPhotoURL { (isSuccess, data) in
            if isSuccess{
                let dataJSON = JSON(data as Any)
                let data = JSON(dataJSON["data"].object as Any)
                let user = JSON(data["user"].object)
                let currentAge = user["currentAge"].stringValue
                let avatar = JSON(user["avatar"].object)
                let file = JSON(avatar["file"].object)
                let url = JSON(file["url"].object)
                let str_avatar = url["smallPhotoURL"].stringValue
                
                let profile = JSON(user["profile"].object)
                let gender = JSON(profile["gender"].object)["lowerCaseName"].stringValue
                let pronoun = JSON(profile["pronoun"].object)["name"].stringValue
                let location = profile["location"].stringValue
                let kinkRole = JSON(profile["kinkRole"].object)["lowerCaseName"].stringValue
                
                thisuser?.userAvatar = str_avatar
                thisuser?.currentAge = currentAge
                thisuser?.gender = gender
                thisuser?.pronoun = pronoun
                thisuser?.location = location
                thisuser?.kinkRole = kinkRole
                thisuser!.saveUserInfo()
                thisuser!.loadUserInfo()
                
                /*if let id = thisuser?.id{
                    self.chatRequestValueChangedListner( "u" + id)
                }*/
                
                self.gotoNextVC1()
            }else{
                self.gotoNextVC1()
            }
        }
    }
    
    func gotoNextVC1()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 1
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
}

extension RegisterLookingFor:MSDropDownDelegate{
    
    func dropdownSelected(tagId: Int, answer: String, value: String, isSelected: Bool) {
        //print("\(answer) value= \(value)")
        switch tagId {
        case dropDownTag.lookingDistance.rawValue:
            print("\(value)")
            self.str_lookingforDistance = value
            break
        case dropDownTag.lookingGender.rawValue:
            print("\(value)")
            str_lookingforGender = answer
            break
        case dropDownTag.lookingfor.rawValue:
            print("this is lookinfor====>this is answer=\(answer) this is value= \(value)")
            print("this is lookinfor value===>\(value)")
            str_lookingforHere = answer
            break
        default:
            print("default")
        }
    }
}




