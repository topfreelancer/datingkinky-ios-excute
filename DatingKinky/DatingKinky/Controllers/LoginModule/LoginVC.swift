//
//  LoginVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import GDCheckbox
import SwiftyJSON

class LoginVC: BaseVC, UITextFieldDelegate {

    @IBOutlet weak var edt_userName: UITextField!
    @IBOutlet weak var edt_pwd: UITextField!
    @IBOutlet weak var cus_faceCheck: GDCheckbox!
    var str_userName = ""
    var str_pwd = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // reset video verification flag
        UserDefault.setBool(key: PARAMS.FIRST_LOGIN, value: false)
        setEdtPlaceholder(edt_userName, placeholderText: "", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_pwd, placeholderText: "", placeColor: .lightGray, padding: .left(8))
        //self.navBarHidden()
        //self.loadLayout()
        self.edt_pwd.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarHidden()
    }
    
    func loadLayout() {
        /*self.edt_userName.text = "iphonetester2"
        self.edt_pwd.text = "123456"*/
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return pressed")
        //gotoVC(VCs.HOME_TAB_BAR)
        str_userName = self.edt_userName.text!
        str_pwd = self.edt_pwd.text!
        self.callLoginApi()
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        //gotoVC(VCs.HOME_TAB_BAR)
        str_userName = self.edt_userName.text!
        str_pwd = self.edt_pwd.text!
        
        if str_userName == ""{
            self.showAlerMessage(message: Messages.USERNAME_REQUIRE)
            return
        }
        if str_pwd == ""{
            self.showAlerMessage(message: Messages.PASSWORD_REQUIRE)
            return
        }
        /*if cus_faceCheck.isOn{
            
        }*/
        else{
            self.callLoginApi()
        }
    }
    
    func callLoginApi(){
        self.showLoadingView(vc: self)
        ApiManager.authenticate(username: self.str_userName, password: self.str_pwd) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                UserDefault.setBool(key: PARAMS.LOGOUT, value: false)
                //print("Login success data =========>",data as Any)
                
                thisuser?.clearUserInfo()
                
                UserDefault.setString(key: PARAMS.USERNAME, value: self.str_userName)
                UserDefault.setString(key: PARAMS.PASSWORD, value: self.str_pwd)
                
                let json = JSON(data as Any)
                
                let jsondata = json["data"].object
                thisuser = UserModel(JSON(jsondata as Any))
                thisuser?.id = JSON(JSON(jsondata as Any)["user"].object)["_id"].stringValue
                thisuser!.saveUserInfo()
                thisuser!.loadUserInfo()
                dump(thisuser, name: "this is userinformation=============>")
                
                // TODO: for the simulator testing
                ApiManager.uploadToken { (isSuccess, data) in
                    if !thisuser!.hasAvatar{
                        self.gotoNavPresent(VCs.UPLOAD_PHOTO, fullscreen: true)
                        return
                    }
                    if !thisuser!.hasfilledaboutMe{
                        self.gotoNavPresent(VCs.TELLS_US, fullscreen: true)
                        return
                    }
                    /*if !thisuser!.hasFilledMatchingData{
                        self.gotoNavPresent(VCs.RESISTER_LOOKING_FOR, fullscreen: true)
                        return
                    }*/else{
                        //self.gotoVC(VCs.HOME_TAB_BAR)
                        ApiManager.getPhotoURL { (isSuccess, data) in
                            if isSuccess{
                                let dataJSON = JSON(data as Any)
                                let data = JSON(dataJSON["data"].object as Any)
                                let user = JSON(data["user"].object)
                                let currentAge = user["currentAge"].stringValue
                                let avatar = JSON(user["avatar"].object)
                                let file = JSON(avatar["file"].object)
                                let url = JSON(file["url"].object)
                                let str_avatar = url["smallPhotoURL"].stringValue
                                
                                let profile = JSON(user["profile"].object)
                                let gender = JSON(profile["gender"].object)["lowerCaseName"].stringValue
                                let pronoun = JSON(profile["pronoun"].object)["name"].stringValue
                                let location = profile["location"].stringValue
                                let kinkRole = JSON(profile["kinkRole"].object)["lowerCaseName"].stringValue
                                
                                thisuser?.userAvatar = str_avatar
                                thisuser?.currentAge = currentAge
                                thisuser?.gender = gender
                                thisuser?.pronoun = pronoun
                                thisuser?.location = location
                                thisuser?.kinkRole = kinkRole
                                thisuser!.saveUserInfo()
                                thisuser!.loadUserInfo()
                                /*if let id = thisuser?.id{
                                    self.chatRequestValueChangedListner( "u" + id)
                                }*/
                                
                                self.gotoNextVC1()
                            }else{
                                self.gotoNextVC1()
                            }
                        }
                    }
                }
            }else{
                
                let json = JSON(data as Any)
                let status = json[PARAMS.STATUS_CODE].intValue
                
                if status == 403{
                    let loginAlert = UIAlertController(title: "Email Confirmation", message: "Please check your mail box.You can confirm mail in your mailbox,\n or else you can input token here directly from your mailbox.", preferredStyle: .alert)
                    loginAlert.view.tintColor = .systemBlue
                    
                    loginAlert.addTextField { emailField in
                        emailField.font = .systemFont(ofSize: 14.0)
                        emailField.isSecureTextEntry = false
                        emailField.placeholder = Messages.EMAIL_REQUIRE
                    }
                    
                    loginAlert.addTextField { tokenField in
                        tokenField.font = .systemFont(ofSize: 14.0)
                        tokenField.isSecureTextEntry = false
                        tokenField.placeholder = Messages.TOKEN_REQUIRE
                    }
                    
                    let loginAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (authAction) in
                            // Get user name value.
                            let emailTextField = loginAlert.textFields![0] as UITextField
                        
                            let tokenTextField = loginAlert.textFields![1] as UITextField
                           
                        if(!(emailTextField.text! == "")  && !(tokenTextField.text! == "")){
                            self.showLoadingView(vc: self)
                            ApiManager.confirmEmail(email: emailTextField.text!, token: tokenTextField.text!) { (isSuccess, true) in
                                self.hideLoadingView()
                                if isSuccess{
                                    self.callLoginApi()
                                }else{
                                    
                                    let json = JSON(data as Any)
                                    let status = json[PARAMS.STATUS_CODE].intValue
                                    if status == 208{
                                        //self.showToast(Messages.ALREADY_CONFIRMED)
                                        self.showAlerMessage(message: Messages.ALREADY_CONFIRMED)
                                    }else{
                                        let response = json["error"].object
                                        
                                        let error = JSON(response as Any)
                                        let msg = error["msg"].stringValue
                                        self.showAlerMessage(message: msg)
                                    }
                                }
                            }
                        }else{
                            self.showAlerMessage(message: "Please input email and token!")
                        }
                    }

                    let cancelAction = UIAlertAction(title: "CANCEL",
                        style: .destructive,
                        handler: { _ in
                        loginAlert.dismiss(animated: true)
                    })
                    
                    loginAlert.addAction(loginAction)
                    loginAlert.addAction(cancelAction)
                    loginAlert.preferredAction = loginAction
                    self.present(loginAlert, animated: true, completion: nil)
                }else{
                    let response = json["error"].object
                    let error = JSON(response as Any)
                    let msg = error["msg"].stringValue
                    self.showAlerMessage(message: msg)
                }
            }
        }
    }
    
    func gotoNextVC1()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 1
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    @IBAction func gotoForgot(_ sender: Any) {
        self.gotoNavPresent(VCs.FORGOT, fullscreen: true)
    }
}
