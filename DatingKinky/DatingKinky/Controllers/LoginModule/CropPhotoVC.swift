//
//  CropPhotoVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON


class CropPhotoVC: BaseVC {
    
    var isTab: Bool = false
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var imv_photo: UIImageView!
    
    @IBOutlet weak var cons_w_photo: NSLayoutConstraint!
    @IBOutlet weak var cons_h_photo: NSLayoutConstraint!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //getting action for cropped picture from cropVC
        NotificationCenter.default.addObserver(self, selector: #selector(photoSendCropPhotoVC),name: NSNotification.Name(rawValue: "photoSendCropPhotoVC"), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        uiv_dlg.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func topbtnClicked(_ sender: Any) {
        if isTab{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.closeMenu(.photoCrop)
        }
    }
    
    @IBAction func onSpaceBtnClicked(_ sender: Any) {
        if isTab{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.closeMenu(.photoCrop)
        }
    }
    
    @IBAction func useitBtnClicked(_ sender: Any) {
        if let filename = fileName, let url = fileURL, let id = fileId{
            print("this is the send side fileName and url", "\(filename) \n \(url)")
            if let cropimage = croppedImage {
                self.showLoadingView(vc: self)
                ApiManager.s3Upload(presignedURL: url, fileName: saveBinaryWithName(image: cropimage, name: filename)) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        print("Success")
                        //self.showLoadingView(vc: self)
                        ApiManager.saveAvatar(file: id) { (isSuccess, data) in
                            //self.hideLoadingView()
                            if isSuccess{
                                if self.isTab{
                                    self.dismiss(animated: true, completion: nil)
                                }else{
                                    self.gotoNavPresent(VCs.TELLS_US, fullscreen: true)
                                }
                            }else{
                                let json = JSON(data as Any)
                                let response = json["error"].object
                                
                                let error = JSON(response as Any)
                                let msg = error["msg"].stringValue
                                self.showAlerMessage(message: msg)
                            }
                        }
                    }else{
                        self.showAlerMessage(message: Messages.SERVERERROR)
                    }
                }
            }
        }
    }
    
    @IBAction func clearBtnClicked(_ sender: Any) {
        if isTab{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.closeMenu(.photoCrop)
        }
    }
    
    @objc func photoSendCropPhotoVC(){
        
        if let cropimage = croppedImage {
            
//            let ratio =  cropimage.size.width / cropimage.size.height
//            if ratio > 1{
//                let value  = cropimage.size.width / (Constants.SCREEN_WIDTH - 60)
//                self.cons_w_photo.constant = cropimage.size.width / value
//                self.cons_h_photo.constant = self.cons_w_photo.constant
//            }else if ratio == 1{
//                self.cons_w_photo.constant = Constants.SCREEN_WIDTH - 60
//                self.cons_h_photo.constant = self.cons_w_photo.constant
//            }else {
//                let value  = cropimage.size.height / (Constants.SCREEN_WIDTH - 60)
//                 self.cons_h_photo.constant = cropimage.size.height / value
//                 self.cons_w_photo.constant = self.cons_h_photo.constant
//            }
            self.imv_photo.image = cropimage
        }
    }
}




