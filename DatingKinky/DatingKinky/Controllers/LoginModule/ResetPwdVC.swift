//
//  ResetVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

class ResetPwdVC: BaseVC {

    @IBOutlet weak var edt_verificaiton: UITextField!
    @IBOutlet weak var edt_newPwd: UITextField!
    var str_email = ""
    var str_verification = ""
    var str_newPwd = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.str_email = resetEmail
        resetEmail = ""
        // Do any additional setup after loading the view.
        setEdtPlaceholder(edt_verificaiton, placeholderText: "", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_newPwd, placeholderText: "", placeColor: .lightGray, padding: .left(8))
    }
    
    @IBAction func resetBtnClicked(_ sender: Any) {
        self.str_verification = self.edt_verificaiton.text!
        self.str_newPwd = self.edt_newPwd.text!
        if self.str_verification == ""{
            self.showAlerMessage(message: Messages.TOKEN_REQUIRE)
            return
        }
        
        if self.str_newPwd == ""{
            self.showAlerMessage(message: Messages.PASSWORD_REQUIRE)
            return
        }else{
            self.showLoadingView(vc: self)
            ApiManager.resetPassword(email: self.str_email, token: self.str_verification, password: self.str_newPwd) { (isSuccess, data) in
                self.hideLoadingView()
                let json = JSON(data as Any)
                if isSuccess{
                    let msg = json["status"].stringValue
                    //self.showAlerMessage(message: msg)
                    self.showResetDialog(msg)
                }else{
                    let response = json["error"].object
                    let error = JSON(response as Any)
                    let msg = error["msg"].stringValue
                    self.showAlerMessage(message: msg)
                }
            }
        }
    }
    
    func showResetDialog(_ msg: String){
        let alertController = UIAlertController(title: "Reset Password", message:msg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
