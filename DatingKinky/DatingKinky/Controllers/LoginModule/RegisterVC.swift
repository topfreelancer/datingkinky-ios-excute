//
//  RegisterVC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/24/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import GDCheckbox
import SwiftyJSON
import GooglePlaces

var str_birthday = ""
var str_birthday_8601Format = ""

class RegisterVC: BaseVC {
    
    @IBOutlet weak var edt_userName: UITextField!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_birthday: UITextField!
    @IBOutlet weak var edt_location: UITextField!
    @IBOutlet weak var edt_password: UITextField!
    @IBOutlet weak var edt_confirmPwd: UITextField!
    
    @IBOutlet weak var cus_termsCheck: GDCheckbox!
    @IBOutlet weak var cus_certificaitonCheck: GDCheckbox!
    
    var str_userName = ""
    var str_email = ""
    var str_birthdate = ""
    var str_location = ""
    var str_password = ""
    var str_confirmpwd = ""
    var str_lang = ""
    var str_lat = ""
    
    var ds_location = [LocationModel]()
    var dropDownTop = VPAutoComplete()
    //var firstime = -1
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        /*edt_location.addTarget(self, action: #selector(RegisterVC.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        self.edt_location.delegate = self*/
         NotificationCenter.default.addObserver(self, selector: #selector(setBirthday),name: NSNotification.Name(rawValue: "sendBirthday"), object: nil)
        self.setEditTexts()
        //self.loadLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarShow()
        animVC = .datePicker
        creatNav(.datePicker)
    }
    
    func setEditTexts() {
        setEdtPlaceholder(edt_userName, placeholderText: "member name", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_email, placeholderText: "email", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_birthday, placeholderText: "birthdate", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_location, placeholderText: "location", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_password, placeholderText: "password", placeColor: .lightGray, padding: .left(8))
        setEdtPlaceholder(edt_confirmPwd, placeholderText: "confirm password", placeColor: .lightGray, padding: .left(8))
    }
    
    @objc func setBirthday(){
        self.edt_birthday.text = str_birthday
    }
    
    func loadLayout() {
        self.edt_userName.text = "iphonetester10"
        self.edt_email.text = "joshjohnston826@gmail.com"
        self.edt_birthday.text = "2020-10-10"
        self.edt_location.text = ""
        self.edt_password.text = "123456"
        self.edt_confirmPwd.text = "123456"
        
        self.cus_termsCheck.isOn = true
        self.cus_certificaitonCheck.isOn = true
    }
    
    func addDropDown(){
        // For Top textField
        dropDownTop.dataSource.removeAll()
        for one in ds_location{
            dropDownTop.dataSource.append(one.str_locationName)
        }
        dropDownTop.onTextField = edt_location
        dropDownTop.onView = self.view
        dropDownTop.show { (str, index) in
            self.dropDownTop.isShowView(is_show: true)
            self.edt_location.text = str
            self.str_lat = self.ds_location[index].str_locationLat
            self.str_lang = self.ds_location[index].str_locationLong
        }
        self.dropDownTop.tableView?.reloadData()
        dropDownTop.isShowView(is_show: true)
    }
    
    func getDiffYearFromDateString(_ strDate: String) -> Int? {
        var years: Int? = nil
        let format: String = "yyyy-MM-dd"
        let df = DateFormatter()
        df.locale = NSLocale.current
        df.dateFormat = format
        let date = df.date(from: strDate)
        if let date = date{
            years = Date().interval(ofComponent: .year, fromDate: date)
        }
        return years
    }
    
    @IBAction func registerBtnClicked(_ sender: Any) {
        
        str_userName = self.edt_userName.text!
        str_email = self.edt_email.text!
        str_birthdate = self.edt_birthday.text!
        str_location = self.edt_location.text!
        str_password = self.edt_password.text!
        str_confirmpwd = self.edt_confirmPwd.text!
        
       
        if str_userName == ""{
            self.showAlerMessage(message: Messages.USERNAME_REQUIRE)
            return
        }
        
        if str_userName.count < 5 || str_userName.count > 50{
            self.showAlerMessage(message: Messages.USERNAME_CONDITION)
            return
        }
        
        if str_email == ""{
            self.showAlerMessage(message: Messages.EMAIL_REQUIRE)
            return
        }
        
        if !str_email.isValidEmail(){
            self.showAlerMessage(message: Messages.EMAIL_VALIDATION)
            return
        }
        
        if str_birthdate == ""{
            self.showAlerMessage(message: Messages.BIRTHDAY_REQUIRE)
            return
        }
        
        if let diffyear = self.getDiffYearFromDateString(self.str_birthdate){
            if diffyear < 18{
                self.showAlerMessage(message: Messages.BIRTHDAY_VALIDATE)
                return
            }
        }
        
        if str_location == ""{
            self.showAlerMessage(message: Messages.LOCATION_REQUIRE)
            return
        }
        
        if str_password == ""{
            self.showAlerMessage(message: Messages.PASSWORD_REQUIRE)
            return
        }
        if str_confirmpwd == ""{
            self.showAlerMessage(message: Messages.CONFIRM_PWD_REQUIRE)
            return
        }
        if str_confirmpwd != str_password{
            self.showAlerMessage(message: Messages.CONFIRM_PWD_CHECK)
            return
        }
        
        if !self.cus_termsCheck.isOn{
            self.showAlerMessage(message: Messages.TERMS_CHECK)
            return
        }
        if !self.cus_certificaitonCheck.isOn{
            self.showAlerMessage(message: Messages.VERIFY_CHECK)
            return
        }else{
            UserDefault.setString(key: PARAMS.USER_LOCATION, value: self.str_location)
            UserDefault.setString(key: PARAMS.USER_LAT, value: self.str_lat)
            UserDefault.setString(key: PARAMS.USER_LNG, value: self.str_lang)
            let coordinateModel = CoordinateModel(lat: self.str_lat, lng: self.str_lang)
            print("8601FormmatString==>", str_birthday_8601Format)
            let registermodel = RegisterModel(username: str_userName, password: str_password, email: str_email, dateofBirth: str_birthday, location: str_location, coordinate: coordinateModel, accountType: "5b8c3662633635082db7ae29", role: "5eeba968feaa9256ed6eea3c", fosta: true)
            self.showLoadingView(vc: self)
            ApiManager.register(registerModel: registermodel) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    print("success=========>",data as Any)
                    /// i must change here the app's logic, must call confirm email api...
                    
                    let loginAlert = UIAlertController(title: "Email Confirmation", message: "Please check your mail box.You can confirm mail in your mailbox,\n or else you can input token here directly from your mailbox.", preferredStyle: .alert)
                    loginAlert.view.tintColor = .systemBlue
                    
                    loginAlert.addTextField { emailField in
                        emailField.font = .systemFont(ofSize: 14.0)
                        emailField.isSecureTextEntry = false
                        emailField.placeholder = Messages.EMAIL_REQUIRE
                        //currentPwd = usernameField.text ?? ""
                    }
                    
                    loginAlert.addTextField { tokenField in
                        tokenField.font = .systemFont(ofSize: 14.0)
                        tokenField.isSecureTextEntry = false
                        tokenField.placeholder = Messages.TOKEN_REQUIRE
                        //currentPwd = usernameField.text ?? ""
                    }
                    
                    let loginAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (authAction) in
                            // Get user name value.
                            let emailTextField = loginAlert.textFields![0] as UITextField
                        
                            let tokenTextField = loginAlert.textFields![1] as UITextField
                           
                        if(!(emailTextField.text! == "")  && !(tokenTextField.text! == "")){
                            self.showLoadingView(vc: self)
                            ApiManager.confirmEmail(email: emailTextField.text!, token: tokenTextField.text!) { (isSuccess, true) in
                                self.hideLoadingView()
                                if isSuccess{
                                    /// call login api
                                    self.showLoadingView(vc: self)
                                    ApiManager.authenticate(username: self.str_userName, password: self.str_password) { (isSuccess, data) in
                                        self.hideLoadingView()
                                        if isSuccess{
                                            UserDefault.setBool(key: PARAMS.LOGOUT, value: false)
                                            //print("Login success data =========>",data as Any)
                                            
                                            thisuser?.clearUserInfo()
                                            
                                            UserDefault.setString(key: PARAMS.USERNAME, value: self.str_userName)
                                            UserDefault.setString(key: PARAMS.PASSWORD, value: self.str_password)
                                            
                                            let json = JSON(data as Any)
                                            
                                            let jsondata = json["data"].object
                                            thisuser = UserModel(JSON(jsondata as Any))
                                            thisuser?.id = JSON(JSON(jsondata as Any)["user"].object)["_id"].stringValue
                                            thisuser!.saveUserInfo()
                                            thisuser!.loadUserInfo()
                                            
                                            // set joined date and other promotion content as false
                                            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                                            UserDefault.setString(key: PARAMS.JOINED_DATE, value: "\(timeNow)")
                                            UserDefault.setBool(key: PARAMS.FIRST_20MINS, value: false)
                                            UserDefault.setBool(key: PARAMS._2DAYS_BEFORE, value: false)
                                            UserDefault.setBool(key: PARAMS._1ADY_BEFORE, value: false)
                                            UserDefault.setBool(key: PARAMS.LAST_DAY, value: false)
                                            
                                            ApiManager.uploadToken { [self] (isSuccess, data) in
                                                // count 20 mins for first promotion show
                                                Timer.scheduledTimer(timeInterval: 120, target: self, selector: #selector(callFirstPromotion), userInfo: nil, repeats: false)
                                                
                                                if !thisuser!.hasAvatar{
                                                    self.gotoNavPresent(VCs.UPLOAD_PHOTO, fullscreen: true)
                                                    return
                                                }
                                                if !thisuser!.hasfilledaboutMe{
                                                    self.gotoNavPresent(VCs.TELLS_US, fullscreen: true)
                                                    return
                                                }else{
                                                    ApiManager.getPhotoURL { (isSuccess, data) in
                                                        if isSuccess{
                                                            let dataJSON = JSON(data as Any)
                                                            let data = JSON(dataJSON["data"].object as Any)
                                                            let user = JSON(data["user"].object)
                                                            let currentAge = user["currentAge"].stringValue
                                                            let avatar = JSON(user["avatar"].object)
                                                            let file = JSON(avatar["file"].object)
                                                            let url = JSON(file["url"].object)
                                                            let str_avatar = url["smallPhotoURL"].stringValue
                                                            
                                                            let profile = JSON(user["profile"].object)
                                                            let gender = JSON(profile["gender"].object)["lowerCaseName"].stringValue
                                                            let pronoun = JSON(profile["pronoun"].object)["name"].stringValue
                                                            let location = profile["location"].stringValue
                                                            let kinkRole = JSON(profile["kinkRole"].object)["lowerCaseName"].stringValue
                                                            
                                                            thisuser?.userAvatar = str_avatar
                                                            thisuser?.currentAge = currentAge
                                                            thisuser?.gender = gender
                                                            thisuser?.pronoun = pronoun
                                                            thisuser?.location = location
                                                            thisuser?.kinkRole = kinkRole
                                                            thisuser!.saveUserInfo()
                                                            thisuser!.loadUserInfo()
                                                            self.gotoNextVC1()
                                                        }else{
                                                            self.gotoNextVC1()
                                                        }
                                                    }
                                                }
                                            }
                                        }else{
                                            
                                            let json = JSON(data as Any)
                                            let status = json[PARAMS.STATUS_CODE].intValue
                                            
                                            if status == 403{
                                                let loginAlert = UIAlertController(title: "Email Confirmation", message: "Please check your mail box.You can confirm mail in your mailbox,\n or else you can input token here directly from your mailbox.", preferredStyle: .alert)
                                                loginAlert.view.tintColor = .systemBlue
                                                
                                                loginAlert.addTextField { emailField in
                                                    emailField.font = .systemFont(ofSize: 14.0)
                                                    emailField.isSecureTextEntry = false
                                                    emailField.placeholder = Messages.EMAIL_REQUIRE
                                                    //currentPwd = usernameField.text ?? ""
                                                }
                                                
                                                loginAlert.addTextField { tokenField in
                                                    tokenField.font = .systemFont(ofSize: 14.0)
                                                    tokenField.isSecureTextEntry = false
                                                    tokenField.placeholder = Messages.TOKEN_REQUIRE
                                                    //currentPwd = usernameField.text ?? ""
                                                }
                                                
                                                let loginAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (authAction) in
                                                        // Get user name value.
                                                        let emailTextField = loginAlert.textFields![0] as UITextField
                                                    
                                                        let tokenTextField = loginAlert.textFields![1] as UITextField
                                                       
                                                    if(!(emailTextField.text! == "")  && !(tokenTextField.text! == "")){
                                                        self.showLoadingView(vc: self)
                                                        ApiManager.confirmEmail(email: emailTextField.text!, token: tokenTextField.text!) { (isSuccess, true) in
                                                            self.hideLoadingView()
                                                            if isSuccess{
                                                                ///self.showToast(Messages.CONFIRMED)
                                                                /// call login api once again....
                                                                
                                                                
                                                            }else{
                                                                
                                                                let json = JSON(data as Any)
                                                                let status = json[PARAMS.STATUS_CODE].intValue
                                                                if status == 208{
                                                                    self.showToast(Messages.ALREADY_CONFIRMED)
                                                                }else{
                                                                    let response = json["error"].object
                                                                    
                                                                    let error = JSON(response as Any)
                                                                    let msg = error["msg"].stringValue
                                                                    self.showToast(msg)
                                                                }
                                                            }
                                                        }
                                                        }else{
                                                            print("current password incorrect")
                                                            self.showToast("")
                                                        }
                                                        
                                                        
                                                }

                                                let cancelAction = UIAlertAction(title: "CANCEL",
                                                     style: .destructive,
                                                     handler: { _ in
                                                        // self.handleUsernamePasswordCanceled(loginAlert: loginAlert)
                                                        loginAlert.dismiss(animated: true)
                                                                                    
                                                })
                                                loginAlert.addAction(loginAction)
                                                loginAlert.addAction(cancelAction)
                                                loginAlert.preferredAction = loginAction
                                                self.present(loginAlert, animated: true, completion: nil)
                                            }else{
                                                let response = json["error"].object
                                                let error = JSON(response as Any)
                                                let msg = error["msg"].stringValue
                                                self.showAlerMessage(message: msg)
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                    let json = JSON(data as Any)
                                    let status = json[PARAMS.STATUS_CODE].intValue
                                    if status == 208{
                                        self.showToast(Messages.ALREADY_CONFIRMED)
                                    }else{
                                        let response = json["error"].object
                                        
                                        let error = JSON(response as Any)
                                        let msg = error["msg"].stringValue
                                        self.showToast(msg)
                                    }
                                }
                            }
                            }else{
                                print("current password incorrect")
                                self.showToast("")
                            }
                            
                            
                    }

                    let cancelAction = UIAlertAction(title: "CANCEL",
                                                     style: .destructive,
                                                     handler: { _ in
                                                        // self.handleUsernamePasswordCanceled(loginAlert: loginAlert)
                                                        loginAlert.dismiss(animated: true)
                                                        
                    })
                    
                    loginAlert.addAction(loginAction)
                    loginAlert.addAction(cancelAction)
                    loginAlert.preferredAction = loginAction
                    self.present(loginAlert, animated: true, completion: nil)
                    
                }else{
                    print("fail=========>",data as Any)
                    let json = JSON(data as Any)
                    let response = json["error"].object
                    let error = JSON(response as Any)
                    let msg = error["msg"].stringValue
                    self.showAlerMessage(message: msg)
                }
            }
        }
    }
    
    @objc func callFirstPromotion() {
        print("callled with 20 mins")
        NotificationCenter.default.post(Notification(name: .show_20mins_promotion, object: nil))
    }

    func gotoNextVC1()  {
        // reset video verification flag
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 1
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    @IBAction func gotoCalendar(_ sender: Any) {
        //self.gotoVC(VCs.CALENDARVC)
        self.openMenu(.datePicker)
    }
  
    @IBAction func termsBtnClicked(_ sender: Any) {
        self.gotoWebViewWithProgressBar(Constants.TERMSOF_USE_LINK)
    }
    
    @IBAction func locationBtnClicked(_ sender: Any) {
        edt_location.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
}

/*extension RegisterVC: UITextFieldDelegate{
    
    @objc func textFieldDidChange(textField: UITextField){
        print("object function chanaged")
        
        ApiManager.getLocations(positionName: textField.text ?? ""){
            (isSuccess,data) in
            if isSuccess{
                self.ds_location.removeAll()
                let data = JSON(data as Any)
                print(data)
                let features = data["features"].arrayValue
                for one in features{
                    self.ds_location.append(LocationModel(one))
                }
                DispatchQueue.main.async {
                    self.addDropDown()
                }
            }
        }
    }
}*/

extension RegisterVC: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    edt_location.text = place.name
    self.str_lang = "\(place.coordinate.longitude)"
    self.str_lat = "\(place.coordinate.latitude)"
    // Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
