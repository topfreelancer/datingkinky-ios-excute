//
//  SplashVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
import _SwiftUIKitOverlayShims

var networkStatus = 0
class SplashVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // reset video verification flag
        self.navBarHidden()
        self.resetDataSources()
        
        if networkStatus == 0{
            self.getOptions()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0, execute: {
                if networkStatus == 0{
                    self.hideLoadingView()
                    //self.showAlerMessagewithCompletion(message: Messages.NETWORK_ISSUE)
                    if UserDefault.getBool(key: PARAMS.FIRST_LOGIN, defaultValue: true){
                        self.gotoVC(VCs.WELCOMENAV)
                        UserDefault.setBool(key: PARAMS.FIRST_LOGIN, value: false)
                    }else{
                        self.gotoVC(VCs.LOGINNAV)
                    }
                }
                else{
                    return
                }
            }
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarHidden()
        // set video chatting flag as false
        UserDefault.setBool(key: PARAMS.IS_VIDEO_CHATTING, value: false)
        UserDefault.setBool(key: PARAMS.IS_VOICE_CHATTING, value: false)
        UserDefault.Sync()
    }
    
    func resetDataSources() {
        
        DS_bodyTypes.removeAll()
        DS_drink.removeAll()
        DS_eatingHabits.removeAll()
        DS_ethnicities.removeAll()
        DS_exercisesFrequencies.removeAll()
        DS_eyeColours.removeAll()
        DS_genders.removeAll()
        DS_hairColours.removeAll()
        DS_heights.removeAll()
        DS_hereFor.removeAll()
        DS_indentifiesAs.removeAll()
        DS_kinkactivities.removeAll()
        DS_kinkRoles.removeAll()
        DS_knikSperes.removeAll()
        DS_orientations.removeAll()
        DS_pronouns.removeAll()
        DS_relationShipStatues.removeAll()
        DS_relationShipStyles.removeAll()
        DS_religions.removeAll()
        DS_safeSex.removeAll()
        DS_sexAndKinks.removeAll()
        DS_smoke.removeAll()
    }
    
    func getOptions() {
        self.showLoadingView(vc: self)
        ApiManager.getOptions { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                //print("this is options=====>",data as Any)
                let totalData = JSON(data as Any)
                
                let bodyTypes = totalData["bodyTypes"].object
                let drink = totalData["drink"].object
                let eatingHabits = totalData["eatingHabits"].object
                let ethnicities = totalData["ethnicities"].object
                let exerciseFrequencies = totalData["exerciseFrequencies"].object
                let eyeColours = totalData["eyeColours"].object
                let genders = totalData["genders"].object
                let hairColours = totalData["hairColours"].object
                let heights = totalData["heights"].object
                let hereFor = totalData["hereFor"].object
                let indentifiesAs = totalData["identifiesAs"].object
                let kinkactivities = totalData["kinkActivities"].object
                let kinkRoles = totalData["kinkRoles"].object
                let kinkSpheres = totalData["kinkSpheres"].object
                let orientations = totalData["orientations"].object
                let pronouns = totalData["pronouns"].object
                let relationShipStatuses = totalData["relationshipStatuses"].object
                let relationShipStyles = totalData["relationshipStyles"].object
                let religions = totalData["religions"].object
                let safeSex = totalData["safeSex"].object
                let sexAndKinks = totalData["sexAndKinks"].object
                let smoke = totalData["smoke"].object
                
                let bodyTypes_json = JSON(bodyTypes as Any)
                let drink_json = JSON(drink as Any)
                let eatingHabits_json = JSON(eatingHabits as Any)
                let ethnicities_json = JSON(ethnicities as Any)
                let exerciseFrequencies_json = JSON(exerciseFrequencies as Any)
                let eyeColours_json = JSON(eyeColours as Any)
                let genders_json = JSON(genders as Any)
                let hairColours_json = JSON(hairColours as Any)
                let heights_json = JSON(heights as Any)
                let hereFor_json = JSON(hereFor as Any)
                let indetifiesAs_json = JSON(indentifiesAs as Any)
                let kinkactivities_json = JSON(kinkactivities as Any)
                let kinkRoles_json = JSON(kinkRoles as Any)
                let kinkSpheres_json = JSON(kinkSpheres as Any)
                let orientations_json = JSON(orientations as Any)
                let pronouns_json = JSON(pronouns as Any)
                let relationShipStatuses_json = JSON(relationShipStatuses as Any)
                let relationShipStyles_json = JSON(relationShipStyles as Any)
                let religions_json = JSON(religions as Any)
                let safeSex_json = JSON(safeSex as Any)
                let sexAndKinks_json = JSON(sexAndKinks as Any)
                let smoke_json = JSON(smoke as Any)
                
                let bodyTypes_arr = bodyTypes_json["values"].arrayObject
                let drink_arr = drink_json["values"].arrayObject
                let eatingHabits_arr = eatingHabits_json["values"].arrayObject
                let ethnicities_arr = ethnicities_json["values"].arrayObject
                let exercisesFreequencies_arr = exerciseFrequencies_json["values"].arrayObject
                let eyeColours_arr = eyeColours_json["values"].arrayObject
                let genders_arr = genders_json["values"].arrayObject
                let hair_arr = hairColours_json["values"].arrayObject
                let heights_arr = heights_json["values"].arrayObject
                let hereFor_arr = hereFor_json["values"].arrayObject
                let indentifies_arr = indetifiesAs_json["values"].arrayObject
                let kinkactivities_arr = kinkactivities_json["values"].arrayObject
                let kinkRoles_arr = kinkRoles_json["values"].arrayObject
                let kinkSpheres_arr = kinkSpheres_json["values"].arrayObject
                let orientations_arr = orientations_json["values"].arrayObject
                let pronouns_arr = pronouns_json["values"].arrayObject
                let relationShipStatues_arr = relationShipStatuses_json["values"].arrayObject
                let relationShipStyles_arr = relationShipStyles_json["values"].arrayObject
                let religions_arr = religions_json["values"].arrayObject
                let safeSex_arr = safeSex_json["values"].arrayObject
                let sexandKinks_arr = sexAndKinks_json["values"].arrayObject
                let smoke_arr = smoke_json["values"].arrayObject
                
                var body_num = 0
                var drink_num = 0
                var eatingHabits_num = 0
                var ethnicities_num = 0
                var excercises_num = 0
                var eyeColors_num = 0
                var genders_num = 0
                var hairColors_num = 0
                var heights_num = 0
                var hereFor_num = 0
                var identifies_num = 0
                var kinkactivities_num = 0
                var kinkRoles_num = 0
                var kinkSpheres_num = 0
                var orientations_num = 0
                var pronouns_num = 0
                var relationShipStatues_num = 0
                var relationShipStyles_num = 0
                var religions_num = 0
                var safeSex_num = 0
                var sexAndKinks_num = 0
                var smoke_num = 0
                
                if let bodyTypesArr = bodyTypes_arr{
                    for one in bodyTypesArr{
                        body_num += 1
                        let jsonOne = JSON(one as Any)
                        DS_bodyTypes.append(OptionsModel(jsonOne))
                        if body_num == bodyTypesArr.count{
                            if let drinkArr = drink_arr{
                                for one in drinkArr{
                                    drink_num += 1
                                    let jsonOne = JSON(one as Any)
                                    DS_drink.append(OptionsModel(jsonOne))
                                    if drink_num == drinkArr.count{
                                        if let eatingHabitsArr = eatingHabits_arr{
                                            for one in eatingHabitsArr{
                                                eatingHabits_num += 1
                                                let jsonOne = JSON(one as Any)
                                                DS_eatingHabits.append(OptionsModel(jsonOne))
                                                if eatingHabits_num == eatingHabitsArr.count{
                                                    if let ethnicitiesArr = ethnicities_arr{
                                                        for one in ethnicitiesArr{
                                                            ethnicities_num += 1
                                                            let jsonOne = JSON(one as Any)
                                                            DS_ethnicities.append(OptionsModel(jsonOne))
                                                            if ethnicities_num == ethnicitiesArr.count{
                                                                if let exercisesFreequenciesArr = exercisesFreequencies_arr{
                                                                    for one in exercisesFreequenciesArr{
                                                                        excercises_num += 1
                                                                        let jsonOne = JSON(one as Any)
                                                                        DS_exercisesFrequencies.append(OptionsModel(jsonOne))
                                                                        if excercises_num == exercisesFreequenciesArr.count{
                                                                            if let eyeColoursArr = eyeColours_arr{
                                                                                for one in eyeColoursArr{
                                                                                    eyeColors_num += 1
                                                                                    let jsonOne = JSON(one as Any)
                                                                                    DS_eyeColours.append(OptionsModel(jsonOne))
                                                                                    if eyeColors_num == eyeColoursArr.count{
                                                                                        if let gendersArr = genders_arr{
                                                                                            for one in gendersArr{
                                                                                                genders_num += 1
                                                                                                let jsonOne = JSON(one as Any)
                                                                                                DS_genders.append(OptionsModel(jsonOne))
                                                                                                if genders_num == gendersArr.count{
                                                                                                    if let hairArr = hair_arr{
                                                                                                        for one in hairArr{
                                                                                                            hairColors_num += 1
                                                                                                            let jsonOne = JSON(one as Any)
                                                                                                            DS_hairColours.append(OptionsModel(jsonOne))
                                                                                                            if hairColors_num == hairArr.count{
                                                                                                                if let heightsArr = heights_arr{
                                                                                                                    for one in heightsArr{
                                                                                                                        heights_num += 1
                                                                                                                        let jsonOne = JSON(one as Any)
                                                                                                                        DS_heights.append(OptionsModel(jsonOne))
                                                                                                                        if heights_num == heightsArr.count{
                                                                                                                            if let hereForArr = hereFor_arr{
                                                                                                                                for one in hereForArr{
                                                                                                                                    hereFor_num += 1
                                                                                                                                    let jsonOne = JSON(one as Any)
                                                                                                                                    DS_hereFor.append(OptionsModel(jsonOne))
                                                                                                                                    if hereFor_num == hereForArr.count{
                                                                                                                                        if let indentifiesArr = indentifies_arr{
                                                                                                                                            for one in indentifiesArr{
                                                                                                                                                identifies_num += 1
                                                                                                                                                let jsonOne = JSON(one as Any)
                                                                                                                                                DS_indentifiesAs.append(OptionsModel(jsonOne))
                                                                                                                                                if identifies_num == indentifiesArr.count{
                                                                                                                                                    if let kinkactivitiesArr = kinkactivities_arr{
                                                                                                                                                        for one in kinkactivitiesArr{
                                                                                                                                                            kinkactivities_num += 1
                                                                                                                                                            let jsonOne = JSON(one as Any)
                                                                                                                                                            DS_kinkactivities.append(OptionsModel(jsonOne))
                                                                                                                                                            if kinkactivities_num == kinkactivitiesArr.count{
                                                                                                                                                                if let kinkRolesArr = kinkRoles_arr{
                                                                                                                                                                    for one in kinkRolesArr{
                                                                                                                                                                        kinkRoles_num += 1
                                                                                                                                                                        let jsonOne = JSON(one as Any)
                                                                                                                                                                        DS_kinkRoles.append(OptionsModel(jsonOne))
                                                                                                                                                                        if kinkRoles_num == kinkRolesArr.count{
                                                                                                                                                                            if let kinkSpheresArr = kinkSpheres_arr{
                                                                                                                                                                                for one in kinkSpheresArr{
                                                                                                                                                                                    kinkSpheres_num += 1
                                                                                                                                                                                    let jsonOne = JSON(one as Any)
                                                                                                                                                                                    DS_knikSperes.append(OptionsModel(jsonOne))
                                                                                                                                                                                    if kinkSpheres_num == kinkSpheresArr.count{
                                                                                                                                                                                        if let orientationsArr = orientations_arr{
                                                                                                                                                                                            for one in orientationsArr{
                                                                                                                                                                                                orientations_num += 1
                                                                                                                                                                                                let jsonOne = JSON(one as Any)
                                                                                                                                                                                                DS_orientations.append(OptionsModel(jsonOne))
                                                                                                                                                                                                if orientations_num == orientationsArr.count{
                                                                                                                                                                                                    if let pronounsArr = pronouns_arr{
                                                                                                                                                                                                        for one in pronounsArr{
                                                                                                                                                                                                            pronouns_num += 1
                                                                                                                                                                                                            let jsonOne = JSON(one as Any)
                                                                                                                                                                                                            DS_pronouns.append(OptionsModel(jsonOne))
                                                                                                                                                                                                            if pronouns_num == pronounsArr.count{
                                                                                                                                                                                                                if let relationShipStatuesArr = relationShipStatues_arr{
                                                                                                                                                                                                                    for one in relationShipStatuesArr{
                                                                                                                                                                                                                        relationShipStatues_num += 1
                                                                                                                                                                                                                        let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                        DS_relationShipStatues.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                        if relationShipStatues_num == relationShipStatuesArr.count{
                                                                                                                                                                                                                            if let relationShipStylesArr = relationShipStyles_arr{
                                                                                                                                                                                                                                for one in relationShipStylesArr{
                                                                                                                                                                                                                                    relationShipStyles_num += 1
                                                                                                                                                                                                                                    let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                                    DS_relationShipStyles.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                                    if relationShipStyles_num == relationShipStylesArr.count{
                                                                                                                                                                                                                                        if let religionsArr = religions_arr{
                                                                                                                                                                                                                                            for one in religionsArr{
                                                                                                                                                                                                                                                religions_num += 1
                                                                                                                                                                                                                                                let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                                                DS_religions.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                                                if religions_num == religionsArr.count{
                                                                                                                                                                                                                                                    if let safeSexArr = safeSex_arr{
                                                                                                                                                                                                                                                        for one in safeSexArr{
                                                                                                                                                                                                                                                            safeSex_num += 1
                                                                                                                                                                                                                                                            let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                                                            DS_safeSex.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                                                            if safeSex_num == safeSexArr.count{
                                                                                                                                                                                                                                                                if let sexandKinksArr = sexandKinks_arr{
                                                                                                                                                                                                                                                                    for one in sexandKinksArr{
                                                                                                                                                                                                                                                                        sexAndKinks_num += 1
                                                                                                                                                                                                                                                                        let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                                                                        DS_sexAndKinks.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                                                                        if sexAndKinks_num == sexandKinksArr.count{
                                                                                                                                                                                                                                                                            if let smokeArr = smoke_arr{
                                                                                                                                                                                                                                                                                for one in smokeArr{
                                                                                                                                                                                                                                                                                    smoke_num += 1
                                                                                                                                                                                                                                                                                    let jsonOne = JSON(one as Any)
                                                                                                                                                                                                                                                                                    DS_smoke.append(OptionsModel(jsonOne))
                                                                                                                                                                                                                                                                                    if smoke_num == smokeArr.count{
                                                                                                                                                                                                                                                                                        self.checkBackgrouond()
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func gotoNextVC() {
        
        if !thisuser!.hasAvatar{
            self.gotoNavPresent(VCs.UPLOAD_PHOTO, fullscreen: true)
            return
        }
        
        if !thisuser!.hasfilledaboutMe{
            self.gotoNavPresent(VCs.TELLS_US, fullscreen: true)
            return
        }
        /*if !thisuser!.hasFilledMatchingData{
            self.gotoNavPresent(VCs.RESISTER_LOOKING_FOR, fullscreen: true)
            return
        }*/else{
            
            ApiManager.getPhotoURL { (isSuccess, data) in
                if isSuccess{
                    let dataJSON = JSON(data as Any)
                    let data = JSON(dataJSON["data"].object as Any)
                    let user = JSON(data["user"].object)
                    let currentAge = user["currentAge"].stringValue
                    let avatar = JSON(user["avatar"].object)
                    let file = JSON(avatar["file"].object)
                    let url = JSON(file["url"].object)
                    let str_avatar = url["smallPhotoURL"].stringValue
                    
                    let profile = JSON(user["profile"].object)
                    let gender = JSON(profile["gender"].object)["lowerCaseName"].stringValue
                    let pronoun = JSON(profile["pronoun"].object)["name"].stringValue
                    let location = profile["location"].stringValue
                    let kinkRole = JSON(profile["kinkRole"].object)["lowerCaseName"].stringValue
                    
                    thisuser?.userAvatar = str_avatar
                    thisuser?.currentAge = currentAge
                    thisuser?.gender = gender
                    thisuser?.pronoun = pronoun
                    thisuser?.location = location
                    thisuser?.kinkRole = kinkRole
                    thisuser!.saveUserInfo()
                    thisuser!.loadUserInfo()
                    
                    //MARK: set value change listener for the request node
                    
                    /*if let id = thisuser?.id{
                        self.chatRequestValueChangedListner( "u" + id)
                    }*/
                    
                    self.gotoNextVC1()
                }else{
                    self.gotoNextVC1()
                }
            }
        }
        //dump(thisuser, name: "thisuser information====>")
        //self.gotoVC(VCs.HOME_TAB_BAR)*/
    }
    
    func gotoNextVC1()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.HOME_TAB_BAR) as! UITabBarController
        toVC.selectedIndex = 1
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
   
    func checkBackgrouond(){
        
        if thisuser!.isValid{
            if UserDefault.getBool(key: PARAMS.LOGOUT,defaultValue: false){
                networkStatus = 1
                self.showLoadingView(vc: self)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.hideLoadingView()
                    self.gotoVC(VCs.LOGINNAV)
                }
                
            }else{
                networkStatus = 1
                let str_userName = thisuser!.username
                let str_pwd = thisuser!.password
                
                self.showLoadingView(vc: self)
                ApiManager.authenticate(username: str_userName, password: str_pwd) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        UserDefault.setBool(key: PARAMS.LOGOUT, value: false)
                        //print("Login success data =========>",data as Any)
                        thisuser?.clearUserInfo()
                        
                        UserDefault.setString(key: PARAMS.USERNAME, value: str_userName)
                        UserDefault.setString(key: PARAMS.PASSWORD, value: str_pwd)
                        
                        let json = JSON(data as Any)
                        
                        let jsondata = json["data"].object
                        thisuser = UserModel(JSON(jsondata as Any))
                        thisuser?.id = JSON(JSON(jsondata as Any)["user"].object)["_id"].stringValue
                        thisuser!.saveUserInfo()
                        thisuser!.loadUserInfo()
                        print("this is the fcm token===>", deviceTokenString)
                        ApiManager.uploadToken { (isSuccess, data) in
                            
                            self.gotoNextVC()
                            dump(thisuser, name: "this is userinformation=============>")
                        }
                    }else{
                        self.gotoVC(VCs.LOGINNAV)
                    }
                }
            }
        }else{
            networkStatus = 1
            self.showLoadingView(vc: self)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                self.hideLoadingView()
                if UserDefault.getBool(key: PARAMS.FIRST_LOGIN,defaultValue: true){
                    self.gotoVC(VCs.WELCOMENAV)
                }else{
                    self.gotoVC(VCs.LOGINNAV)
                }
            })
        }
    }
}
