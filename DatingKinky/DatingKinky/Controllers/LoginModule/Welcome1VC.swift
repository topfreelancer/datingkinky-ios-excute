//
//  Welcome1VC.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class Welcome1VC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.gotoNavPresent(VCs.WELCOME_2, fullscreen: true)
    }
    @IBAction func skipBtnClicked(_ sender: Any) {
        self.gotoVC(VCs.LOGINNAV)
    }
}
