
import UIKit
import EventKit

class DatePickerVC: BaseVC {
    
    @IBOutlet weak var uiv_dlgBack: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var heredate: Date? = nil
    var localTimeZoneIdentifier: String { return TimeZone.current.identifier }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let today = Date()
        self.datePicker.setDate(today, animated: false)
        extendedLayoutIncludesOpaqueBars = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        uiv_dlgBack.roundCorners([.topLeft, .topRight], radius: 20)
    }
    
    @IBAction func onValueChange(_ picker : UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"   // You can also
        let inputDate = dateFormatter.string(from: picker.date)
        str_birthday = "\(inputDate)"
    }
    
    @IBAction func spaceClicked(_ sender: Any) {
        self.closeMenu(.datePicker)
    }
    
    @IBAction func topBtnClicked(_ sender: Any) {
        self.closeMenu(.datePicker)
    }
}











