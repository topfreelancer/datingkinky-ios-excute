//
//  ImagePicker.swift
//  emoglass
//
//  Created by AngelDev on 7/19/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
}

open class ImagePicker: NSObject {

    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    private var cameraShow: Bool = false

    public init(presentationController: UIViewController, delegate: ImagePickerDelegate, cameraShow: Bool) {
        self.pickerController = UIImagePickerController()
        pickerController.allowsEditing = false
        pickerController.sourceType = .camera

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate

        self.pickerController.delegate = self
       // self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
        self.cameraShow = cameraShow
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }

    public func present(from sourceView: UIView) {

        if self.cameraShow{
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                return
            }
            self.pickerController.sourceType = .camera
            self.presentationController?.present(self.pickerController, animated: true)
        }else{
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                return
            }
            self.pickerController.sourceType = .photoLibrary
            self.presentationController?.present(self.pickerController, animated: true)
        }
        /*let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)*/

    }

    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)

        self.delegate?.didSelect(image: image)
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let image = info[.originalImage] as? UIImage {

            let  targetImage = resizeImag1(image: image, maxHeight: 500, maxWidth: 500, compressionQuality: 0.5, mode: "jpg")
            self.pickerController(picker, didSelect: targetImage)
        }
    }
}

extension ImagePicker: UINavigationControllerDelegate {

}
