//
//  VoiceChatViewController.swift
//  Agora-iOS-Voice-Tutorial
//
//  Created by to on 2017/4/10.
//  Copyright © 2017年 Agora. All rights reserved.
//

import UIKit
import AgoraRtcKit

class VoiceChatViewController: BaseVC {

    var roomID = "default"
    var str_profile: String?
    @IBOutlet weak var controlButtonsView: UIView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_ueserProfileContent: UILabel!
    @IBOutlet weak var imv_profile: UIImageView!
    var agoraKit: AgoraRtcEngineKit!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefault.setBool(key: PARAMS.IS_VOICE_CHATTING, value: true)
        UserDefault.Sync()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addNavBarImage()
        self.addLeftButton4NavBar1()
        setUI()
        initializeAgoraEngine()
        joinChannel()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefault.setBool(key: PARAMS.IS_VOICE_CHATTING, value: false)
        UserDefault.Sync()
    }
    
    func addLeftButton4NavBar1() {
        // if needed i will add
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "btn_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(addTappedLeftBtn1), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom:8, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func addTappedLeftBtn1() {
        self.leaveChannel()
        let storyboad = UIStoryboard(name: StoryBoards.CHAT, bundle: nil)
        let messageSendVC = storyboad.instantiateViewController(withIdentifier: VCs.MESSAGESENDNAV)
        UIApplication.shared.keyWindow?.rootViewController = messageSendVC
    }
    
    func setUI() {
        if let url = self.str_profile{
            setImageWithURL(url, imv: self.imv_profile)
        }
        if let chattingUserName = memberName4ChatRequest, let chattingUserProfile = memberProfile4ChatRequest{
            self.lbl_userName.text = chattingUserName
            if let lastSeenChatRequests = lastSeenChatRequest{
                self.lbl_ueserProfileContent.text = chattingUserProfile + ":" + lastSeenChatRequests
            }
        }
    }
    
    func initializeAgoraEngine() {
        // Initializes the Agora engine with your app ID.
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: Constants.agoraAppID, delegate: self)
    }
    
    func joinChannel() {
        // Allows a user to join a channel.
        agoraKit.joinChannel(byToken: "", channelId: roomID, info:nil, uid:0) {[unowned self] (sid, uid, elapsed) -> Void in
            // Joined channel "demoChannel"
            self.agoraKit.setEnableSpeakerphone(true)
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
    @IBAction func didClickHangUpButton(_ sender: UIButton) {
        self.leaveChannel()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Chat", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.MESSAGESENDNAV)
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    func leaveChannel() {
        agoraKit.leaveChannel(nil)
        hideControlButtons()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    func hideControlButtons() {
        controlButtonsView.isHidden = true
    }
    
    @IBAction func didClickMuteButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        // Stops/Resumes sending the local audio stream.
        agoraKit.muteLocalAudioStream(sender.isSelected)
    }
    
    @IBAction func didClickSwitchSpeakerButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        agoraKit.setEnableSpeakerphone(sender.isSelected)
    }
}

extension VoiceChatViewController: AgoraRtcEngineDelegate{
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStateChangedOfUid uid: UInt, state: AgoraAudioRemoteState, reason: AgoraAudioRemoteStateReason, elapsed: Int) {
        if state.rawValue == 0{
            self.leaveChannel()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Chat", bundle: nil)
            let toVC = storyBoard.instantiateViewController( withIdentifier: VCs.MESSAGESENDNAV)
            toVC.modalPresentationStyle = .fullScreen
            self.present(toVC, animated: false, completion: nil)
        }
    }
}
