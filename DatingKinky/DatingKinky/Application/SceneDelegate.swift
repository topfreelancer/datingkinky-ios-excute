//
//  SceneDelegate.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/22/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        /*ApiManager.uploadToken { (isSuccess, data) in
            if isSuccess{
                print("upload again")
            }
        }*/
        // set is video chatting flag as false
        dismiss_flag = false
        UserDefault.setBool(key: PARAMS.IS_VIDEO_CHATTING, value: false)
        UserDefault.setBool(key: PARAMS.IS_VOICE_CHATTING, value: false)
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
    }
}

