//
//  AllChatModel&Cell.swift
//  DatingKinky
//
//  Created by Ubuntu on 8/2/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class AllChatModel: NSObject{
    
    var id: String?
    var userName: String?
    var userAvatar: String?
    var duration: String?
    var content: String?
    var currentAge: String?
    var kinkRole: String?
    var gender: String?
    var messageNum: String?
    var pronoun: String?
    var location: String?
    var lastSeen: String?
    var showOverlay: Bool = false
    var isBlock: String?
    
    override init() {
        self.id = ""
        self.userName = ""
        self.userAvatar = ""
        self.duration = ""
        self.content = ""
        self.gender = ""
        self.currentAge = ""
        self.kinkRole = ""
        self.messageNum = ""
        self.pronoun = ""
        self.location = ""
        self.lastSeen = ""
        self.showOverlay = false
        self.isBlock = ""
    }
    
    init(id: String, userName : String, userAvatar : String, duration: String, content: String, currentAge: String, kinkRole: String, gender: String, messageNum: String, pronoun: String, location: String, lastSeen: String, showOverlay: Bool = false, isBlock: String) {
        
        self.id = id
        self.userName = userName
        self.userAvatar = userAvatar
        self.duration = getStrDateshort(duration)
        self.content = content
        self.gender = gender
        self.currentAge = currentAge
        self.kinkRole = kinkRole
        self.messageNum = messageNum
        self.pronoun = pronoun
        self.location = location
        self.lastSeen = lastSeen
        self.showOverlay = showOverlay
        self.isBlock = isBlock
    }
}

class AllChatCell: UICollectionViewCell {
    
    @IBOutlet weak var totalView: UIView!
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    @IBOutlet weak var uiv_overlay: UIView!
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var btn_decline: UIButton!
    @IBOutlet weak var btn_spaceBtn: UIButton!
    //@IBOutlet weak var uiv_colorOverlay: UIView!
    //@IBOutlet weak var uiv_blur: UIView!
    @IBOutlet weak var uiv_blur: CustomBlurView!
    
    var entity: AllChatModel!{
        didSet{
            let url = URL(string: entity.userAvatar ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
            self.lbl_userName.text = entity.userName
           
            self.lbl_duration.text = entity.duration
            self.lbl_content.text = entity.content
            self.lbl_content.font = lbl_content.font.italic
            //totalView.addBlur()
            self.uiv_overlay.isHidden = !entity.showOverlay
            uiv_blur.visualEffectView.blurRadius = 3
        }
    }
}

