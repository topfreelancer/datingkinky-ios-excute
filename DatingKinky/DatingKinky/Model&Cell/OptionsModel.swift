//
//  OptionsModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 8/3/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class OptionsModel: NSObject{
    
    var valueName: String?
    var valueID: String?
    
    init(_ json: JSON) {
        
        self.valueName = json["name"].stringValue
        self.valueID = json["_id"].stringValue
    }
}
