//
//  UserMembershipModel.swift
//  DatingKinky
//
//  Created by top Dev on 14.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserMembershipModel: NSObject {
    var joined_date: String?
    var current_membership: String?
    var renew_date: String?
    var lookingfor: String?
    var orientation: String?
    var status: String?
    var relationship_style: String?
    var herefor: String?
    var kinkroles: String?
    var trialstart: String?
    var trialend: String?
    
    override init() {
       super.init()
        self.joined_date = ""
        self.current_membership = ""
        self.renew_date = ""
        self.lookingfor = ""
        self.orientation = ""
        self.status = ""
        self.relationship_style = ""
        self.herefor = ""
        self.kinkroles = ""
        self.trialstart = ""
        self.trialend = ""
    }
    init(joined_date: String,  current_membership: String, renew_date: String, lookingfor: String, orientation: String, status: String, relationship_style: String, herefor: String, kinkroles: String, trilastart: String, trialend: String ) {
        self.joined_date = joined_date
        self.current_membership = current_membership
        self.renew_date = renew_date
        self.lookingfor = lookingfor
        self.orientation = orientation
        self.status = status
        self.relationship_style = relationship_style
        self.herefor = herefor
        self.kinkroles = kinkroles
        self.trialstart = trilastart
        self.trialend = trialend
    }
    
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setString(key: PARAMS.JOINED_DATE, value: joined_date)
        UserDefault.setString(key: PARAMS.CURRENT_MEMBERSHIP, value: current_membership)
        UserDefault.setString(key: PARAMS.RENEW_DATE, value: renew_date)
        UserDefault.setString(key: PARAMS.LOOKING_FOR, value: lookingfor)
        UserDefault.setString(key: PARAMS.ORIENTATION, value: orientation)
        UserDefault.setString(key: PARAMS.STATUS, value: status)
        UserDefault.setString(key: PARAMS.RELATIONSHIP_STYLE, value: relationship_style)
        UserDefault.setString(key: PARAMS.HEREFOR, value: herefor)
        UserDefault.setString(key: PARAMS.KINKROLES, value: kinkroles)
        UserDefault.setString(key: PARAMS.TIRAL_START, value: trialstart)
        UserDefault.setString(key: PARAMS.TRIAL_END, value: trialend)
        UserDefault.Sync()
    }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        self.joined_date = UserDefault.getString(key: PARAMS.JOINED_DATE, defaultValue: "")
        self.current_membership = UserDefault.getString(key: PARAMS.CURRENT_MEMBERSHIP, defaultValue: "")
        self.renew_date = UserDefault.getString(key: PARAMS.RENEW_DATE, defaultValue: "")
        self.lookingfor = UserDefault.getString(key: PARAMS.LOOKING_FOR, defaultValue: "")
        self.orientation = UserDefault.getString(key: PARAMS.ORIENTATION, defaultValue: "")
        self.status = UserDefault.getString(key: PARAMS.STATUS, defaultValue: "")
        self.relationship_style = UserDefault.getString(key: PARAMS.RELATIONSHIP_STYLE, defaultValue: "")
        self.herefor = UserDefault.getString(key: PARAMS.HEREFOR, defaultValue: "")
        self.kinkroles = UserDefault.getString(key: PARAMS.KINKROLES, defaultValue: "")
        self.trialstart = UserDefault.getString(key: PARAMS.TIRAL_START, defaultValue: "")
        self.trialend = UserDefault.getString(key: PARAMS.TRIAL_END, defaultValue: "")
    }
    
    // Clear save user credential
    func clearUserInfo() {
        self.joined_date = ""
        self.current_membership = ""
        self.renew_date = ""
        self.lookingfor = ""
        self.orientation = ""
        self.status = ""
        self.relationship_style = ""
        self.herefor = ""
        self.kinkroles = ""
        self.trialstart = ""
        self.trialend = ""
        
        UserDefault.setString(key: PARAMS.JOINED_DATE, value: nil)
        UserDefault.setString(key: PARAMS.CURRENT_MEMBERSHIP, value: nil)
        UserDefault.setString(key: PARAMS.RENEW_DATE, value: nil)
        UserDefault.setString(key: PARAMS.LOOKING_FOR, value: nil)
        UserDefault.setString(key: PARAMS.ORIENTATION, value: nil)
        UserDefault.setString(key: PARAMS.STATUS, value: nil)
        UserDefault.setString(key: PARAMS.RELATIONSHIP_STYLE, value: nil)
        UserDefault.setString(key: PARAMS.HEREFOR, value: nil)
        UserDefault.setString(key: PARAMS.KINKROLES, value: nil)
        UserDefault.setString(key: PARAMS.TIRAL_START, value: nil)
        UserDefault.setString(key: PARAMS.TRIAL_END, value: nil)
        UserDefault.Sync()
    }
}
