//
//  ChatRequestsModel&Cell.swift
//  DatingKinky
//
//  Created by Ubuntu on 8/2/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatRequestModel: NSObject{
    
    var id: String?
    var receiver_id: String?
    var userName: String?
    var userAvatar: String?
    var userCurrentAge: String?
    var userGender: String?
    var userKinkRole: String?
    var userPronoun: String?
    var userLocation: String?
    var state: String?
    var lastSeen: String?
    
    override init() {
        
        self.id = ""
        self.receiver_id = ""
        self.userName = ""
        self.userAvatar = ""
        self.userCurrentAge = ""
        self.userGender = ""
        self.userKinkRole = ""
        self.userPronoun = ""
        self.userLocation = ""
        self.state = ""
        self.lastSeen = ""
    }
    
    init(id: String, sender_id: String, userName : String, userAvatar : String,userCurrentAge: String, userGender: String, userKinkRole: String, userPronoun: String, userLocation: String, state: String, lastSeen: String) {
        
        self.id = id
        self.receiver_id = sender_id
        self.userName = userName
        self.userAvatar = userAvatar
        self.userCurrentAge = userCurrentAge
        self.userGender = userGender
        self.userKinkRole = userKinkRole
        self.userPronoun = userPronoun
        self.userLocation = userLocation
        self.state = state
        self.lastSeen = lastSeen
    }
}

class ChatRequestCell: UICollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_age_gender_role: UILabel!
    @IBOutlet weak var lbl_pronoun: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var btn_denny: UIButton!
    @IBOutlet weak var btn_chat: UIButton!
    @IBOutlet weak var imv_bottomPagination: UIImageView!
    @IBOutlet weak var btn_more: UIButton!
    
    var entity: ChatRequestModel!{
        
        didSet{
            let url = URL(string: entity.userAvatar ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
            self.lbl_userName.text = entity.userName
            let gender = entity.userGender! == "woman" ? "W" : "M"
            self.lbl_age_gender_role.text = entity.userCurrentAge! + gender + "," + " " + entity.userKinkRole!
            self.lbl_pronoun.text = entity.userPronoun
            self.lbl_location.text = entity.userLocation
        }
    }
}


