//
//  UpdateUserModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/30/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation

class UpdateUserModel: NSObject {
    
    var aboutMe: String?
    var aboutUs: String?
    var aboutYou: String?
    var firstMeeting: String?
    var gender: String?
    var hereFor: [String]?
    var kinkRole: String?
    var location: String?
    var orientation: String?
    var pronoun: String?
    var relationshipStatus: String?
    var relationshipStyle: String?
    var relationshipStyleDetails: String?
    
    var coordinates: CoordinateModel?
    var appearance: AppearanceModel?
    var kinkStyle: KinkStyleModel?
    var lifeStyle: LifeStyleModel?
    
    override init() {
        super.init()
        aboutMe = ""
        aboutUs = ""
        aboutYou = ""
        firstMeeting = ""
        gender = ""
        hereFor = [""]
        kinkRole = ""
        location = ""
        orientation = ""
        pronoun = ""
        relationshipStatus = ""
        relationshipStyle = ""
        relationshipStyleDetails = ""
        
        coordinates = nil
        appearance = nil
        kinkStyle = nil
        lifeStyle = nil
    }
    
    init(aboutMe: String, aboutUs: String, aboutYou: String, firstMeeting: String, gender: String, hereFor: [String], kinkRole: String, location: String, orientation: String, pronoun: String, relationshipStatus: String, relationshipStyle: String, relationshipStyleDetails: String, coordinates: CoordinateModel?, appearance: AppearanceModel?, kinkStyle: KinkStyleModel?, lifeStyle: LifeStyleModel?){
        
        self.aboutMe = aboutMe
        self.aboutUs = aboutUs
        self.firstMeeting = firstMeeting
        self.gender = gender
        self.hereFor = hereFor
        self.kinkRole = kinkRole
        self.location = location
        self.orientation = orientation
        self.pronoun = pronoun
        self.relationshipStatus = relationshipStatus
        self.relationshipStyle = relationshipStyle
        self.relationshipStyleDetails = relationshipStyleDetails
        self.coordinates = coordinates
        self.appearance = appearance
        self.kinkStyle = kinkStyle
        self.lifeStyle = lifeStyle
    }
    
    
}

class AppearanceModel: NSObject {
    
    var bodyType: String?
    var ethnicDetails: String?
    var ethnicity: String?
    var eyeColour: String?
    var hairColour: String?
    var height: String?
    
    override init() {
        bodyType = ""
        ethnicDetails = ""
        ethnicity = ""
        eyeColour = ""
        hairColour = ""
        height = ""
    }
    
    init(bodyType : String,ethnicDetails : String,ethnicity : String,
         eyeColour : String,hairColour: String,height : String){
        self.bodyType = bodyType
        self.ethnicDetails = ethnicDetails
        self.ethnicity = ethnicity
        self.eyeColour = eyeColour
        self.hairColour = hairColour
        self.height = height
    }
    
}

class KinkStyleModel: NSObject {
    
    var fetlife: String?
    var identifiesAs: [String]?
    var kinkActivities: [String]?
    var kinkSpheres: [String]?
    var sexAndKinks: [String]?
    
    override init() {
        fetlife = ""
        identifiesAs = nil
        kinkActivities = nil
        kinkSpheres = nil
        sexAndKinks = nil
    }
    
    init(fetlife: String,identifiesAs: [String]?,kinkActivities: [String]?,kinkSpheres : [String]?,sexAndKinks: [String]?) {
        self.fetlife = fetlife
        self.identifiesAs = identifiesAs
        self.kinkActivities = kinkActivities
        self.kinkSpheres = kinkSpheres
        self.sexAndKinks = sexAndKinks
    }
    
}

class LifeStyleModel: NSObject {
    
    var drink: String?
    var eatingHabits: String?
    var exerciseFrequency: String?
    var ownWords: String?
    var religion: String?
    var safeSex: String?
    var safeSexDetails: String?
    var smoke: String?
    var workDetails: String?
    var fetlife: String?
    
    override init() {
        drink = ""
        eatingHabits = ""
        exerciseFrequency = ""
        ownWords = ""
        religion = ""
        safeSex = ""
        safeSexDetails = ""
        smoke = ""
        workDetails = ""
        fetlife = ""
    }
    
    init(drink: String,eatingHabits: String,exerciseFrequency: String,ownWords: String,religion: String,safeSex: String,safeSexDetails: String,smoke: String,workDetails: String, fetlife: String) {
        self.drink = drink
        self.eatingHabits = eatingHabits
        self.exerciseFrequency = exerciseFrequency
        self.ownWords = ownWords
        self.religion = religion
        self.safeSex = safeSex
        self.safeSexDetails = safeSexDetails
        self.smoke = smoke
        self.workDetails = workDetails
        self.fetlife = fetlife
    }
}

