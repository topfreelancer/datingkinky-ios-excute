//
//  PlusPaymentRecordModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/28/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlusPaymentRecordModel: NSObject, NSCoding{
    
    var __v = 0
    var _id = ""
    var user = ""
    var updatedAt = ""
    var transactionId = ""
    var timestamp = ""
    var subscriptionTypeId = ""
    var subscriptionId = ""
    var status = ""
    var referringUrl = ""
    var referringPeriod = ""
    var rebills = ""
    var priceDescription = ""
    var paymentType = ""
    var nextRenewalDate = ""
    var eventType = ""
    var email = ""
    var creaetedAt = ""
    var clientSubacc = ""
    var clientAccnum = ""
    
    init(__v : Int, _id : String, user : String, updatedAt : String, transactionId : String, timestamp : String, subscriptionTypeId : String, subscriptionId : String, status : String, referringUrl : String, referringPeriod : String, rebills : String, priceDescription : String, paymentType : String, nextRenewalDate : String, eventType : String, email : String, creaetedAt : String, clientSubacc : String, clientAccnum : String) {
        
        self.__v = __v
        self._id = _id
        self.user = user
        self.updatedAt = updatedAt
        self.transactionId = transactionId
        self.timestamp = timestamp
        self.subscriptionTypeId = subscriptionTypeId
        self.subscriptionId = subscriptionId
        self.status = status
        self.referringUrl = referringUrl
        self.referringPeriod = referringPeriod
        self.rebills = rebills
        self.priceDescription = priceDescription
        self.paymentType = paymentType
        self.nextRenewalDate = nextRenewalDate
        self.eventType = eventType
        self.email = email
        self.creaetedAt = creaetedAt
        self.clientSubacc = clientSubacc
        self.clientAccnum = clientAccnum
    }
    
    init(_ jsonData : JSON) {
        
        self.__v = jsonData[PARAMS.__V].intValue
        self._id = jsonData[PARAMS._ID].stringValue
        self.user = jsonData[PARAMS.USER].stringValue
        self.updatedAt = jsonData[PARAMS.UPDATED_AT].stringValue
        self.transactionId = jsonData[PARAMS.TRANSACTION_ID].stringValue
        self.timestamp = jsonData[PARAMS.TIMESTAMP].stringValue
        self.subscriptionTypeId = jsonData[PARAMS.SUBSCRIPTION_TYPE_ID].stringValue
        self.subscriptionId = jsonData[PARAMS.SUBSCRIPTION_ID].stringValue
        self.status = jsonData[PARAMS.STATUS].stringValue
        self.referringUrl = jsonData[PARAMS.REFERRING_URL].stringValue
        self.referringPeriod = jsonData[PARAMS.RECURRING_PERIOD].stringValue
        self.rebills = jsonData[PARAMS.REBILLS].stringValue
        self.priceDescription = jsonData[PARAMS.PRICE_DESCRIPTION].stringValue
        self.paymentType = jsonData[PARAMS.PAYMENT_TYPE].stringValue
        self.nextRenewalDate = jsonData[PARAMS.NEXT_RENEWAL_DATE].stringValue
        self.eventType = jsonData[PARAMS.EVENT_TYPE].stringValue
        self.email = jsonData[PARAMS.EMAIL].stringValue
        self.creaetedAt = jsonData[PARAMS.CREATED_AT].stringValue
        self.clientSubacc = jsonData[PARAMS.CLIENT_SUB_ACC].stringValue
        self.clientAccnum = jsonData[PARAMS.CLIENT_ACC_NUM].stringValue
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let __v = aDecoder.decodeInteger(forKey: PARAMS.__V)
        let _id = aDecoder.decodeObject(forKey: PARAMS._ID) as! String
        let user = aDecoder.decodeObject(forKey: PARAMS.USER) as! String
        let updatedAt = aDecoder.decodeObject(forKey: PARAMS.UPDATED_AT) as! String
        let transactionId = aDecoder.decodeObject(forKey: PARAMS.TRANSACTION_ID) as! String
        let timestamp = aDecoder.decodeObject(forKey: PARAMS.TIMESTAMP) as! String
        let subscriptionTypeId = aDecoder.decodeObject(forKey: PARAMS.SUBSCRIPTION_TYPE_ID) as! String
        let subscriptionId = aDecoder.decodeObject(forKey: PARAMS.SUBSCRIPTION_ID) as! String
        let status = aDecoder.decodeObject(forKey: PARAMS.STATUS) as! String
        let referringUrl = aDecoder.decodeObject(forKey: PARAMS.REFERRING_URL) as! String
        let referringPeriod = aDecoder.decodeObject(forKey: PARAMS.RECURRING_PERIOD) as! String
        let rebills = aDecoder.decodeObject(forKey: PARAMS.REBILLS) as! String
        let priceDescription = aDecoder.decodeObject(forKey: PARAMS.PRICE_DESCRIPTION) as! String
        let paymentType = aDecoder.decodeObject(forKey: PARAMS.PAYMENT_TYPE) as! String
        let nextRenewalDate = aDecoder.decodeObject(forKey: PARAMS.NEXT_RENEWAL_DATE) as! String
        let eventType = aDecoder.decodeObject(forKey: PARAMS.EVENT_TYPE) as! String
        let email = aDecoder.decodeObject(forKey: PARAMS.EMAIL) as! String
        let creaetedAt = aDecoder.decodeObject(forKey: PARAMS.CREATED_AT) as! String
        let clientSubacc = aDecoder.decodeObject(forKey: PARAMS.CLIENT_SUB_ACC) as! String
        let clientAccnum = aDecoder.decodeObject(forKey: PARAMS.CLIENT_ACC_NUM) as! String
        
        self.init(__v : __v, _id : _id, user : user, updatedAt : updatedAt, transactionId : transactionId, timestamp : timestamp, subscriptionTypeId : subscriptionTypeId, subscriptionId : subscriptionId, status : status, referringUrl : referringUrl, referringPeriod : referringPeriod, rebills : rebills, priceDescription : priceDescription, paymentType : paymentType, nextRenewalDate : nextRenewalDate, eventType : eventType, email : email, creaetedAt : creaetedAt, clientSubacc : clientSubacc, clientAccnum : clientAccnum)
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(__v,forKey: PARAMS.__V)
        aCoder.encode(_id,forKey: PARAMS._ID)
        aCoder.encode(user,forKey: PARAMS.USER)
        aCoder.encode(updatedAt,forKey: PARAMS.UPDATED_AT)
        aCoder.encode(transactionId,forKey: PARAMS.TRANSACTION_ID)
        aCoder.encode(timestamp,forKey: PARAMS.TIMESTAMP)
        aCoder.encode(subscriptionTypeId,forKey: PARAMS.SUBSCRIPTION_TYPE_ID)
        aCoder.encode(subscriptionId,forKey: PARAMS.SUBSCRIPTION_ID)
        aCoder.encode(status,forKey: PARAMS.STATUS)
        aCoder.encode(referringUrl,forKey: PARAMS.REFERRING_URL)
        aCoder.encode(referringPeriod,forKey: PARAMS.RECURRING_PERIOD)
        aCoder.encode(rebills,forKey: PARAMS.REBILLS)
        aCoder.encode(priceDescription,forKey: PARAMS.PRICE_DESCRIPTION)
        aCoder.encode(paymentType,forKey: PARAMS.PAYMENT_TYPE)
        aCoder.encode(nextRenewalDate,forKey: PARAMS.NEXT_RENEWAL_DATE)
        aCoder.encode(eventType,forKey: PARAMS.EVENT_TYPE)
        aCoder.encode(email,forKey: PARAMS.EMAIL)
        aCoder.encode(creaetedAt,forKey: PARAMS.CREATED_AT)
        aCoder.encode(clientSubacc,forKey: PARAMS.CLIENT_SUB_ACC)
        aCoder.encode(clientAccnum,forKey: PARAMS.CLIENT_ACC_NUM)
    }
}
