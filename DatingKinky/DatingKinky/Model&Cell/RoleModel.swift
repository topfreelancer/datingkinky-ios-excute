//
//  RoleModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/28/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class RoleModel: NSObject, NSCoding {
    
    var _id = ""
    var name = ""
    var lowerCasename = ""
    
    init(_id : String, name : String, lowerCasename : String) {
        
        self._id = _id
        self.name = name
        self.lowerCasename = lowerCasename
    }
    
    
    init(_ jsonData : JSON) {
        
        self._id = jsonData[PARAMS._ID].stringValue
        self.name = jsonData[PARAMS.NAME].stringValue
        self.lowerCasename = jsonData[PARAMS.LOWERCASE_NAME].stringValue
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let _id = aDecoder.decodeObject(forKey: PARAMS._ID) as! String
        let name = aDecoder.decodeObject(forKey: PARAMS.NAME) as! String
        let lowerCasename = aDecoder.decodeObject(forKey: PARAMS.LOWERCASE_NAME) as! String
        
        self.init(_id : _id, name : name,lowerCasename: lowerCasename)
    }

    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(_id,forKey: PARAMS._ID)
        aCoder.encode(name,forKey: PARAMS.NAME)
        aCoder.encode(lowerCasename,forKey: PARAMS.LOWERCASE_NAME)
    }
}

