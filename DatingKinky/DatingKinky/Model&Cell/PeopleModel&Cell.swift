//
//  PeopleModel.swift
//  DatingKinky
//
//  Created by Ubuntu on 7/30/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class PeopleModel: NSObject{
    
    var id: String?
    var realName: String?
    var userName: String?
    var email: String?
    var pronoun: String?
    var userAvatar: String?
    var gender: String?
    var currentAge: String?
    var location: String?
    var userKinkRole: String?
    var lookingForGender: [String]?
    var orientation: [String]?
    var status: [String]?
    var relationshipStyle: [String]?
    var hereFor: [String]?
    var kinkRole: [String]?
    var identifiAs: [String]?
    var isPlusMember: Bool = false
    var chatRequest: Bool = false
    var lastActive: String?
    
    init(_ id: String, realName : String, userName : String, email: String,pronoun: String, userAvatar : String, gender : String, currentAge : String, location : String,userkinkRole: String, lookingForGender : [String],orientation : [String],status : [String],relationshipStyle : [String],hereFor : [String],kinkRole : [String],identifiAs: [String], isPlusMember : Bool,chatRequest: Bool, lastActive: String) {
        
        self.id = id
        self.realName = realName
        self.userName = userName
        self.email = email
        self.pronoun = pronoun
        self.userAvatar = userAvatar
        self.gender = gender
        self.currentAge = currentAge
        self.location = location
        self.userKinkRole = userkinkRole
        self.lookingForGender = lookingForGender
        self.orientation = orientation
        self.status = status
        self.relationshipStyle = relationshipStyle
        self.hereFor = hereFor
        self.kinkRole = kinkRole
        self.isPlusMember = isPlusMember
        self.chatRequest = chatRequest
        self.identifiAs = identifiAs
        self.lastActive = lastActive
    }
}

class titleViewCell: UICollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet weak var imv_plusMemeber: UIImageView!
    @IBOutlet weak var uiv_bottomChatRequest: UIView!
    @IBOutlet weak var chatBtn: UIButton!
    
    var entity: PeopleModel!{
        
        didSet{
            let url = URL(string: entity.userAvatar ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
            imv_plusMemeber.isHidden = !entity.isPlusMember
            uiv_bottomChatRequest.isHidden = !entity.chatRequest
        }
    }
}

class ThumbViewCell: UICollectionViewCell {
    
    @IBOutlet var imv_portfolio: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_userContent: UILabel!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var lbl_lastSeen: UILabel!
    
    var entity: PeopleModel!{
        didSet{
            
            let url = URL(string: entity.userAvatar ?? "")
            imv_portfolio.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
            lbl_userName.text = entity.userName
            lbl_lastSeen.text = entity.lastActive
            let gender = entity.gender == "woman" ? "W" : "M"
            let spaces = String(repeating: " ", count: 2)
            var content = entity.currentAge! + gender
            if let kinkrole = entity.userKinkRole, !kinkrole.isEmpty{
                content += "," + spaces + kinkrole
            }
            if let showhim = entity.pronoun, !showhim.isEmpty{
                content += spaces + "|" + spaces + showhim
            }
            if let location = entity.location , !location.isEmpty{
                content += spaces + "|" + spaces + location
            }
            lbl_userContent.text = content
        }
    }
}





