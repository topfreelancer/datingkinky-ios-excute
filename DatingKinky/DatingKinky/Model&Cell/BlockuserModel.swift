//
//  BlockuserModel.swift
//  DatingKinky
//
//  Created by top Dev on 13.12.2020.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import SwiftyJSON

class BlockuserModel: NSObject {
    
    var blockusers: String?
    var blockedusers: String?
    var snoozedusers: String?
    
    override init() {
       super.init()
        blockusers = ""
        blockedusers = ""
        snoozedusers = ""
    }
       
    init(_ json : JSON) {
        let blockstatus = JSON(json["blockstatus"].arrayObject?.first as Any)
        if let blockedstatus = json["blocked_account"].arrayObject{
            if blockedstatus.count != 0{
                var blocked = [String]()
                blocked.removeAll()
                var num = 0
                for one in blockedstatus{
                    num += 1
                    blocked.append(JSON(one as Any)["user_id"].stringValue)
                    if num == blockedstatus.count{
                        self.blockedusers = blocked.joined(separator: ",")
                    }
                }
            }else{
                self.blockedusers = ""
            }
        }
        if let snoozeaccounts = json["snooze_account"].arrayObject{
            if snoozeaccounts.count != 0{
                var snoozed = [String]()
                snoozed.removeAll()
                var num = 0
                for one in snoozeaccounts{
                    num += 1
                    snoozed.append(JSON(one as Any)["user_id"].stringValue)
                    if num == snoozeaccounts.count{
                        self.snoozedusers = snoozed.joined(separator: ",")
                    }
                }
            }else{
                self.snoozedusers = ""
            }
        }
        self.blockusers = blockstatus[PARAMS.BLOCKUSERS].stringValue
    }
    
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setString(key: PARAMS.BLOCKUSERS, value: blockusers)
        UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: blockedusers)
        UserDefault.setString(key: PARAMS.SNOOZEDUSERS, value: snoozedusers)
        UserDefault.Sync()
    }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        self.blockusers = UserDefault.getString(key: PARAMS.BLOCKUSERS, defaultValue: "")
        self.blockedusers = UserDefault.getString(key: PARAMS.BLOCKEDUSERS, defaultValue: "")
        self.snoozedusers = UserDefault.getString(key: PARAMS.SNOOZEDUSERS, defaultValue: "")
    }
    
    // Clear save user credential
    func clearUserInfo() {
        blockusers = ""
        blockedusers = ""
        snoozedusers = ""
        UserDefault.setString(key: PARAMS.BLOCKUSERS, value: nil)
        UserDefault.setString(key: PARAMS.BLOCKEDUSERS, value: nil)
        UserDefault.setString(key: PARAMS.SNOOZEDUSERS, value: nil)
        UserDefault.Sync()
    }
    
    func getBlockUsers_BlockedUsers() -> [String]? {
        if let blockusers = blockusers, let blockedusers = blockedusers{
            let blockuserarray = blockusers.components(separatedBy: ",")
            let blockeduserarray = blockedusers.components(separatedBy: ",")
            return blockuserarray + blockeduserarray
        }else{
            return nil
        }
    }
    
    func getBlockedUsers() -> [String]? {
        if let blockedusers = blockedusers{
            let blockeduserarray = blockedusers.components(separatedBy: ",")
            return blockeduserarray
        }else{
            return nil
        }
    }
    
    func getBlockUsers_BlockedUsers_SnoozedUsers() -> [String]? {
        if let blockusers = blockusers, let blockedusers = blockedusers, let snoozedusers = snoozedusers{
            let blockuserarray = blockusers.components(separatedBy: ",")
            let blockeduserarray = blockedusers.components(separatedBy: ",")
            let snoozeduserarray = snoozedusers.components(separatedBy: ",")
            return blockuserarray + blockeduserarray + snoozeduserarray
        }else{
            return nil
        }
    }
}


